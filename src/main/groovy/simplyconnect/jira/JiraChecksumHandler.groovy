package simplyconnect.jira

import com.atlassian.jira.rest.client.domain.Field
import com.atlassian.jira.rest.client.domain.Issue
import simplyconnect.http.HttpResponseWrapper

/**
 * User: david
 * Date: 23/02/13
 * Time: 11:25 PM
 */
class JiraChecksumHandler implements ChecksumHandler {
    String attachmentField
    Jiranator jiranator

    JiraChecksumHandler(String attachmentField) {
        this.attachmentField = attachmentField
    }

    Set<String> getChecksums(String jiraNumber) {
        Issue issue = jiranator.find(jiraNumber)
        Field attachmentField = issue.getField(attachmentField)
        if (attachmentField) {
            return JiraUtil.checksumToSet(attachmentField.value.toString())
        } else {
            throw new JiraChecksumNotSetup("To enable Jira to store the checksums, add an editable custom checksum field. No custom field: $attachmentField")
        }
    }

    void setChecksums(String jiraNumber, Set<String> checksums) {
        String checksumString = JiraUtil.checksumToString(checksums)
        HttpResponseWrapper response = JiraRestUrl.updateField(
                jiranator.config.user,
                jiranator.config.password,
                jiranator.config.baseJiraUri,
                jiraNumber,
                attachmentField,
                checksumString)
        response.throwIfNotOk("Could not set the checksums for the attachments, jira number")
    }

}
