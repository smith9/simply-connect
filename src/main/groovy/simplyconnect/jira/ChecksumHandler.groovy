package simplyconnect.jira

/**
 * User: david
 * Date: 23/02/13
 * Time: 11:19 PM
 */
public interface ChecksumHandler {
    void setJiranator(Jiranator jiranator)
    Set<String> getChecksums(String jiraNumber)
    void setChecksums(String jiraNumber, Set<String> checksums)
}