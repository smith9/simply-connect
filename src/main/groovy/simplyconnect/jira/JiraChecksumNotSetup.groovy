package simplyconnect.jira

/**
 * User: david
 * Date: 24/02/13
 * Time: 11:36 AM
 */
class JiraChecksumNotSetup extends JiraException {
    JiraChecksumNotSetup() {
        super()
    }

    JiraChecksumNotSetup(Throwable cause) {
        super(cause)
    }

    JiraChecksumNotSetup(String message) {
        super(message)
    }

    JiraChecksumNotSetup(String message, Throwable cause) {
        super(message, cause)
    }

    protected JiraChecksumNotSetup(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace)
    }
}
