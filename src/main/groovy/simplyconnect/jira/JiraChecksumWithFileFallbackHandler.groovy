package simplyconnect.jira

import com.atlassian.jira.rest.client.domain.Field
import com.atlassian.jira.rest.client.domain.Issue
import groovy.util.logging.Slf4j

/**
 * User: david
 * Date: 23/02/13
 * Time: 11:25 PM
 */
@Slf4j
class JiraChecksumWithFileFallbackHandler implements ChecksumHandler {
    JiraChecksumHandler jiraHandler
    FileChecksumHandler fileHandler
    boolean useFallback = false

    JiraChecksumWithFileFallbackHandler(String attachmentField, File checksumDir) {
        jiraHandler = new JiraChecksumHandler(attachmentField)
        fileHandler = new FileChecksumHandler(checksumDir)
    }

    public void setJiranator(Jiranator jiranator) {
        jiraHandler.jiranator = jiranator
    }

    Set<String> getChecksums(String jiraNumber) {
        Set<String> checksums
        try {
            checksums = jiraHandler.getChecksums(jiraNumber)
        } catch(JiraChecksumNotSetup exc) {
            logException(jiraNumber, exc)
            useFallback = true
            checksums = fileHandler.getChecksums(jiraNumber)
        }
        return checksums
    }

    void logException(String jiraNumber, JiraChecksumNotSetup exc) {
        log.error("Jira handling not setup, Jira Number $jiraNumber", exc)
    }

    void setChecksums(String jiraNumber, Set<String> checksums) {
        if (useFallback) {
            fileHandler.setChecksums(jiraNumber, checksums)
        } else {
            jiraHandler.setChecksums(jiraNumber, checksums)
        }
    }
}
