package simplyconnect.jira

import com.atlassian.jira.rest.client.GetCreateIssueMetadataOptions
import com.atlassian.jira.rest.client.GetCreateIssueMetadataOptionsBuilder
import com.atlassian.jira.rest.client.IssueRestClient
import com.atlassian.jira.rest.client.JiraRestClient
import com.atlassian.jira.rest.client.NullProgressMonitor
import com.atlassian.jira.rest.client.domain.BasicIssue
import com.atlassian.jira.rest.client.domain.CimIssueType
import com.atlassian.jira.rest.client.domain.CimProject
import com.atlassian.jira.rest.client.domain.Comment
import com.atlassian.jira.rest.client.domain.EntityHelper
import com.atlassian.jira.rest.client.domain.Issue
import com.atlassian.jira.rest.client.domain.Transition
import com.atlassian.jira.rest.client.domain.input.IssueInput
import com.atlassian.jira.rest.client.domain.input.TransitionInput
import com.atlassian.jira.rest.client.internal.jersey.JerseyJiraRestClientFactory
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j

import javax.activation.DataSource
import com.atlassian.jira.rest.client.domain.BasicPriority
import simplyconnect.http.HttpClientWrapper

/**
 * TODO start up a test instance of Jira and write some tests against it
 * User: david
 * Date: 03/02/13
 */
@Slf4j
class Jiranator {
    Credentials config
    IssueRestClient issueClient
    AttachmentHandler attachmentHandler
    private List<BasicPriority> priorities
    public static NullProgressMonitor NULL_PROGRESS_MONITOR = new NullProgressMonitor()

    Jiranator(Credentials config, ChecksumHandler checksumHandler) {
        this.config = config
        attachmentHandler = new AttachmentHandler(this, checksumHandler)
        checksumHandler.setJiranator(this)
    }

    void addAttachments(String jiraNumber, DataSource... attachments) {
        if (attachments.length > 0) {
            attachmentHandler.addAttachments(jiraNumber, attachments)
        }
    }

    public String createIssue(IssueInput input) {
        // TODO add subject to the the following log statement
        log.info("Attempting to create issue")
        BasicIssue issue = issueClient.createIssue(input, NULL_PROGRESS_MONITOR)
        // TODO add subject to the the following log statement
        log.info("Created jira ${issue.key}")
        return issue.key
    }

    public Issue addComment(String jiraNumber, String comment) {
        return addComment(find(jiraNumber), comment)
    }

    public Issue addComment(Issue issue, String comment) {
        URI commentsUri = issue.getCommentsUri()
        String jiraNumber = issue.getKey()
        log.info("adding comment to $jiraNumber with summary ${issue.summary}")
        issueClient.addComment(NULL_PROGRESS_MONITOR, commentsUri, Comment.valueOf(comment))
        log.info("added comment to $jiraNumber with summary ${issue.summary}")
        return issue
    }

    public boolean isValidTransition(String jiraNumber, String newStatus) {
        return isValidTransition(find(jiraNumber), newStatus)
    }

    public boolean isValidTransition(Issue issue, String newStatus) {
        String jiraNumber = issue.getKey()
        log.info("checking valid transition $newStatus on $jiraNumber with summary ${issue.summary}")

        Iterable<Transition> transitions = getIssueClient().getTransitions(issue.getTransitionsUri(), NULL_PROGRESS_MONITOR);
        boolean validTransition = getTransitionByName(transitions, newStatus) != null
        log.info("checked transition validity: $validTransition to $newStatus on $jiraNumber with summary ${issue.summary}")
        return validTransition
    }

    public Issue transition(String jiraNumber, String newStatus, String comment = "") {
        return transition(find(jiraNumber), newStatus, comment)

    }

    public Issue transition(Issue issue, String newStatus, String comment = "") {
        String jiraNumber = issue.getKey()
        if (issue.status?.name == newStatus) {
            log.info("skipping change of status because status is already $newStatus for $jiraNumber")
        } else {
            log.info("changing status to $newStatus on $jiraNumber with summary ${issue.summary}")

            Iterable<Transition> transitions = getIssueClient().getTransitions(issue.getTransitionsUri(), NULL_PROGRESS_MONITOR)
            final Transition newTransitionStatus = getTransitionByName(transitions, newStatus)

            if (newTransitionStatus) {
                TransitionInput input = null
                if (comment) {
                    input = new TransitionInput(newTransitionStatus.getId(), Comment.valueOf(comment))
                } else {
                    input = new TransitionInput(newTransitionStatus.getId())
                }
                getIssueClient().transition(issue, input, NULL_PROGRESS_MONITOR)

                log.info("changed status to $newStatus on $jiraNumber with summary ${issue.summary}")
            } else {
                log.info("Skipping change of status because this issue cannot be transitioned from ${issue.status?.name} to $newStatus")
            }
        }
        return issue

    }

    private static Transition getTransitionByName(Iterable<Transition> transitions, String transitionName) {
        return (Transition) transitions.find { Transition transition ->
            transition.name == transitionName
        }
    }

    public Issue find(String jiraNumber) {
        log.info("Searching for jira $jiraNumber")
        return getIssueClient().getIssue(jiraNumber, NULL_PROGRESS_MONITOR)
    }

    public IssueRestClient getIssueClient() {
        if (!issueClient) {
            log.info("Creating issue client")
            JerseyJiraRestClientFactory factory = new JerseyJiraRestClientFactory();
            JiraRestClient restClient = factory.createWithBasicHttpAuthentication(config.baseJiraUri, config.user, config.password);
            issueClient = restClient.getIssueClient()
        }
        return issueClient
    }

    CimProject findCimProject(String projectKey) {
        log.info("Searching for project $projectKey")
        // TODO - should we cache these?
        GetCreateIssueMetadataOptions issueMetadata = new GetCreateIssueMetadataOptionsBuilder().withProjectKeys(projectKey).withExpandedIssueTypesFields().build()
        Iterable<CimProject> projects = getIssueClient().getCreateIssueMetadata(issueMetadata, NULL_PROGRESS_MONITOR)
        return projects.iterator().next();
    }

    CimIssueType findCimIssueType(String jiraType, CimProject project) {
        log.info("Searching for issue type ${jiraType}")
        return EntityHelper.findEntityByName(project.getIssueTypes(), jiraType);
    }

    String findUserIdByEmail(String address) {
        log.info("Finding user id for email: $address")
        String urlString = JiraRestUrl.urlUserSearchByEmail(config.baseJiraUri, address)
        String userId = null
        try {
            String result = retrieve(urlString)
            if (result) {
                def json = new JsonSlurper().parseText(result)
                if (json.size() > 0) {
                    userId = json[0].name
                }
            }
        } catch (Exception exc) {
            log.error("Could not locate user id for email address $address via url $urlString", exc)
        }
        log.info("User id for $address was $userId")
        return userId
    }

    private String retrieve(String urlString) {
        log.info("retrieving data via $urlString")
        URL url = new URL(urlString)
        def authString = "${config.user}:${config.password}".getBytes().encodeBase64().toString()
        URLConnection connection = url.openConnection()
        connection.setRequestProperty("Authorization", "Basic ${authString}")
        connection.connect()
        return connection.content.text
    }

    static Long getPriorityId(String priorityName, List<BasicPriority> priorities) {
        BasicPriority found = priorities.find { BasicPriority priority ->
            priority.name == priorityName
        }
        if(!found) {
            String values = priorities.collect { it.name}.join(",")
            throw new JiraException("Priority $priorityName is not a valid property, valid values are $values")
        }
        return found.id
    }

    public List<BasicPriority> getPriorities() {
        List<BasicPriority> priorities = []
        Object json = new HttpClientWrapper().getJson(JiraRestUrl.getPriorityUrl(config.baseJiraUri))
        json.each { priorityJson ->
            priorities << new BasicPriority(new URI(priorityJson.self), Long.parseLong(priorityJson.id), priorityJson.name)
        }
        return priorities
    }
    public Long getPriorityId(String priorityName) {
        if(!priorities) {
            priorities = getPriorities()
        }
        return getPriorityId(priorityName, priorities)
    }
}
