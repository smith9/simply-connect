package simplyconnect.jira

/**
 * User: david
 * Date: 23/02/13
 * Time: 11:25 PM
 */
class FileChecksumHandler implements ChecksumHandler {
    FileChecksumHandler(File checksumDir) {
        this.checksumDir = checksumDir
    }

    File checksumDir

    void setJiranator(Jiranator jiranator) {

    }

    Set<String> getChecksums(String jiraNumber) {
        File checksumFile = determineChecksumFile(jiraNumber)
        if (checksumFile.exists()) {
            return JiraUtil.checksumToSet(checksumFile.text)
        } else {
            return []
        }
    }

    File determineChecksumFile(String jiraNumber) {
        return new File(checksumDir, "${jiraNumber}.txt")
    }

    void setChecksums(String jiraNumber, Set<String> checksums) {
        File checksumFile = determineChecksumFile(jiraNumber)
        checksumFile.text = JiraUtil.checksumToString(checksums)
    }
}
