package simplyconnect.jira

import simplyconnect.http.HttpClientWrapper
import simplyconnect.http.HttpResponseWrapper
import com.atlassian.jira.rest.client.domain.BasicPriority

/**
 * User: david
 * Date: 03/02/13
 */
class JiraRestUrl {
    public static String urlUserSearchByEmail(URI jiraBaseUri, String emailAddress) {
        // TODO do we need to encode email addresses?
        emailAddress = URLEncoder.encode(emailAddress)
        return jiraBaseUri.toString() + "/rest/api/2/user/search?username=${emailAddress}"
    }

    public static HttpResponseWrapper updateField(String user, String password, URI jiraBaseUri, String jiraNumber, String fieldId, String newValue) {
        String json = """{ "fields": { "${fieldId}": "${newValue}" } }"""
        return updateField(user, password, jiraBaseUri, jiraNumber, json)
    }

    public static HttpResponseWrapper updateField(String user, String password, URI jiraBaseUri, String jiraNumber, String json) {
        HttpClientWrapper client = new HttpClientWrapper()
        client.addBasicAuthHeader(user, password)
        URI updateFieldUri = createUri(jiraBaseUri, "/rest/api/2/issue/${jiraNumber}/")
        return client.putJson(updateFieldUri, json)
    }

    private static URI createUri(URI jiraBaseUri, String appendPath) {
        return URI.create(jiraBaseUri.toString() + appendPath)
    }

    static URI getPriorityUrl(URI baseUri) {
        return createUri(baseUri, "/rest/api/2/priority")
    }
}
