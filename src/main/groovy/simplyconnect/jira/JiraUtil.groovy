package simplyconnect.jira

import java.util.regex.Pattern

/**
 * User: david
 * Date: 03/02/13
 */
class JiraUtil {
    public static Pattern jiraNumberPattern(String projectCode) {
        return ~/${projectCode}-[0-9]+/
    }

    public static Set<Pattern> jiraNumberPatterns(Set<String> projectCodes) {
        return projectCodes.collect { String projectCode ->
            jiraNumberPattern(projectCode)
        }
    }

    /**
     * Extract a set of jira numbers from a piece of text using the supplied regular expression pattersn
     *
     * @param stringToParse
     * @param patterns , a pattern for a jira number would look like:
     * @return
     */
    public static Set<String> extractJiraNumbers(String stringToParse, Set<Pattern> patterns) {
        Set<String> jiraNumbers = []
        patterns.each { Pattern pattern ->
            stringToParse.eachMatch(pattern) {
                jiraNumbers << it
            }
        }
        return jiraNumbers
    }

    public static String checksumToString(Set<String> checksums) {
        return checksums.join(",")
    }

    public static Set<String> checksumToSet(String checksums) {
        return checksums.split(",")
    }

}
