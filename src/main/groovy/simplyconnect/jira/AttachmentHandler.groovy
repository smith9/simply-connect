package simplyconnect.jira

import com.atlassian.jira.rest.client.IssueRestClient
import com.atlassian.jira.rest.client.domain.Issue
import groovy.util.logging.Slf4j
import org.apache.commons.io.FileUtils
import simplyconnect.io.ChecksumGenerator

import javax.activation.DataSource

/**
 * User: david
 * Date: 23/02/13
 * Time: 8:54 PM
 */
@Slf4j
class AttachmentHandler {
    Jiranator jiranator
    ChecksumHandler checksumHandler

    AttachmentHandler(Jiranator jiranator, ChecksumHandler checksumHandler) {
        this.jiranator = jiranator
        this.checksumHandler = checksumHandler
    }

    void addAttachments(String jiraNumber, DataSource... attachments) {
        File tempOutputDir = File.createTempDir()
        ChecksumGenerator generator = new ChecksumGenerator(outputDir: tempOutputDir)
        Map<String, File> attachmentFilesAndChecksums = generator.generate(attachments)
        deleteOnExit(attachmentFilesAndChecksums.values())
        deleteOnExit([tempOutputDir])
        Set<String> alreadyAttachedChecksums = checksumHandler.getChecksums(jiraNumber)
        attachFiles(jiraNumber, alreadyAttachedChecksums, attachmentFilesAndChecksums)
    }

    void deleteOnExit(Collection<File> files) {
        files.each { File file ->
            file.deleteOnExit()
        }
    }

    void attachFiles(String jiraNumber, Set<String> alreadyAttachedChecksums, Map<String, File> checksumsAndFiles) {
        IssueRestClient client = jiranator.getIssueClient()
        Issue issue = jiranator.find(jiraNumber)
        checksumsAndFiles.each { String checksum, File attachment ->
            if (alreadyAttachedChecksums.contains(checksum)) {
                log.info("Skipping file already attached file: ${attachment.name} checksum: $checksum")
            } else {
                client.addAttachment(Jiranator.NULL_PROGRESS_MONITOR, issue.attachmentsUri, attachment.newInputStream(), attachment.name)
                alreadyAttachedChecksums << checksum
                checksumHandler.setChecksums(jiraNumber, alreadyAttachedChecksums)
            }
            FileUtils.deleteQuietly(attachment)
        }
    }
}
