package simplyconnect.jira

/**
 * User: david
 * Date: 24/02/13
 * Time: 11:27 AM
 */
class JiraException extends RuntimeException {
    JiraException() {
        super()
    }

    JiraException(Throwable cause) {
        super(cause)
    }

    JiraException(String message) {
        super(message)
    }

    JiraException(String message, Throwable cause) {
        super(message, cause)
    }

    protected JiraException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace)
    }
}
