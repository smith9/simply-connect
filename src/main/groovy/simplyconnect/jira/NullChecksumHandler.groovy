package simplyconnect.jira

import groovy.util.logging.Slf4j

/**
 * User: david
 * Date: 24/02/13
 * Time: 12:01 PM
 */
@Slf4j
class NullChecksumHandler implements ChecksumHandler {
    void setJiranator(Jiranator jiranator) {

    }

    Set<String> getChecksums(String jiraNumber) {
        logDoesNothingMessage()
        return []
    }

    private void logDoesNothingMessage() {
        log.info("Default checksum handler does nothing, you can use others such as JiraChecksumHandler, FileChecksumHandler")
    }

    void setChecksums(String jiraNumber, Set<String> checksums) {
        logDoesNothingMessage()
    }
}
