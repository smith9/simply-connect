package simplyconnect.jira

import com.atlassian.jira.rest.client.domain.CimIssueType
import com.atlassian.jira.rest.client.domain.CimProject
import com.atlassian.jira.rest.client.domain.input.IssueInput
import com.atlassian.jira.rest.client.domain.input.IssueInputBuilder
import org.apache.commons.lang.StringUtils

/**
 * User: david
 * Date: 03/02/13
 */
class BasicIssueInputBuilder {
    String reporterEmail
    String summary
    String description
    String projectKey
    String jiraType
    List<String> components
    List<String> fixVersions
    Map<String, String> customFields
    Long priority

    // TODO is there a JIRA constant for this?
    static int MAX_SUMMARY_LENGTH = 255

    BasicIssueInputBuilder reporterEmail(String reporterEmail) {
        this.reporterEmail = reporterEmail
        return this
    }

    BasicIssueInputBuilder summary(String summary) {
        this.summary = StringUtils.abbreviate(summary, MAX_SUMMARY_LENGTH)
        return this
    }

    BasicIssueInputBuilder description(String description) {
        this.description = description
        return this
    }

    BasicIssueInputBuilder priority(Long priority) {
        this.priority = priority
        return this
    }

    BasicIssueInputBuilder projectKey(String projectKey) {
        this.projectKey = projectKey
        return this
    }

    BasicIssueInputBuilder jiraType(String jiraType) {
        this.jiraType = jiraType
        return this
    }

    BasicIssueInputBuilder components(List<String> components) {
        this.components = components
        return this
    }

    BasicIssueInputBuilder fixVersions(List<String> fixVersions) {
        this.fixVersions = fixVersions
        return this
    }

    BasicIssueInputBuilder customFields(Map<String, String> customFields) {
        this.customFields = customFields
        return this
    }

    IssueInput build(Jiranator jiranator) {
        CimProject project = jiranator.findCimProject(projectKey)
        CimIssueType issueType = jiranator.findCimIssueType(jiraType, project)
        IssueInputBuilder builder = new IssueInputBuilder(project, issueType, summary)
        builder.setDescription(description)
        String userId = jiranator.findUserIdByEmail(reporterEmail)
        if(userId) {
            builder.setReporterName(userId)
        }
        if(priority) {
            builder.setPriorityId(priority)
        }
        if(components) {
            builder.setComponentsNames(components)
        }
        if(fixVersions) {
            builder.setFixVersionsNames(fixVersions)
        }

        customFields?.each { String key, String value ->
            builder.setFieldValue(key, value)
        }
        return builder.build();
    }
}
