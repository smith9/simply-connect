package simplyconnect.jira

import java.util.regex.Pattern

/**
 * User: david
 * Date: 03/02/13
 */
interface Credentials {
    String getUser()
    String getPassword()
    URI getBaseJiraUri()
}

