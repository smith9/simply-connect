package simplyconnect.convertor

import groovy.util.logging.Slf4j
import simplyconnect.mail.Accept
import simplyconnect.mail.AcceptAll
import simplyconnect.mail.MessageWrapper

import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * User: david
 * Date: 1/03/13
 * Time: 7:00 PM
 */
@Slf4j
class InlineImageConvertor extends MessageWrapperConvertor {
    static Accept ACCEPT_ALL = new AcceptAll()
    Accept allowImageInline

    InlineImageConvertor(MessageWrapper mailDetails, Accept allowImageInline = ACCEPT_ALL) {
        super(mailDetails)
        this.allowImageInline = allowImageInline
    }

    String convert(String body) {
        try {
            body = insertWikiMarkup(body)
        } catch (Exception exc) {
            log.error("Could not insert wiki markup for ${getMessageDetails().getSummaryDetails()}. Reason: ${exc.getMessage()}", exc)
        }
//        return m.replaceAll('<p>!$1!</p>$0')
        return body
    }

    private String insertWikiMarkup(String body) {
        MessageWrapper details = getMessageDetails()
        Pattern pattern = ~/<img.+src=["']cid:([^"']+)["'].+>/
        Matcher m = pattern.matcher(body)
        m.each {
            String contentId = it[1]
            String imgElement = it[0]
            String imageName = details.getAttachmentName(contentId)
            if (imageName && allowImageInline.accept(imageName)) {
                body = body.replaceAll(imgElement, "<p>!${imageName}!</p>$imgElement")
            }
        }
        return body
    }
}
