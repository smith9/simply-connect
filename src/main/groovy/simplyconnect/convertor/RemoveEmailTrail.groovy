package simplyconnect.convertor

/**
 * User: david
 * Date: 02/02/13
 */
class RemoveEmailTrail implements Convertor {
    String convert(String body) {
        int indexOfFrom = body.indexOf("From:")
        if(indexOfFrom != -1) {
            return body.substring(0, indexOfFrom)
        } else {
            return body
        }
    }

    public static void main(String[] args) {
        new RemoveEmailTrail().convert()
    }
}

