package simplyconnect.convertor

import groovy.util.logging.Slf4j
import org.apache.tika.sax.BodyContentHandler
import org.apache.tika.parser.html.HtmlParser
import org.apache.tika.metadata.Metadata
import org.xml.sax.ContentHandler
import simplyconnect.mail.MessageWrapper
import com.sun.xml.internal.ws.api.message.Message

/**
 * User: david
 * Date: 02/02/13
 */

@Slf4j
class HtmlToPlainText extends MessageExtractor {

    HtmlToPlainText(MessageWrapper message) {
        super(message)
    }

    String convert(String body) {
        if (getMessageDetails().hasHtmlContent()) {
            body = htmlToText(body)
            // replace two or more line endings with a single new line
            // TODO work out what line ending to put
            body = body.replaceAll("[\n\r]{2,}", "\n")
        }
        return body
    }

    private String htmlToText(String body) {
        log.info("Processing html with body length ${body.length()}")
        InputStream input = new ByteArrayInputStream(body.getBytes())
        ContentHandler handler = new BodyContentHandler(-1);
        Metadata metadata = new Metadata();
        new HtmlParser().parse(input, handler, metadata);
        return handler.toString()
    }
}
