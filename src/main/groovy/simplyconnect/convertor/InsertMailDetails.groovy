package simplyconnect.convertor

import simplyconnect.mail.AddressPart
import simplyconnect.mail.MessageWrapper

import javax.mail.Message.RecipientType

/**
 * User: david
 * Date: 02/02/13
 */
class InsertMailDetails extends MessageWrapperConvertor {
    static String NEWLINE = System.getProperty("line.separator")

    InsertMailDetails(MessageWrapper messageDetails) {
        super(messageDetails)
    }

    String convert(String body) {
        StringBuilder details = new StringBuilder()
        details.append("*From:* ").append(messageDetails.getFromAsString(AddressPart.ADDRESS_AND_NAME)).append(NEWLINE)
        details.append("*Received at:* ").append(messageDetails.receivedDate.toString("dd/MM/yyyy HH:mm")).append(NEWLINE)
        List<String> to = messageDetails.getRecipientsAsString(RecipientType.TO, AddressPart.ADDRESS_AND_NAME)
        details.append("*To:* ").append(to.join(", ")).append(NEWLINE)
        List<String> cc = messageDetails.getRecipientsAsString(RecipientType.CC, AddressPart.ADDRESS_AND_NAME)
        if(cc) {
            details.append("*CC:* ").append(cc.join(", ")).append(NEWLINE)
        }
        details.append("*Subject:* ").append(messageDetails.subject).append(NEWLINE).append(NEWLINE)
        return details.toString() + body
    }
}
