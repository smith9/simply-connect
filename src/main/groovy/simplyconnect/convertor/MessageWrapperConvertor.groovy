package simplyconnect.convertor

import simplyconnect.mail.MessageWrapper

/**
 * User: david
 * Date: 02/02/13
 */
abstract class MessageWrapperConvertor implements Convertor {
    private MessageWrapper messageDetails

    MessageWrapperConvertor(MessageWrapper mailDetails) {
        this.messageDetails = mailDetails
    }

    public MessageWrapper getMessageDetails() {
        return messageDetails
    }
}
