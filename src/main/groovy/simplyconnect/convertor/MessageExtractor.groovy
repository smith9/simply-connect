package simplyconnect.convertor

import groovy.util.logging.Slf4j
import org.apache.commons.mail.util.MimeMessageParser
import simplyconnect.mail.MailException
import simplyconnect.mail.MessageWrapper

import javax.mail.Message
import javax.mail.internet.MimeMessage

/**
 * User: david
 * Date: 18/02/13
 * Time: 10:10 PM
 */
@Slf4j
class MessageExtractor extends MessageWrapperConvertor {

    MessageExtractor(MessageWrapper message) {
        super(message)
    }

    String convert(String body) {
        if (messageDetails.isMimeMessage()) {
            return convertMimeMessage()
        }
    }

    String convertMimeMessage() {

        if (messageDetails.hasHtmlContent()) {
            return messageDetails.getHtmlContent()
        } else if (messageDetails.hasPlainContent()) {
            return messageDetails.getPlainContent()
        } else {
            MailException exc = new MailException("Message had no html or plain text content")
            exc.messageWrapper = messageDetails
            throw exc
        }
    }
}
