package simplyconnect.convertor

/**
 * User: david
 * Date: 02/02/13
 */
class ConvertorUtils {
    static String convert(String input, List<Convertor> filters) {
        String convertedInput = input
        filters.each { Convertor filter ->
           convertedInput = filter.convert(convertedInput)
        }
        return convertedInput
    }
}
