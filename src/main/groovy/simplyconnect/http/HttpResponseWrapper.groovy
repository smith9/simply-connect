package simplyconnect.http

import groovy.util.logging.Slf4j
import org.apache.http.HttpResponse
import org.apache.http.client.HttpResponseException
import org.apache.http.util.EntityUtils

/**
 * User: david
 * Date: 27/02/13
 * Time: 11:49 PM
 */
@Slf4j
class HttpResponseWrapper {
    HttpResponse response

    HttpResponseWrapper(HttpResponse response) {
        this.response = response
    }

    int getStatusCode() {
        return response.getStatusLine().statusCode
    }

    String getReasonPhrase() {
        return response.statusLine.reasonPhrase
    }

    void consumeContent() {
        if (response.entity) {
            log.debug("Consuming content")
            EntityUtils.consumeQuietly(response.entity)
        }
    }

    String getContent() {
        if (response.entity) {
            log.debug("getting content")
            return EntityUtils.toString(response.entity)
        } else {
            return ""
        }
    }

    void throwIfNotOk(String prefix = "") {
        if (statusCode < 200 || statusCode > 299) {
            log.debug("Throwing exception for statusCode ${statusCode} and reason ${reasonPhrase}")
            String message = prefix ? "$prefix: $reasonPhrase" : reasonPhrase
            throw new HttpResponseException(statusCode, message)
        }

    }
}
