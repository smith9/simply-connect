package simplyconnect.http

import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPut
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.message.AbstractHttpMessage
import org.apache.http.util.EntityUtils

/**
 * User: david
 * Date: 27/02/13
 * Time: 5:26 PM
 */
@Slf4j
class HttpClientWrapper {
    HttpClient client
    private String user
    private String password
    AuthType authType

    HttpClientWrapper() {
        client  = new DefaultHttpClient()
        authType = AuthType.NONE
    }

    void addBasicAuthHeader(String user, String password) {
        this.user = user
        this.password = password
        this.authType = AuthType.BASIC_HEADER
    }

    public putJson(URI uri, String jsonContent) {
        HttpPut put = new HttpPut(uri)
        applyAuth(put)
        StringEntity input = new StringEntity(jsonContent)
        input.setContentType("application/json")
        put.setEntity(input)
        HttpResponse response = client.execute(put)
        return new HttpResponseWrapper(response)
    }

    public HttpResponseWrapper get(URI uri) {
        HttpGet get = new HttpGet(uri)
        applyAuth(get)
        HttpResponse response = client.execute(get)
        return new HttpResponseWrapper(response)
    }

    public def getJson(URI uri) {
        HttpResponseWrapper response = get(uri)
        response.throwIfNotOk()
        return parseJson(response.content)
    }

    def parseJson(String jsonString) {
        return new JsonSlurper().parseText(jsonString)
    }

    private void applyAuth(AbstractHttpMessage message) {
        if (authType == AuthType.BASIC_HEADER) {
            def login = user + ':' + password
            def basic = "Basic " + login.bytes.encodeBase64().toString()
            message.addHeader('Authorization', basic)
        }
    }
}
