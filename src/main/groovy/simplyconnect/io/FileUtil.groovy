package simplyconnect.io

/**
 * User: david
 * Date: 17/04/13
 * Time: 9:28 PM
 */
class FileUtil {
    static String fileFriendly(String input) {
        return input.replaceAll(/\W+/, "")
    }
}
