package simplyconnect.io.tablefile

/**
 * Created by IntelliJ IDEA.
 * User: Rob Nielsen
 * Date: 3/04/12
 * Time: 4:39 PM
 */

class TableFile<T> {
    Class rowClass
    List<String> titles = []
    Map<Object, T> data = new TreeMap<Object, T>()

    List<TableFile<T>> alignedTables = []

    public TableFile(Class<T> rowClass, List<String> titles, String content = null) {
        this.rowClass = rowClass
        this.titles = titles
        if (content) {
            load(content)
        }
    }

    List<T> getResults() {
        data.values() as List<T>
    }

    public void setTitles(List<String> titles) {
        this.titles = titles
    }

    public void setResults(List<T> rows) {
        data.clear()
        addResults(rows)
    }

    public void addResults(List<T> rows) {
        rows.each { addResult(it) }
    }

    T getResult(Object key) {
        data[key]
    }

    public T addResult(T storable) {
        if (storable) {
            Object key = storable.key ?: data.size()
            if (data.containsKey(key)) {
                throw new RuntimeException("A row already exists for key ${key}")
            }
            data[key] = storable
        }
        return storable
    }

    void alignWith(TableFile<T> otherFile) {
        alignedTables << otherFile
    }

    private List<Integer> getMaxLengths() {
        List<Integer> maxLengths = []
        ([this] + alignedTables).each { TableFile tableFile ->
            ([titles] + data.values()*.toRow()).each { List<String> row ->
                row.eachWithIndex { Object item, int index ->
                    maxLengths[index] = Math.max((maxLengths[index] ?: 0), item?.toString()?.length() ?: 0)
                }
            }
        }
        return maxLengths
    }

    String toString() {
        List<Integer> maxLengths = maxLengths
        StringBuilder sb = new StringBuilder()
        addRow(sb, [], '-', false, '-', maxLengths)
        addRow(sb, titles, ' ', true, '|', maxLengths)
        addRow(sb, [], '-', false, '|', maxLengths)
        data.values().each { storable ->
            addRow(sb, storable.toRow(), ' ', false, '|', maxLengths)
        }
        addRow(sb, [], '-', false, '-', maxLengths)
        return sb.toString()
    }

    private addRow(StringBuilder sb, List<String> row, String pad, boolean center, String columnDivider, List<Integer> maxLengths) {
        sb.append(columnDivider)
        for (int i = 0; i < maxLengths.size(); i++) {
            sb.append(pad)
            String item = row[i] ?: ''
            int maxLength = getMaxLength(i)
            if (center) {
                sb.append(item.center(maxLength, pad))
            } else {
                sb.append(item.padRight(maxLength, pad))
            }
            sb.append(pad).append(columnDivider)
        }
        sb.append('\r\n')
    }

    public void load(File resultFile) {
        if (resultFile.exists()) {
            load(resultFile.text)
        }
        def currentText = toString()
        if (!resultFile.exists() || resultFile.text != currentText) {
            resultFile.text = currentText;
        }
    }

    public void load(String resultText) {
        resultText.eachLine { String line, index ->
            line = line.trim()
            if (line.startsWith('-')) {
                return
            }
            line = asciify(line)
            List<String> elements = split(line)
            if (elements && elements != titles && !(elements[0] =~ /^\-+$/)) {
                while (elements.size() < titles.size()) {
                    elements << ''
                }
                try {
                    addResult((T) rowClass.newInstance([elements] as Object[]))
                } catch(Exception e) {
                    throw new RuntimeException("Error parsing line ${index}\nLine Text: \"${line}\"\nElements:  ${elements}\nTitles: ${titles}\n${e.toString()}", e)
                }
            }
        }
    }

    static String asciify(String s) {
        s.replaceAll(/[\p{Z}]/, " ").replaceAll(/[\p{Pd}]/, "-")
    }

    private List<String> split(String line) {
        line = line.replaceFirst(/^\|?(.*?)\|?$/, /$1/)
        List<String> elements = line.split(/(?<!\|)\|(?!\|)/)
        if (elements.size() == 1) {
            elements = line.split('\t')
        }
        elements = elements.collect { it.trim() }
        int lastNonEmpty = elements.findLastIndexOf { it }
        return elements.subList(0, lastNonEmpty + 1)
    }

    public void store(File file) {
        file.parentFile.mkdirs()
        file.text = toString()
    }


    private int getMaxLength(int index) {
        maxLengths[index] ?: 0
    }


    private String lineSep() {
        return '\r\n'
    }

    void filter(List keys) {
        if (keys) {
            assert data.keySet().retainAll(keys)
        }
    }

    void filter(Closure closure) {
        for(Iterator it = data.keySet().iterator(); it.hasNext();) {
            Object key = it.next()
            T value = data[key]
            if (!closure.call(value)) {
                it.remove()
            }
        }
    }
}
