package simplyconnect.io.tablefile

/**
 * Created by IntelliJ IDEA.
 * User: Rob Nielsen
 * Date: 11/04/12
 * Time: 7:39 AM
 * To change this template use File | Settings | File Templates.
 */
interface PipeStorable {
    Object getKey()
    List<String> toRow()
}

// example
//class Stationery implements PipeStorable {
//    String stockCode
//    String name
//    String description
//    StationeryType type
//    String barcode
//
//    public Stationery() {
//    }
//
//    public Stationery(List<String> row) {
//        this(row[0], row[1], row[2], (StationeryType)StationeryType.valueOf(row[3]), row[4])
//    }
//
//    public Stationery(String stockCode, String name, String description, StationeryType type, String barcode) {
//        this.stockCode = stockCode
//        this.name = name
//        this.description = description
//        this.type = type
//        this.barcode = barcode ?: ''
//    }
//
//    boolean isValid(){
//        return stockCode && name && description && type
//    }
//
//    static List<String> getColumnNames() {
//        ["Stock Code", "Name", "Description", "Type", "Barcode"]
//    }
//
//    String toString() {
//        toRow().join(' | ')
//    }
//
//    @Override
//    Object getKey() {
//        return null//"${type}:${stockCode}"
//    }
//
//    @Override
//    List<String> toRow() {
//        return [stockCode, name, description, type?.name() ?: StationeryType.INSERT, barcode]
//    }
//
//    static TableFile<Stationery> loadTableFile(File contentFile) {
//        TableFile<Stationery> tableFile = new TableFile<Stationery>(Stationery.class, columnNames);
//        tableFile.load(contentFile)
//        return tableFile
//    }
//}
