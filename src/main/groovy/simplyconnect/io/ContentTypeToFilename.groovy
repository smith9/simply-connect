package simplyconnect.io

import org.apache.tika.mime.MimeTypeException
import org.apache.tika.mime.MimeTypes
import org.apache.tika.mime.MimeType
import org.apache.tika.mime.MimeTypesFactory

/**
 * User: u321869
 * Date: 8/03/13
 */
class ContentTypeToFilename {
    public String generate(String contentType) {
        String filename
        try {
            filename = contentType.replaceAll(/\W+/, "") + getExtension(contentType)
        } catch (MimeTypeException exc) {
            filename = "unknown.unknown"
        }
        return filename
    }

    String getExtension(String contentType) {
        MimeType type = MimeTypes.getDefaultMimeTypes().getRegisteredMimeType(contentType)
        if(type?.extension) {
            return type.extension
        } else {
            return "unknown"
        }
    }
}
