package simplyconnect.io

import org.apache.commons.io.FileUtils
import org.apache.commons.io.FilenameUtils
import org.joda.time.DateTime

import javax.activation.DataSource
import java.security.DigestInputStream
import java.security.MessageDigest

/**
 * User: david
 * Date: 25/02/13
 * Time: 5:20 PM
 */
class ChecksumGenerator {
    File outputDir
    String digestAlgorithm = "MD5"
    Closure generateFile = { File dir, DataSource dataSource ->
        outputDir.mkdirs()
        File attachmentFile = generateFilename(dir, dataSource)
        if (attachmentFile.exists()) {
            String prefix = FilenameUtils.getBaseName(attachmentFile.name)
            String extension = FilenameUtils.getExtension(attachmentFile.name)
            attachmentFile = File.createTempFile(prefix, ".$extension", dir)
        }
        return attachmentFile

    }

    File generateFilename(File dir, DataSource dataSource) {
        String filename = dataSource.name
        if(!filename) {
            // not every attachment has a name set so generate one
            filename = new ContentTypeToFilename().generate(dataSource.contentType)
        }
        return new File(outputDir, filename)
    }

    Map<String, File> generate(DataSource... attachments) {
        DateTime.now()
        MessageDigest checksumCalc = MessageDigest.getInstance(digestAlgorithm)
        Map<String, File> checksumsAndAttachments = [:]
        attachments.each { DataSource attachment ->
            DigestInputStream input = new DigestInputStream(attachment.inputStream, checksumCalc)
            File attachmentFile = copyToFile(outputDir, attachment, input)
            checksumsAndAttachments.put(toHex(checksumCalc.digest()), attachmentFile)
        }
        return checksumsAndAttachments
    }
    // TODO is there an open source class that already does this?
    private String toHex(byte[] digest) {
        List<String> hex = digest.collect { Byte digestByte ->
            Integer.toHexString((int) (digestByte & 0xff)).padLeft(2, "0")
        }
        return hex.join("")
    }

    private File copyToFile(File outputDir, DataSource attachment, DigestInputStream input) {
        File attachmentFile = generateFile(outputDir, attachment)
        FileUtils.copyInputStreamToFile(input, attachmentFile)
        return attachmentFile
    }

}
