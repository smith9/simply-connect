package simplyconnect.mail

/**
 * User: david
 * Date: 02/02/13
 */
public enum AddressPart {
    ADDRESS,
    NAME,
    ADDRESS_AND_NAME
}