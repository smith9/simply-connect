package simplyconnect.mail

import org.apache.commons.io.IOUtils

import javax.activation.DataSource
import javax.mail.Address
import javax.mail.BodyPart
import javax.mail.internet.InternetAddress

/**
 * User: david
 * Date: 02/02/13
 */
class MailUtils {
    static String getContent(BodyPart part) {
        if (part.isMimeType("text/*")) {
            return part.getContent()
        } else {
            // TODO fix this up, what if it is an image...only wanting text
            // is there some open source that will get the content of a part like hte MimeMessageParser
            return part.getInputStream().text
        }
    }
    static List<DataSource> filterAttachments(List<DataSource> dataSources, Accept nameAccept) {
            return dataSources.findAll { DataSource dataSource ->
                nameAccept.accept(dataSource.name)
            }
        }

    static List<MessageWrapper> filterMessages(List<MessageWrapper> messages, Set<String> keywords) {
        return messages.findAll { MessageWrapper message ->
            message.hasAnyKeyword(keywords)
        }
    }

    static List<String> addressesToStrings(Address[] addresses, AddressPart part = AddressPart.ADDRESS) {
        List<String> collect = addresses.collect { Address address ->
            addressToString(address, part)}
        return collect
    }
    static String addressToString(Address address, AddressPart addressPart) {
        if (address == null) {
            return null
        } else if(address instanceof InternetAddress) {
            internetAddressToString(address as InternetAddress, addressPart)
        } else {
            // TODO if the address is not an InternetAddress do we need to parse out the name or address parts?
            return address.toString()
        }
    }

    static String internetAddressToString(InternetAddress address, AddressPart part) {
        String addressString
        switch(part) {
            case AddressPart.ADDRESS:
                addressString = address.address
                break
            case AddressPart.NAME:
                addressString = address.personal
                break
            default:
                addressString = address.toString()
                break
        }
        return addressString
    }

}
