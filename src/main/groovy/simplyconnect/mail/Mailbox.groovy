package simplyconnect.mail

import javax.mail.Folder
import javax.mail.*
import groovy.util.logging.Slf4j

import javax.mail.event.MessageCountEvent
import javax.mail.event.MessageCountListener
import javax.mail.internet.MimeMessage
import javax.mail.Flags.Flag
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMultipart
import javax.activation.DataHandler
import javax.mail.util.ByteArrayDataSource

/**
 * User: david
 * Date: 02/02/13
 */
@Slf4j
class Mailbox {
    Store store
    boolean debug = false
    boolean expungeImmediatelyOnDeleteMessage = true
    boolean expungeImmediatelyOnMoveMessage = true
    @Delegate FolderCache folders

    void connect(String user, String password, String host, String protocol, Properties properties = new Properties()) {
        initialiseStore(properties, protocol)
        log.info("Opening mail account ${user} on ${host}")
        store.connect(host, user, password)
    }

    void connect(String user, String password, String host, int port, String protocol, Properties properties = new Properties()) {
        initialiseStore(properties, protocol)
        log.info("Opening mail account ${user} on ${host}:${port}")
        store.connect(host, port, user, password)
    }

    private void initialiseStore(Properties properties, String protocol) {
        // don't call getDefaultInstance as per Oracle's recommendation so that you get a new session each time
        final Session session = Session.getInstance(properties)
        session.setDebug(debug)
        store = session.getStore(protocol)
        folders = new FolderCache(store)
    }

    void disconnect(boolean expunge = false) {
        folders.closeAll(expunge)
        log.info("closing mailbox")
        store.close()
    }

    List<MessageWrapper> retrieveMessages(String inboxPath) {
        Folder inbox = folders.open(inboxPath)
        return inbox.getMessages().collect { Message message ->
            new MessageWrapper(message)
        }.sort { it.receivedDate }

//        }.sort { MessageWrapper wrapperA, MessageWrapper wrapperB ->
//            return wrapperB.receivedDate <=> wrapperA.receivedDate
//        }
    }

    void moveMessage(Message message, String toFolderPath, boolean createIfNotExist = false, int waitTimeout = 2000) {
        Message[] messages = [message]
        Folder fromFolder = message.folder
        Folder toFolder = ensureFolderExists(toFolderPath, createIfNotExist)
        log.info("moving message subject: ${message.subject} from ${fromFolder.name} to ${toFolder.name}")
        monitorMove(toFolder)
        fromFolder.copyMessages(messages, toFolder)
        waitForMoveCompletion(toFolder, waitTimeout)
        fromFolder.setFlags(messages, new Flags(Flags.Flag.DELETED), true)
        // we could wait until the folder is closed to do the expunge but seems more correct to do it as we go
        // plus had some issues in unit tests where some emails stayed in the inbox but had a deleted flag
        // meaning the expunge didn't seem to work properly. This perhaps may just have been greenmail?
        // the following works:
        if(expungeImmediatelyOnMoveMessage) {
            fromFolder.expunge()
        }
    }
    void deleteMessage(Message message) {
        Folder fromFolder = message.folder
        log.info("deleting message subject: ${message.subject} from ${fromFolder.name}")
        // TODO do we need to wait for the deletion?
        Message[] messages = [message]
        fromFolder.setFlags(messages, new Flags(Flags.Flag.DELETED), true)
        // we could wait until the folder is closed to do the expunge but seems more correct to do it as we go
        // plus had some issues in unit tests where some emails stayed in the inbox but had a deleted flag
        // meaning the expunge didn't seem to work properly. This perhaps may just have been greenmail?
        // the following works:
        if(expungeImmediatelyOnDeleteMessage) {
            fromFolder.expunge()
        }

    }
    private Folder ensureFolderExists(String toFolderPath, boolean createIfNotExists) {
        if(createIfNotExists && !exists(toFolderPath)) {
            create(toFolderPath)
        }
        return folders.open(toFolderPath)
    }

    private void monitorMove(Folder folder) {
        folder.addMessageCountListener(new MessageCountListener() {
            void messagesAdded(MessageCountEvent e) {
                synchronized (folder) {
                    folder.notify()
                }
            }

            void messagesRemoved(MessageCountEvent e) {
            }
        })
    }

    private void waitForMoveCompletion(Folder folder, int waitTimeout) {
        synchronized (folder) {
            folder.wait(waitTimeout)
        }
    }

    public Message createDraft(String folder, String subject, String body, String contentType = "text/plain; charset=utf-8") {
        Message message = null
        try {
            message = new MimeMessage(Session.getInstance(System.getProperties()))
            message.setSubject(subject)
            message.setFlag(Flag.DRAFT, true);
            MimeBodyPart mimeBodyPart = new MimeBodyPart()
            Multipart multiPart = new MimeMultipart()
            mimeBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(body, contentType)))
            multiPart.addBodyPart(mimeBodyPart)
            message.setContent(multiPart, contentType)

            folders.open(folder).appendMessages(message)
        } catch (MessagingException exc) {
            log.error("Could not create draft message with subject $subject in folder $folder")
            throw exc
        }
        return message
    }
}
