package simplyconnect.mail

import org.apache.commons.mail.util.MimeMessageUtils
import simplyconnect.io.FileUtil

import javax.mail.Message
import javax.mail.internet.MimeMessage

/**
 * User: david
 * Date: 17/04/13
 * Time: 8:38 AM
 */
class MailFileUtils {
    static Closure CREATE_FILENAME = { MessageWrapper message ->
        List<String> parts = [message.getMessageId(),
                message.getSubject(),
                "from",
                message.getFromAsString(),
                "to",
                message.getToAsString(),
                ] as List<String>
        String fileFriendlyParts = parts.collect { String part ->
            return FileUtil.fileFriendly(part)
        }.join("-")
        return "id-${fileFriendlyParts}.eml"
    }
    static List<MessageWrapper> toWrapper(List<Message> messages) {
        return messages.collect { new MessageWrapper(it)}
    }

    static List<MessageWrapper> load(List<File> emlFiles) {
        return emlFiles.collect { File emlFile ->
            return loadMessage(emlFile)
        }
    }

    static MessageWrapper loadMessage(File emlFile) {
        return createMessage(emlFile.text)
    }

    static MessageWrapper createMessage(String emlFileContent) {
        MimeMessage message = MimeMessageUtils.createMimeMessage(null, emlFileContent)
        return new MessageWrapper(message)
    }

    static List<File> save(List<MessageWrapper> messages, File outputDir, Closure createFilename = CREATE_FILENAME) {
        return messages.collect { MessageWrapper message ->
            File outputFile = new File(outputDir, createFilename(message))
            FileOutputStream out = new FileOutputStream(outputFile)
            message.writeRawMessage(out)
            return outputFile
        }
    }
}
