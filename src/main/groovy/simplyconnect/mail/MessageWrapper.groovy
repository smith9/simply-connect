package simplyconnect.mail

import org.apache.commons.io.IOUtils
import org.apache.commons.mail.util.MimeMessageParser
import org.joda.time.DateTime
import simplyconnect.convertor.HtmlToPlainText
import simplyconnect.convertor.MessageExtractor

import javax.activation.DataSource
import javax.mail.Address
import javax.mail.BodyPart
import javax.mail.Header
import javax.mail.Message
import javax.mail.Message.RecipientType
import javax.mail.Multipart
import javax.mail.Part
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart
import javax.mail.internet.MimeBodyPart

/**
 * User: david
 * Date: 02/02/13
 */
class MessageWrapper implements GetHeader{
    Message message
    MimeMessageParser messageParser
    private InlineImageHandler inlineImageHandler
    private String htmlWithoutMarkup

    MessageWrapper(Message message) {
        this.message = message
    }

    String rawMessage() {
        ByteArrayOutputStream out = new ByteArrayOutputStream()
        message.writeTo(out)
        out.toString()
    }
    void writeRawMessage(OutputStream out) {
        message.writeTo(out)
    }

    Address getFrom() {
        return message.from?.first()
    }

    Address getReplyTo() {
        return message.replyTo?.first()
    }

    String getFromAsString(AddressPart part = AddressPart.ADDRESS) {
        Address firstFrom = getFrom()
        return MailUtils.addressToString(firstFrom, part)
    }

    String getReplyToAsString(AddressPart part = AddressPart.ADDRESS) {
        Address replyTo = getReplyTo()
        return MailUtils.addressToString(replyTo, part)
    }

    private List<Address> getRecipients(RecipientType type) {
        Address[] recipients = message.getRecipients(type)
        return recipients ? recipients : []
    }

    List<Address> getTo() {
        return getRecipients(RecipientType.TO)
    }

    List<Address> getCc() {
        return getRecipients(RecipientType.CC)
    }

    List<Address> getBcc() {
        return getRecipients(RecipientType.BCC)
    }

    List<String> getToAsString(AddressPart part = AddressPart.ADDRESS) {
        return getRecipientsAsString(RecipientType.TO, part)
    }

    List<String> getCcAsString(AddressPart part = AddressPart.ADDRESS) {
        return getRecipientsAsString(RecipientType.CC, part)
    }

    List<String> getBccAsString(AddressPart part = AddressPart.ADDRESS) {
        return getRecipientsAsString(RecipientType.BCC, part)
    }

    private List<String> getRecipientsAsString(RecipientType type, AddressPart part) {
        Address[] recipients = message.getRecipients(type)
        return MailUtils.addressesToStrings(recipients, part)
    }

    String getSubject() {
        return message.subject
    }

    String getContent() {
        return message.getContent()
    }

    DateTime getReceivedDate() {
        return new DateTime(message.receivedDate)
    }

    List<String> getHeaders() {
        List<String> headers = message.getAllHeaders().collect { Header header ->
            return "${header.name}: ${header.value}"
        }
        return headers
    }
    Set<String> getHeaderKeys() {
        List<String> headerKeys = message.getAllHeaders().collect { Header header ->
            return header.name
        }
        return headerKeys as Set<String>
    }
    Set<String> getKeywords() {
        Set<String> allKeywords = new TreeSet<>()
        String[] headerKeywords = message.getHeader("Keywords")
        headerKeywords.each { String keywords ->
            keywords.split(",").each { String keyword ->
                allKeywords << keyword.trim()
            }
        }
        return allKeywords
    }

    boolean hasKeyword(String keyword) {
        return getKeywords().contains(keyword)
    }

    boolean isMimeMessage() {
        return message instanceof MimeMessage
    }

    MimeMessage getMimeMessage() {
        if (message instanceof MimeMessage) {
            return message
        } else {
            throw new IllegalStateException("Message is not of the requested type, expected was MimeMessage, actual was ${message.class.name}, perhaps call isMimeMessage()")
        }
    }

    public boolean isMultipart() {
        parseMimeMessage()
        return messageParser.isMultipart()
    }

    public List<DataSource> getAttachmentList() {
        parseMimeMessage()
        return messageParser.getAttachmentList()
    }

    public String getHtmlContentWithoutMarkup() {
        if (!htmlWithoutMarkup) {
            // TODO perhaps allow callers to specify which extractors to use
            String messageText = new MessageExtractor(this).convert(null)
            htmlWithoutMarkup = new HtmlToPlainText(this).convert(messageText)
        }
        return htmlWithoutMarkup
    }

    public boolean hasHtmlContent() {
        parseMimeMessage()
        return messageParser.hasHtmlContent()
    }

    public String getHtmlContent() {
        parseMimeMessage()
        return messageParser.getHtmlContent()
    }

    public boolean hasPlainContent() {
        parseMimeMessage()
        return messageParser.hasPlainContent()
    }

    public String getPlainContent() {
        parseMimeMessage()
        return messageParser.getPlainContent()
    }

    private void parseMimeMessage() {
        if (!messageParser) {
            messageParser = new MimeMessageParser(getMimeMessage())
            messageParser.parse()
        }
    }

    InlineImageHandler getInlineImageHandler() {
        initImageHandler()
        return inlineImageHandler
    }

    void initImageHandler() {
        if (!inlineImageHandler) {
            parseMimeMessage()
            inlineImageHandler = new InlineImageHandler(this)
        }
    }

    boolean hasAnyKeyword(Set<String> keywords) {
        return !keywords.intersect(getKeywords()).isEmpty()
    }

    /**
     * @deprecated
     * @see #getPartByContentId(java.lang.String)
     */
    BodyPart getPart(String contentId) {
        return getPartByContentId(contentId)
    }

//    private BodyPart findInMultipart(String cid, MimeMultipart multipart) {
//        BodyPart foundPart = null
//        int partCount = multipart.getCount()
//        for (int p = 0; p < partCount; p++) {
//            BodyPart currentPart = findPart(cid, multipart.getBodyPart(p))
//            if (currentPart) {
//                foundPart = currentPart
//                break
//            }
//        }
//        return foundPart
//    }

//    private BodyPart findPart(String cid, Part part) {
//        BodyPart foundPart = null
//        if (match(cid, part)) {
//            foundPart = part
//        } else {
//            Object content = part.getContent()
//            if (content instanceof MimeMultipart) {
//                foundPart = findInMultipart(cid, content)
//            }
//        }
//        return foundPart
//    }

//    private boolean match(String cid, Part part) {
//        boolean match = false
//        if (part instanceof MimeBodyPart) {
////            println "MessageWrapper.match - is a MimeBodyPart"
////            MimeBodyPart mbp = part as MimeBodyPart
////            println "content id header = ${mbp.getHeader("Content-ID")}"
////            Enumeration lines = mbp.getAllHeaderLines()
////            for (String line : lines) {
////                println line
////            }
////            println "-------------------------"
//        }
////        if (part instanceof MimeBodyPart && (part as MimeBodyPart).getContentID() == cid) {
//        if (part instanceof MimeBodyPart && (part as MimeBodyPart).getHeader("Content-ID")?.first() == cid) {
////            println "matched"
//            match = true
//        } else {
////            println "did not match"
//        }
//        return match
//    }

//    BodyPart getPart(String contentId) {
//        if (contentId) {
//            return findPart(ensureIdFormat(contentId), message)
//        } else {
//            return null
//        }
//    }

    String ensureIdFormat(String contentId) {
        return contentId.startsWith("<") ? contentId : "<$contentId>"
    }

    public boolean hasAttachments() {
        parseMimeMessage()
        return messageParser.hasAttachments()
    }

    public DataSource findAttachmentByName(String name) {
        parseMimeMessage()
        return messageParser.findAttachmentByName(name)
    }

    String getAttachmentName(String cid) {
        return getPart(cid)?.getFileName()
    }

    String getSummaryDetails() {
        return "Subject: ${getSubject()} from ${getFromAsString()} received at ${getReceivedDate().toString("dd/MM/yyyy hh:mm")}"
    }

    boolean isMultiPartReport() {
        return message.isMimeType("multipart/report")
    }

    boolean isDeliveryStatusReportType() {
        return message.getContentType().contains("report-type=delivery-status")
    }

    String getDeliveryStatusContent() {
        String content = ""
        // delivery status should be a body part
        BodyPart statusPart = (BodyPart) getDeliveryStatusPart()
        if (statusPart) {
            content = MailUtils.getContent(statusPart)
        }
        return content
    }

    Part getDeliveryStatusPart() {
        List<Part> parts = getPartsByContentType("message/delivery-status")
        if (parts) {
            return parts.first()
        } else {
            return null
        }
    }

    List<Part> getPartsByContentType(String contentType) {
        List<Part> parts = []
        collectParts(message, parts) { Part part ->
            part.isMimeType(contentType)
        }
        return parts;
    }
    BodyPart getPartByContentId(String contentId) {
        String idFormatted = ensureIdFormat(contentId)
        List<Part> parts = []
        collectParts(message, parts) { Part part ->
            part instanceof MimeBodyPart && (part as MimeBodyPart).getHeader("Content-ID")?.first() == idFormatted
        }
        return parts ? parts.first() as BodyPart : null;
    }
    void collectParts(Multipart multipart, List<Part> parts, Closure match) {
        int partCount = multipart.getCount()
        partCount.times { int count ->
            Part childPart = multipart.getBodyPart(count)
            collectParts(childPart, parts, match)
        }
    }
    void collectParts(Part part, List<Part> parts, Closure match) {
        if (part.isMimeType("multipart/*")) {
            collectParts((Multipart)part.getContent(), parts, match)
        } else {
            addPartIfMatch(part, parts, match)
        }
    }
    void addPartIfMatch(Part part, List<Part> parts, Closure match) {
        if (match(part)) {
            parts << part
        }
    }

    List<String> getHeader(String name) {
        return message.getHeader(name) as List<String>
    }
    String getFirstHeader(String name) {
        String[] headerValues = message.getHeader(name)
        return headerValues?.first()
    }

    String getMessageId() {
        return getFirstHeader("Message-ID")
    }
}