package simplyconnect.mail

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

/**
 * User: david
 * Date: 4/03/13
 * Time: 5:41 PM
 */
class InlineImageHandler {
    Set<ImageDetails> imageDetails = []
    InlineImageHandler(MessageWrapper messageWrapper) {
        this.messageWrapper = messageWrapper
        extract()
    }

    MessageWrapper messageWrapper
    Set<ImageDetails> extract() {
        if (messageWrapper.hasHtmlContent()) {
            imageDetails = extractInlineImages(messageWrapper.htmlContent)
            addAttachmentNames()
        }
        return imageDetails
    }

    void addAttachmentNames() {
        imageDetails.each { ImageDetails detail ->
            String filename = messageWrapper.getAttachmentName(detail.cid)
            detail.attachmentName = filename
        }
    }

    static Set<ImageDetails> extractInlineImages(String html) {
        Set<ImageDetails> imageDetails = []
        Document doc = Jsoup.parse(html)

        Elements imgs = doc.select("img")
        imgs.each { Element img ->
            println "img = $img"
            ImageDetails detail = new ImageDetails()
            detail.alt = img.attr("alt")
            detail.src = img.attr("src")
            extractCid(detail)
            imageDetails << detail
        }

        return imageDetails
    }

    static void extractCid(ImageDetails detail) {
        if (detail.src.startsWith("cid")) {
            detail.cid = detail.src.split(":")[1]
        }
    }

    ImageDetails getImageByCid(String cid) {
        return imageDetails.find { it.cid == cid };
    }
    ImageDetails getImageByAttachmentName(String attachmentName) {
        return imageDetails.find { it.attachmentName == attachmentName };
    }
}
