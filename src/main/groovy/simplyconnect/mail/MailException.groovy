package simplyconnect.mail

/**
 * User: david
 * Date: 02/02/13
 */
class MailException extends RuntimeException {
    MessageWrapper messageWrapper

    MailException() {
        super()
    }

    MailException(Throwable cause) {
        super(cause)
    }

    MailException(String message) {
        super(message)
    }

    MailException(String message, Throwable cause) {
        super(message, cause)
    }

    protected MailException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace)
    }

    public String getMessageDetails() {
        if (messageWrapper) {
            return "From: ${messageWrapper.getFromAsString()} Subject: ${messageWrapper.getSubject()}"
        }
    }
}
