package simplyconnect.mail.bounce.categoryfile

import simplyconnect.io.tablefile.TableFile
import simplyconnect.mail.MessageWrapper
import simplyconnect.mail.bounce.categoriser.Categoriser
import simplyconnect.mail.bounce.categoriser.CategoryResult
import simplyconnect.mail.bounce.categoriser.MessageResults


/**
 * User: david
 * Date: 17/04/13
 * Time: 8:21 AM
 */
class CategoriserUtil {
    static List<MessageResults> categorise(Categoriser categoriser, List<MessageWrapper> messages) {
        return messages.collect { MessageWrapper message ->
            return categoriser.categorise(message)
        }
    }

    static List<MessageResult> toCategorisedResult(List<MessageResults> categorisedMessages) {
        List<MessageResult> categorisedResults = []
        categorisedMessages.each { MessageResults categorisedMessage ->
            categorisedMessage.results.eachWithIndex { CategoryResult result, int index ->
                categorisedResults << new MessageResult(message: categorisedMessage.message, result: result, index: index)
            }
        }
        return categorisedResults
    }

    static File toTableFile(List<MessageResult> messageResultList, File outputDir, String filename = "messages.txt") {
        TableFile table = new TableFile(MessageResult, MessageResult.getTitles())
        table.addResults(messageResultList)
        File tableFile = new File(outputDir, filename)
        table.store(tableFile)
        return tableFile
    }
}
