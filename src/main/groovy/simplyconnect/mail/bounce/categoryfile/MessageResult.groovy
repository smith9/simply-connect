package simplyconnect.mail.bounce.categoryfile

import simplyconnect.io.tablefile.PipeStorable
import simplyconnect.mail.MessageWrapper
import simplyconnect.mail.bounce.categoriser.CategoryResult

/**
 * User: david
 * Date: 17/04/13
 * Time: 10:14 PM
 */
class MessageResult implements PipeStorable {
    MessageWrapper message
    CategoryResult result
    int index

    static List<String> getTitles() {
        return ["Message Id", "Index", "Type", "Category", "Sub Category", "Action", "Status Code", "Email Address"];
    }

    Object getKey() {
        return message.getMessageId() + "-" + index
    }

    List<String> toRow() {
        return [message.getMessageId(), index, result.type, result.category, result.subCategory, result.action, result.statusCode?.status, result.emailAddress]
    }
}
