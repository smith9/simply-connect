package simplyconnect.mail.bounce.deliverystatus

import simplyconnect.mail.fields.FieldParser
import simplyconnect.mail.fields.Field
import simplyconnect.mail.fields.NameValueField
import simplyconnect.mail.fields.SeparatorField

/**
 * User: david
 * Date: 27/03/13
 * Time: 5:58 PM
 */
class DeliveryStatusParser {
    DeliveryStatusNotification notification = new DeliveryStatusNotification()
    DsnState state = DsnState.INITIAL
    RecipientFields currentRecipientFields

    DeliveryStatusNotification parse(String deliveryStatusContent) {
        List<Field> fields = new FieldParser().parse(deliveryStatusContent)

        fields.each { Field field ->
            handleField(field)
        }
        return notification
    }

    void handleField(Field field) {
        calculateState(field)
        createRecipientIfFirstRecipientField()

        if (field instanceof NameValueField) {
            switch (state) {
                case DsnState.PROCESSING_MESSAGE_FIELDS:
                    notification.addField(field)
                    break
                case DsnState.PROCESSING_FIRST_RECIPIENT_FIELD:
                case DsnState.PROCESSING_REMAINING_RECIPIENT_FIELDS:
                    currentRecipientFields.addField(field)
                    break
            }
        }
    }

    private void createRecipientIfFirstRecipientField() {
        if (state == DsnState.PROCESSING_FIRST_RECIPIENT_FIELD) {
            currentRecipientFields = new RecipientFields()
            notification.addRecipient(currentRecipientFields)
        }
    }

    void calculateState(Field field) {
        boolean isNamedField = field instanceof NameValueField
        boolean isSeparatorField = field instanceof SeparatorField
        switch (state) {
            case DsnState.INITIAL:
                if (isNamedField) {
                    state = DsnState.PROCESSING_MESSAGE_FIELDS
                }
                break
            case DsnState.PROCESSING_MESSAGE_FIELDS:
            case DsnState.PROCESSING_REMAINING_RECIPIENT_FIELDS:
                if (isSeparatorField) {
                    state = DsnState.RECIPIENT_SEPARATOR
                }
                break
            case DsnState.PROCESSING_FIRST_RECIPIENT_FIELD:
                if (isNamedField) {
                    state = DsnState.PROCESSING_REMAINING_RECIPIENT_FIELDS
                } else if (isSeparatorField) {
                    state = DsnState.RECIPIENT_SEPARATOR
                }
                break
            case DsnState.RECIPIENT_SEPARATOR:
                if (isNamedField) {
                    state = DsnState.PROCESSING_FIRST_RECIPIENT_FIELD
                }
                break
        }
    }
}
