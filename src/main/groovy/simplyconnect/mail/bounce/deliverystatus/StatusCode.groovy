package simplyconnect.mail.bounce.deliverystatus

/**
 * User: david
 * Date: 29/03/13
 * Time: 8:01 PM
 */
class StatusCode {
    String status

    StatusCode(String status) {
        this.status = status
    }

    /**
     *
     * @return e.g. the 5 from 5.7.3
     */
    String getStatusClass() {
        return getStatusPart(0)
    }
    /**
     *
     * @return e.g. the 7 from 5.7.3
     */
    String getStatusSubject() {
        return getStatusPart(1)
    }
    /**
     *
     * @return e.g. the 3 from 5.7.3
     */
    String getStatusDetail() {
        return getStatusPart(2)
    }

    /**
     *
     * @return e.g. the 7.3 from 5.7.3
     */
    String getStatusSubjectAndDetail() {
        // split 5.1.2 into 5 and 1.2
        String[] twoParts = status.split("[.]", 2)
        return twoParts[1]
    }

    private String getStatusPart(int i) {
        String[] parts = status.split("[.]")
        return parts[i]
    }


}
