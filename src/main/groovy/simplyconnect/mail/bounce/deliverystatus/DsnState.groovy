package simplyconnect.mail.bounce.deliverystatus

/**
 * User: david
 * Date: 16/04/13
 * Time: 10:13 PM
 */
public enum DsnState {
    INITIAL,
    PROCESSING_MESSAGE_FIELDS,
    PROCESSING_REMAINING_RECIPIENT_FIELDS,
    PROCESSING_FIRST_RECIPIENT_FIELD,
    RECIPIENT_SEPARATOR
}