package simplyconnect.mail.bounce.deliverystatus

import simplyconnect.mail.fields.Field
import simplyconnect.mail.fields.Field
import simplyconnect.mail.fields.FieldMap

import java.util.regex.Matcher

/**
 * User: david
 * Date: 28/03/13
 * Time: 7:32 AM
 [ original-recipient-field CRLF ]
           final-recipient-field CRLF
           action-field CRLF
           status-field CRLF
           [ remote-mta-field CRLF ]
           [ diagnostic-code-field CRLF ]
           [ last-attempt-date-field CRLF ]
           [ final-log-id-field CRLF ]
           [ will-retry-until-field CRLF ]
            *( extension-field CRLF )
 */
class RecipientFields {
    public static final String ORIGINAL_RECIPIENT_FIELD = "Original-Recipient"
    public static final String FINAL_RECIPIENT_FIELD = "Final-Recipient"
    public static final String ACTION_FIELD = "Action"
    public static final String STATUS_FIELD = "Status"
    public static final String REMOTE_MTA_FIELD = "Remote-MTA"
    public static final String DIAGNOSITC_CODE_FIELD = "Diagnostic-Code"
    public static final String LAST_ATTEMPT_DATE_FIELD = "Last-Attempt-Date"
    public static final String FINAL_LOG_ID_FIELD = "Final-Log-Id"
    public static final String WILL_RETRY_UNTIL_FIELD = "Will-Retry-Until"

    static final Set<String> RECIPIENT_FIELDS = [ORIGINAL_RECIPIENT_FIELD,
    FINAL_RECIPIENT_FIELD,
    ACTION_FIELD,
    STATUS_FIELD,
    REMOTE_MTA_FIELD,
    DIAGNOSITC_CODE_FIELD,
    LAST_ATTEMPT_DATE_FIELD,
    FINAL_LOG_ID_FIELD,
    WILL_RETRY_UNTIL_FIELD].collect {it.toLowerCase()}
    FieldMap fields = new FieldMap()
    void addField(Field field) {
        fields.add(field)
    }
    Field get(String name) {
        return fields.get(name)
    }
    Field getFinalRecipientField() {
        return fields.get(FINAL_RECIPIENT_FIELD)
    }

    Field getOriginalRecipientField() {
        return fields.get(ORIGINAL_RECIPIENT_FIELD)
    }

    Field getActionField() {
        return fields.get(ACTION_FIELD)
    }

    Field getStatusField() {
        return fields.get(STATUS_FIELD)
    }
    Field getLastAttemptDateField() {
        return fields.get(LAST_ATTEMPT_DATE_FIELD)
    }
    Field getFinalLogIdField() {
        return fields.get(FINAL_LOG_ID_FIELD)
    }
    Field getWillRetryUntilField() {
        return fields.get(WILL_RETRY_UNTIL_FIELD)
    }

    Field getRemoteMtaField() {
        return fields.get(REMOTE_MTA_FIELD)
    }

    Field getDiagnosticCodeField() {
        return fields.get(DIAGNOSITC_CODE_FIELD)
    }

    StatusCode getStatusCode() {
        String processedValue = getStatusField().getValue()
        Matcher matcher = processedValue =~ /^([0-9].[0-9]{1,3}.[0-9]{1,3})/
        return matcher.count > 0 ? new StatusCode(matcher[0][1]) : null
    }
    String getAction(boolean removeComments = true) {
        return getActionField().getValue(removeComments);
    }

    String getOriginalRecipientAddressType() {
        return getOriginalRecipientField()?.getValueBeforeSemiColon()
    }

    String getOriginalRecipientAddress(boolean removeComments = true) {
        return getOriginalRecipientField()?.getValueAfterSemiColon(removeComments)
    }
    String getEmailAddress(boolean removeComments = true) {
        String originalRecipient = getOriginalRecipientAddress(removeComments)
        if (originalRecipient) {
            return originalRecipient
        } else {
            return getFinalRecipientAddress(true)
        }
    }

    String getFinalRecipientAddressType() {
        return getFinalRecipientField().getValueBeforeSemiColon()
    }

    String getFinalRecipientAddress(boolean removeComments = true) {
        return getFinalRecipientField().getValueAfterSemiColon(removeComments)
    }

    String getRemoteMtaType() {
        return getRemoteMtaField()?.getValueBeforeSemiColon()
    }

    String getRemoteMtaName(boolean removeComments = true) {
        return getRemoteMtaField()?.getValueAfterSemiColon(removeComments)
    }

    String getDiagnosticCodeType() {
        return getDiagnosticCodeField()?.getValueBeforeSemiColon()
    }

    String getDiagnosticCodeTypeText(boolean removeComments = true) {
        return getDiagnosticCodeField()?.getValueAfterSemiColon(removeComments)
    }

    String getFinalLogId(boolean removeComments = true) {
        return getFinalLogIdField().getValue(removeComments)
    }

    String getLastAttemptDate() {
        return getLastAttemptDateField()?.getValue(false)
    }

    String getWillRetryUntil() {
        return getWillRetryUntilField().getValue(false)
    }
}
