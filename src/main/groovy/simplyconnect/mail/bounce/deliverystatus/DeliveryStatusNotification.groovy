package simplyconnect.mail.bounce.deliverystatus

import simplyconnect.mail.fields.Field
import simplyconnect.mail.fields.FieldMap
import simplyconnect.mail.fields.NameValueField

/**
 * User: david
 * Date: 27/03/13
 * Time: 6:08 PM
 * [ original-envelope-id-field CRLF ]
             reporting-mta-field CRLF
             [ dsn-gateway-field CRLF ]
             [ received-from-mta-field CRLF ]
             [ arrival-date-field CRLF ]
             *( extension-field CRLF )
 */
class DeliveryStatusNotification {
    public static final String REPORTING_MTA_FIELD = "Reporting-MTA"
    public static final String ORIGINAL_ENVELOPE_ID_FIELD = "Original-Envelope-Id"
    public static final String DSN_GATEWAY_FIELD = "DSN-Gateway"
    public static final String RECEIVED_FROM_MTA_FIELD = "Received-From-MTA"
    public static final String ARRIVAL_DATE_FIELD = "Arrival-Date"

    FieldMap fields = new FieldMap()
    List<RecipientFields> recipients = []
    void addField(Field perMessageField) {
        fields.add(perMessageField)
    }
    NameValueField getReportingMtaField() {
        return fields.get(REPORTING_MTA_FIELD)
    }
    NameValueField getOriginalEnvelopeIdField() {
        return fields.get(ORIGINAL_ENVELOPE_ID_FIELD)
    }
    NameValueField getDsnGatewayField() {
        return fields.get(DSN_GATEWAY_FIELD)
    }
    NameValueField getReceivedFromMtaField() {
        return fields.get(RECEIVED_FROM_MTA_FIELD)
    }
    NameValueField getArrivalDateField() {
        return fields.get(ARRIVAL_DATE_FIELD)
    }

    void addRecipient(RecipientFields recipientFields) {
        recipients << recipientFields
    }

    String getReceivedFromMtaNameType() {
        return getReceivedFromMtaField().getValueBeforeSemiColon()
    }

    String getReceivedFromMtaName(boolean removeComments = true) {
        return getReceivedFromMtaField().getValueAfterSemiColon(removeComments)
    }

    String getOriginalEnvelopeId(boolean removeComments = true) {
        return getOriginalEnvelopeIdField()?.getValue(removeComments)
    }

    String getReportingMtaNameType(boolean removeComments = true) {
        return getReportingMtaField()?.getValueBeforeSemiColon(removeComments)
    }

    String getReportingMtaName(boolean removeComments = true) {
        return getReportingMtaField()?.getValueAfterSemiColon(removeComments)
    }

    String getDsnGatewayNameType(boolean removeComments = true) {
        return getDsnGatewayField()?.getValueBeforeSemiColon(removeComments)
    }

    String getDsnGatewayName(boolean removeComments = true) {
        return getDsnGatewayField()?.getValueAfterSemiColon(removeComments)
    }

    String getArrivalDate() {
        return getArrivalDateField()?.getValue(false)
    }
}
