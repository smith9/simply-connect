package simplyconnect.mail.bounce.config

/**
 * User: david
 * Date: 3/04/13
 * Time: 10:58 PM
 */
class ConfigurationException extends RuntimeException {
    ConfigurationException() {
    }

    ConfigurationException(Throwable throwable) {
        super(throwable)
    }

    ConfigurationException(String message) {
        super(message)
    }

    ConfigurationException(String message, Throwable throwable) {
        super(message, throwable)
    }
}
