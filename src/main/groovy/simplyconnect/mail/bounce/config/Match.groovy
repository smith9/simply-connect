package simplyconnect.mail.bounce.config

import simplyconnect.mail.GetHeader

/**
 * User: david
 * Date: 3/04/13
 */
class Match implements Matcher{
    List<Term> terms = []
    int requiredMatches = 1

    public void add(Term term) {
        terms << term
    }

    boolean matches(String subject, String content, GetHeader header) {
        int matchCount = 0
        for(Term term : terms) {
            if (term.matches(subject, content, header)) {
                matchCount++
                if (matchCount >= requiredMatches) {
                    break
                }
            }
        }
        return matchCount >= requiredMatches

    }

    void validate() {
        if (requiredMatches > terms.size()) {
            throw new ConfigurationException("There are not enough expr or header children of match for the requiredMatches count")
        }
    }
}
