package simplyconnect.mail.bounce.config

import simplyconnect.mail.GetHeader

/**
 * User: david
 * Date: 3/04/13
 */
class HeaderTerm implements Term {
    String name
    String value

    HeaderTerm(String name, String value) {
        this.name = name
        this.value = value
    }

    boolean matches(String subject, String content, GetHeader header) {
        List<String> headerValues = header.getHeader(name)
        boolean match = false
        for(String headerValue: headerValues) {
            if (headerValue.find(value)) {
                match = true
                break
            }
        }
        return match
    }
}
