package simplyconnect.mail.bounce.config

import simplyconnect.mail.bounce.deliverystatus.StatusCode

/**
 * User: david
 * Date: 18/04/13
 * Time: 9:36 PM
 */
class StatusCodeConfig {
    Properties properties

    StatusCodeConfig(File statusCodeFile) {
        properties = new Properties()
        properties.load(new FileInputStream(statusCodeFile))
    }

    String getDescription(StatusCode statusCode) {
        return properties.getProperty(statusCode.statusSubjectAndDetail)
    }
}
