package simplyconnect.mail.bounce.config

import groovy.util.slurpersupport.GPathResult
import simplyconnect.mail.bounce.categoriser.CategoryResult
import simplyconnect.mail.bounce.deliverystatus.StatusCode

/**
 * User: david
 * Date: 3/04/13
 */
class CategoryConfig {
    List<CategoryDetails> load(File configFile) {
        List<CategoryDetails> matchers = []
        GPathResult config = new XmlSlurper().parse(configFile)
        config.type.each { GPathResult typeNode ->
            typeNode.category.each { GPathResult categoryNode ->
                categoryNode.item.each { GPathResult itemNode ->
                    CategoryDetails category = new CategoryDetails()
                    category.category = categoryNode.@name
                    category.type = typeNode.@name
                    category.action = itemNode.@action
                    if (itemNode.@statusCode != "") {
                        category.statusCode = new StatusCode((String)itemNode.@statusCode)
                    }
                    category.subCategory = itemNode.@subCategory
                    addEmptyReturnPathMatch(itemNode, category)
                    itemNode.match.each { GPathResult matchNode ->
                        category.add(createMatch(matchNode))
                    }
                    matchers << category
                }
            }
        }

        return matchers
    }

    private void addEmptyReturnPathMatch(GPathResult itemNode, CategoryDetails category) {
        boolean matchOnlyEmptyReturnPath = Boolean.parseBoolean((String) itemNode.@emptyReturnPath)
        if (matchOnlyEmptyReturnPath) {
            Match match = new Match()
            match.add(new HeaderTerm("Return-Path", "<>"))
            category.add(match)
        }
    }

    Match createMatch(GPathResult matchNode) {
        Match match = new Match()
        if (matchNode.@requiredMatches) {
            match.requiredMatches = Integer.valueOf((String)matchNode.@requiredMatches)
        }
        matchNode.expr.each { GPathResult exprNode ->
            match.add(createExprTerm(exprNode))
        }
        matchNode.header.each { GPathResult header ->
            match.add(createHeaderTerm(header))
        }

        match.validate()
        return match
    }

    HeaderTerm createHeaderTerm(GPathResult headerNode) {
        return new HeaderTerm((String)headerNode.@name, (String)headerNode.@value)
    }

    private ExprTerm createExprTerm(GPathResult exprNode) {
        ExprTerm expr = new ExprTerm(exprNode.text())
        if (exprNode.@searchContent != "") {
            expr.searchContent = Boolean.parseBoolean((String) exprNode.@searchContent)
        }
        if (exprNode.@searchSubject != "") {
            expr.searchSubject = Boolean.parseBoolean((String) exprNode.@searchSubject)
        }
        return expr
    }
}
