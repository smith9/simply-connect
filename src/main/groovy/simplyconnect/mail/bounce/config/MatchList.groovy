package simplyconnect.mail.bounce.config

import simplyconnect.mail.GetHeader

/**
 * User: david
 * Date: 3/04/13
 */
class MatchList implements Matcher {
    List<Match> matches = []

    public void add(Match match) {
        matches << match
    }

    boolean matches(String subject, String content, GetHeader header) {
        Match failedMatch = matches.find { Match match ->
            return !match.matches(subject, content, header)
        }
        return !failedMatch

    }
}
