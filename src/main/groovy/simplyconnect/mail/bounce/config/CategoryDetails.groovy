package simplyconnect.mail.bounce.config

import simplyconnect.mail.GetHeader

import simplyconnect.mail.bounce.categoriser.CategoryResult
import simplyconnect.mail.bounce.deliverystatus.StatusCode

/**
 * User: david
 * Date: 3/04/13
 */
class CategoryDetails implements Matcher{
    private MatchList matchList = new MatchList()
    String category
    String subCategory
    String type
    String action
    StatusCode statusCode

    CategoryResult createCategory() {
        CategoryResult category = new CategoryResult(type, category)
        category.subCategory = subCategory
        category.action = action
        category.statusCode = statusCode
        return category
    }

    boolean matches(String subject, String content, GetHeader header) {
        return matchList.matches(subject, content, header)
    }

    void add(Match match) {
        matchList.add(match)
    }
}
