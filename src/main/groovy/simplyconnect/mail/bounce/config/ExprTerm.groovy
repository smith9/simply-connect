package simplyconnect.mail.bounce.config

import simplyconnect.mail.GetHeader

/**
 * User: david
 * Date: 3/04/13
 */
class ExprTerm implements Term {
    ExprTerm(String expr) {
        this.expr = expr
    }

    String expr
    boolean searchContent = true
    boolean searchSubject = true

    boolean matches(String subject, String content, GetHeader header) {
        boolean matchedInContent = searchContent && content.find(expr)
        boolean matchedInSubject = searchSubject && subject.find(expr)
        return matchedInContent || matchedInSubject
    }
}
