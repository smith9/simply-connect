package simplyconnect.mail.bounce.config

import simplyconnect.mail.GetHeader

/**
 * User: david
 * Date: 3/04/13
 */
public interface Matcher {
    boolean matches(String subject, String content, GetHeader header)
}