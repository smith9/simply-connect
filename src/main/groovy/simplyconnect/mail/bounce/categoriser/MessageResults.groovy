package simplyconnect.mail.bounce.categoriser

import simplyconnect.io.tablefile.PipeStorable
import simplyconnect.mail.MessageWrapper
import simplyconnect.mail.bounce.categoriser.CategoryResult

/**
 * User: david
 * Date: 17/04/13
 * Time: 8:27 AM
 */
class MessageResults {
    MessageWrapper message
    List<CategoryResult> results
}
