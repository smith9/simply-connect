package simplyconnect.mail.bounce.categoriser

import simplyconnect.mail.MessageWrapper
import simplyconnect.mail.bounce.config.CategoryConfig
import simplyconnect.mail.bounce.config.CategoryDetails
import simplyconnect.mail.bounce.config.StatusCodeConfig
import simplyconnect.mail.bounce.deliverystatus.DeliveryStatusNotification
import simplyconnect.mail.bounce.deliverystatus.DeliveryStatusParser
import simplyconnect.mail.bounce.deliverystatus.RecipientFields

import javax.mail.Part
import javax.mail.internet.MimeMessage

/**
 * User: david
 * Date: 24/03/13
 * Time: 8:14 PM
 */
class Categoriser {
    List<CategoryDetails> matchers = []
    StatusCodeConfig statusConfig
    UnstructuredDsnMatcher unstructuredDsnMatcher
    StructuredDsnMatcher structuredDsnMatcher

    Categoriser(File bounceXml, File statusCodeProperties) {
        initMatchers(bounceXml, statusCodeProperties)
    }

    void initMatchers(File bounceXml, File statusCodeProperties) {
        List<CategoryDetails> matchers = new CategoryConfig().load(bounceXml)
        unstructuredDsnMatcher = new UnstructuredDsnMatcher(matchers)
        StatusCodeConfig statusCodeConfig = new StatusCodeConfig(statusCodeProperties)
        structuredDsnMatcher = new StructuredDsnMatcher(statusCodeConfig)
    }

    MessageResults categorise(MessageWrapper message) {
        List<CategoryResult> results = []
        if (message.isMultiPartReport()) {
            if (message.isDeliveryStatusReportType()) {
                results = structuredDsnMatcher.match(message)
            }
        } else {
            results = unstructuredDsnMatcher.match(message)
            List<CategoryResult> childMessageResults = findChildStructuredDsnIfUnknown(message, results)
            if(childMessageResults) {
                results = childMessageResults
            }
            // TODO if still unknown, consider pulling out all textual parts of the message and running the unstructured parser on those
        }
        return new MessageResults(message: message, results: results)
    }

    List<CategoryResult> findChildStructuredDsnIfUnknown(MessageWrapper message, List<CategoryResult> unstructuredResults) {
        List<CategoryResult> results = []
        if(unstructuredResults.size() == 1 && unstructuredResults.first().type == CategoryResult.TYPE_UNKNOWN) {
            List<Part> childMessages = message.getPartsByContentType("message/rfc822")
            childMessages.each { Part part ->
                MimeMessage childMessage = new MimeMessage(message.message.session, part.getInputStream())
                results.addAll(structuredDsnMatcher.match(new MessageWrapper(childMessage)))
            }
        }
        return results
    }
}
