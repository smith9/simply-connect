package simplyconnect.mail.bounce.categoriser

import simplyconnect.mail.bounce.deliverystatus.StatusCode

/**
 * User: david
 * Date: 24/03/13
 * Time: 8:14 PM
 */
class CategoryResult {
    public static String CATEGORY_HARD = "hard"
    public static String CATEGORY_SOFT = "soft"
    public static String CATEGORY_DELIVERED = "delivered"
    public static String CATEGORY_DELAYED = "delayed"
    public static String CATEGORY_RELAYED = "relayed"
    public static String CATEGORY_EXPANDED = "expanded"
    public static String CATEGORY_AUTOREPLY = "auto-reply"
    public static String CATEGORY_READRECEIPT = "read-receipt"
    public static String CATEGORY_VIRUS = "virus"
    public static String CATEGORY_SPAMBLOCK = "spam-block"
    public static String CATEGORY_SPAMCONTENT = "spam-content"
    public static String CATEGORY_CHALLENGE = "challenge"
    public static String CATEGORY_SUBSCRIBE = "subscribe"
    public static String CATEGORY_UNSUBSCRIBE = "unsubscribe"
    public static String CATEGORY_UNKNOWN = "unknown"

    public static String TYPE_NOT_DELIVERED = "not-delivered"
    public static String TYPE_INFO = "info"
    public static String TYPE_UNKNOWN = "unknown"
    String category
    String subCategory
    String type
    String action
    StatusCode statusCode
    String emailAddress

    CategoryResult(String type, String category) {
        this.type = type
        this.category = category
    }

    CategoryResult(String action, StatusCode statusCode) {
        this.statusCode = statusCode
        this.action = action
        determineTypeAndCategory(action, statusCode)
    }

    private void determineTypeAndCategory(String action, StatusCode statusCode) {
        switch(action) {
            case "delayed":
                type = TYPE_INFO
                category = CATEGORY_DELAYED
                break
            case "delivered":
                type = TYPE_INFO
                category = CATEGORY_DELIVERED
                break
            case "relayed":
                type = TYPE_INFO
                category = CATEGORY_RELAYED
                break
            case "expanded":
                type = TYPE_INFO
                category = CATEGORY_EXPANDED
                break
            case "failed":
                String statusClass = statusCode.statusClass
                type = TYPE_NOT_DELIVERED
                if (statusClass == "4") {
                    category = CATEGORY_SOFT
                } else if (statusClass == "5") {
                    category = CATEGORY_HARD
                }
                break
            default:
                type = TYPE_UNKNOWN
                category = CATEGORY_UNKNOWN
                break
        }
    }


}
