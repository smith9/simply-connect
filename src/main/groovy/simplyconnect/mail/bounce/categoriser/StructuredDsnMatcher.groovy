package simplyconnect.mail.bounce.categoriser

import simplyconnect.mail.MessageWrapper
import simplyconnect.mail.bounce.config.StatusCodeConfig
import simplyconnect.mail.bounce.deliverystatus.DeliveryStatusNotification
import simplyconnect.mail.bounce.deliverystatus.DeliveryStatusParser
import simplyconnect.mail.bounce.deliverystatus.RecipientFields

/**
 * User: david
 * Date: 3/04/13
 */
class StructuredDsnMatcher implements DsnMatcher {
    private StatusCodeConfig config

    StructuredDsnMatcher(StatusCodeConfig config) {
        this.config = config
    }

    List<CategoryResult> match(MessageWrapper message) {
        List<CategoryResult> results = []

        String deliveryStatus = message.getDeliveryStatusContent()
        results.addAll(extractDeliveryStatusCategories(deliveryStatus))

        return results
    }

    List<CategoryResult> extractDeliveryStatusCategories(String deliveryStatusNotification) {
        List<CategoryResult> results = []
        DeliveryStatusNotification dsn = new DeliveryStatusParser().parse(deliveryStatusNotification)
        dsn.recipients.each { RecipientFields recipient ->
            CategoryResult result = new CategoryResult(recipient.action, recipient.statusCode)
            result.emailAddress = recipient.getEmailAddress()
            String statusCodeDescription = config.getDescription(recipient.statusCode)
            if(recipient.statusCode && statusCodeDescription) {
                result.subCategory = statusCodeDescription
            }
            results << result
        }
        return results;
    }

}
