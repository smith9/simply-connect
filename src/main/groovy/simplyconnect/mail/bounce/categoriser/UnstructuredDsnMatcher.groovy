package simplyconnect.mail.bounce.categoriser

import groovy.util.logging.Slf4j
import simplyconnect.mail.MessageWrapper
import simplyconnect.mail.bounce.config.CategoryDetails
import simplyconnect.mail.bounce.config.HeaderTerm
import simplyconnect.mail.bounce.config.Match
import simplyconnect.mail.bounce.config.MatchList
import simplyconnect.mail.bounce.config.Term

/**
 * User: david
 * Date: 3/04/13
 */
class UnstructuredDsnMatcher implements DsnMatcher{
    UnstructuredDsnMatcher(List<CategoryDetails> matchers) {
        this.matchers = matchers
    }

    List<CategoryDetails> matchers = []

    List<CategoryResult> match(MessageWrapper message) {
        String subject = message.subject
        String content = message.getHtmlContentWithoutMarkup()
        CategoryResult category = match(subject, content, message)
        return [category]
    }


    CategoryResult match(String subject, String content, MessageWrapper messageWrapper) {
        CategoryResult category = null
        matchers.each { CategoryDetails matcher ->
            if (matcher.matches(subject, content, messageWrapper)) {
                category = matcher.createCategory()
            }
        }
        if (!category) {
            category = new CategoryResult(CategoryResult.TYPE_UNKNOWN, CategoryResult.CATEGORY_UNKNOWN)
        }

        return category
    }

}
