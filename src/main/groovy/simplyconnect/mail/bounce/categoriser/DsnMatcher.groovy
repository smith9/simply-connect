package simplyconnect.mail.bounce.categoriser

import simplyconnect.mail.MessageWrapper

/**
 * User: david
 * Date: 3/04/13
 */
interface DsnMatcher {
    List<CategoryResult> match(MessageWrapper messageWrapper)
}
