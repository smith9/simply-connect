package simplyconnect.mail.fields

import java.util.regex.Pattern

/**
 * User: david
 * Date: 27/03/13
 * Time: 9:33 PM
 */
class NameValueField implements Field {
    String name
    String value
    private static final Pattern COMMENT_WITH_SURROUNDING_SPACES = ~/ *\([^)]+\) */
    void append(String content) {
        if(!name) {
            String[] parts = content.split(":", 2)
            name = parts[0].trim()
            value = parts[1].trim()
        } else {
            value += content
        }
    }

    String getValueBeforeSemiColon(boolean removeComments = true) {
        return getValuePart(0, ";", removeComments)
    }

    String getValueAfterSemiColon(boolean removeComments = true) {
        return getValuePart(1, ";", removeComments)
    }

    private String getValuePart(int index, String separator, boolean removeComments = true) {
        String part = null
        String processedValue = getValue(removeComments)
        if (processedValue) {
            String[] parts = processedValue.split(separator, 2)
            if (index < parts.length) {
                part = parts[index].trim()
            }
        }
        return part
    }

    String getValue(boolean removeComments = true) {
        if (removeComments) {
            return value.replaceAll(COMMENT_WITH_SURROUNDING_SPACES, " ").trim()
        } else {
            return value
        }
    }
}
