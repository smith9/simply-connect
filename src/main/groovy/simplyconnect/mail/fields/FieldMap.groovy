package simplyconnect.mail.fields

/**
 * User: david
 * Date: 28/03/13
 * Time: 8:14 AM
 */
class FieldMap {
    Map<String, Field> fields = [:]
    void add(Field field) {
        fields[field.name.toLowerCase()] = field
    }

    Field get(String fieldName) {
        return fields.get(fieldName.toLowerCase())
    }
}
