package simplyconnect.mail.fields

/**
 * User: david
 * Date: 27/03/13
 * Time: 9:28 PM
 */
class FieldParser {
    List<Field> fields = []
    Field field = null
    List<Field> parse(String content) {
        content.eachLine { String line ->
            if(isSeparator(line)) {
                addSeparator()
            } else if (startOfNewField(line)) {
                createNewField()
            }
            field.append(line)
        }
        return fields
    }

    void addSeparator() {
        fields << new SeparatorField()
    }

    boolean isSeparator(String line) {
        return line.length() == 0
    }

    void createNewField() {
        field = new NameValueField()
        fields << field
    }

    private boolean startOfNewField(String line) {
        return line.length() > 0 && line.charAt(0).isLetter()
    }
}
