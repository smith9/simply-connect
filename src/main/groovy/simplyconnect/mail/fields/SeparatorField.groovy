package simplyconnect.mail.fields

/**
 * User: u321869
 * Date: 16/04/13
 */
class SeparatorField implements Field{

    void append(String line) {
        if(line.length() != 0) {
            throw new RuntimeException("Separator fields are blank lines with no content")
        }
    }

    String getName() {
        return ""
    }

    String getValue(boolean removeComments = true) {
        return ""
    }
}
