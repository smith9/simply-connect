package simplyconnect.mail.fields

import java.util.regex.Pattern

/**
 * User: david
 * Date: 27/03/13
 * Time: 9:33 PM
 */
interface Field {
    void append(String content)
    String getName()
    String getValue(boolean removeComments)
}
