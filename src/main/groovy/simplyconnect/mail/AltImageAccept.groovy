package simplyconnect.mail

import java.util.regex.Pattern

/**
 * User: david
 * Date: 5/03/13
 * Time: 11:06 PM
 */
class AltImageAccept implements Accept {
    InlineImageHandler inlineImageHandler
    Set<Pattern> rejectAltTextPatterns = []

    AltImageAccept(InlineImageHandler inlineImageHandler, Set<Pattern> rejectAltTextPatterns) {
        this.inlineImageHandler = inlineImageHandler
        this.rejectAltTextPatterns = rejectAltTextPatterns
    }

    boolean accept(String imageName) {
        boolean accepted = true
        ImageDetails imageDetails = inlineImageHandler.getImageByAttachmentName(imageName)
        if (imageDetails) {
            String altText = imageDetails.alt
            for (Pattern pattern: rejectAltTextPatterns) {
                if (altText.matches(pattern)) {
                    accepted = false
                    break
                }
            }
        }
        return accepted
    }
}
