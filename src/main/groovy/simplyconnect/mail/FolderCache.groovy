package simplyconnect.mail

import groovy.util.logging.Slf4j

import javax.mail.Folder
import javax.mail.Message
import javax.mail.Store

/**
 * User: david
 * Date: 02/02/13
 */
@Slf4j
class FolderCache {
    Store store

    Map<String, Folder> folders = [:].withDefault { String path ->
        return store.getFolder(path)
    }

    FolderCache(Store store) {
        this.store = store
    }

    public Folder get(String path) {
        return folders[path]
    }

    public Folder open(String path, int openMode = Folder.READ_WRITE) {
        Folder folder = get(path)
        if (!folder.exists()) {
            throw new MailException("Could not locate folder ${folder.fullName}, please create it")
        }
        if (!folder.open) {
            log.info("opening folder ${folder.fullName}")
            folder.open(openMode)
        }
        return folder
    }

    public Message[] expunge(String path) {
        Folder folder = get(path)
        return folder.expunge()
    }

    public Folder create(String path, int type = Folder.HOLDS_MESSAGES) {
        Folder folder = get(path)
        folder.create(type)
        return folder
    }

    public void close(String path, boolean expunge = false) {
        Folder removedFolder = folders.remove(path)
        closeFolder(removedFolder, expunge)
    }
    public void closeAll(boolean expunge = false) {
        folders.values().each { Folder folder ->
            closeFolder(folder, expunge)
        }
        folders.clear()
    }

    private static void closeFolder(Folder folder, boolean expunge) {
        if (folder.open) {
            log.info("closing folder ${folder.fullName}, remove messages marked as deleted: $expunge")
            if(expunge) {
                folder.expunge()
                // seemed to have issues expunging via close method (messages not deleting), so adding this one to see if it helps
                log.info("expunge complete")
            }
            folder.close(expunge)
        }
    }

    public boolean exists(String path) {
        Folder folder = get(path)
        return folder.exists()
    }

    public boolean isOpen(String path) {
        Folder folder = get(path)
        return folder.open
    }
}
