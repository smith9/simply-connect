package simplyconnect.convertor

import com.icegreen.greenmail.util.GreenMail
import org.apache.commons.mail.HtmlEmail
import org.junit.Before
import org.junit.Test
import simplyconnect.mail.BaseMailTest
import simplyconnect.mail.Mailbox
import simplyconnect.mail.MessageWrapper
import simplyconnect.mail.TestUtil

import javax.mail.internet.MimeMessage
import java.util.regex.Matcher
import java.util.regex.Pattern

import static org.junit.Assert.assertEquals

/**
 * User: david
 * Date: 1/03/13
 * Time: 9:22 PM
 */
class InlineImageConvertorTest extends BaseMailTest {
    @Test
    void testConvert() {
        TestUtil.addMessageToInbox()

        Mailbox mailbox = TestUtil.connectToMailbox()
        MessageWrapper message = mailbox.retrieveMessages(inboxPath).first()
//        String html = TestUtil.createHtml()
        String expectedHtml = """<html>
  <body>
    <h1>Some Heading</h1>
    <p>!squigle.png!</p><img src='cid:859569AA-EABE-4480-8163-10DC62653606' alt='image some_sig_footer.jpg' width='100' height='50' id='cb926640-5d18-447d-9be7-8cf7a9258d6b' />
    <p>This is some paragraph text</p>
    <div id='results' class='simple'>
      <table border='1'>
        <tr>
          <th>Heading 1</th>
          <th>Heading 2</th>
        </tr>
        <tr>
          <td>
            <p>paragraph in cell 1</p>
          </td>
          <td>some text in cell 2</td>
        </tr>
      </table>
    </div>
  </body>
</html>"""
        String html = new MessageExtractor(message).convert(null)
        TestUtil.assertEqualsIgnoreEndings(expectedHtml, new InlineImageConvertor(message).convert(html))
    }

//    @Test
//    void testPattern() {
//        Pattern pattern = ~/<img.+src=["']cid:([^"']+)["'].+>/
//        Matcher m = pattern.matcher("""<img src="cid:as" />""")
//        assertEquals("", m.replaceAll('<p>$1</p>$0'))
//
//    }
}
