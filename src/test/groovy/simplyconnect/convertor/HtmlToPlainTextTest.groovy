package simplyconnect.convertor

import org.apache.commons.mail.HtmlEmail
import org.apache.commons.mail.util.MimeMessageParser
import org.apache.commons.mail.util.MimeMessageUtils
import org.junit.Test
import simplyconnect.mail.MessageWrapper
import simplyconnect.mail.TestUtil

import javax.activation.DataSource
import javax.mail.Message
import javax.mail.Session
import javax.mail.internet.MimeMessage

import static org.junit.Assert.*

/**
 * User: david
 * Date: 18/02/13
 * Time: 10:13 PM
 */
class HtmlToPlainTextTest {

    @Test
    void testConvertPlainTextMessage() {
        Message message = new MimeMessage((Session) null)
        String content = "This is a plain text test"
        message.setContent(content, "text/plain; charset=utf-8")
        message.saveChanges()
        MessageWrapper messageWrapper = new MessageWrapper(message)
        HtmlToPlainText extractor = new HtmlToPlainText(messageWrapper)
        assertEquals(content, extractor.convert(content))
    }

    @Test
    void testConvertHtmlMessage() {
//        HtmlEmail email = TestUtil.createHtmlMessage()
//        email.buildMimeMessage()
//
//        MimeMessage mimeMessage = email.getMimeMessage()
//        this line is important in the test, seems to be if you don't send the message, you at least need to save changes
//        mimeMessage.saveChanges()
//
//        MimeMessageParser parser = new MimeMessageParser(mimeMessage)
//        parser.parse()
//
//        MessageWrapper messageWrapper = new MessageWrapper(mimeMessage)
        MessageWrapper messageWrapper = TestUtil.createHtmlMessageWrapper(null)
        HtmlToPlainText extractor = new HtmlToPlainText(messageWrapper)

        String html = """<html>
  <body>
    <h1>Some Heading</h1>
    <img src='cid:859569AA-EABE-4480-8163-10DC62653606' alt='image some_sig_footer.jpg' width='100' height='50' id='cb926640-5d18-447d-9be7-8cf7a9258d6b' />
    <p>This is some paragraph text</p>
    <div id='results' class='simple'>
      <table border='1'>
        <tr>
          <th>Heading 1</th>
          <th>Heading 2</th>
        </tr>
        <tr>
          <td>
            <p>paragraph in cell 1</p>
          </td>
          <td>some text in cell 2</td>
        </tr>
      </table>
    </div>
  </body>
</html>"""

        TestUtil.assertEqualsIgnoreEndings("""
    Some Heading
    """ + """
    This is some paragraph text
    """ + """
      \tHeading 1\tHeading 2
\t
            paragraph in cell 1
          	some text in cell 2
    """+"""
  """, extractor.convert(html))
    }

    @Test
    void testConvertPartialHtmlMessage() {
//        HtmlEmail email = TestUtil.createHtmlMessage()
//        email.buildMimeMessage()
//
//        MimeMessage mimeMessage = email.getMimeMessage()
//        // this line is important in the test, seems to be if you don't send the message, you at least need to save changes
//        mimeMessage.saveChanges()
//
////        MimeMessageParser parser = new MimeMessageParser(mimeMessage)
////        parser.parse()
//
//        MessageWrapper messageWrapper = new MessageWrapper(mimeMessage)
        MessageWrapper messageWrapper = TestUtil.createHtmlMessageWrapper(null)
        HtmlToPlainText extractor = new HtmlToPlainText(messageWrapper)

        String html = """
    <h1>Some Heading</h1>
    <img src='cid:859569AA-EABE-4480-8163-10DC62653606' alt='image some_sig_footer.jpg' width='100' height='50' id='cb926640-5d18-447d-9be7-8cf7a9258d6b' />
    <p>This is some paragraph text</p>
    <div id='results' class='simple'>
      <table border='1'>
        <tr>
          <th>Heading 1</th>
          <th>Heading 2</th>
        </tr>
        <tr>
          <td>
            <p>paragraph in cell 1</p>
          </td>
          <td>some text in cell 2</td>
        </tr>
      </table>
    </div>"""

        TestUtil.assertEqualsIgnoreEndings("""Some Heading
    """ + """
    This is some paragraph text
    """ + """
      \tHeading 1\tHeading 2
\t
            paragraph in cell 1
          	some text in cell 2
    """+"""
""", extractor.convert(html))
    }

//    @Test
//    void testConvertPlainText() {
//        String content = "This is a plain text test"
//        HtmlToPlainText extractor = new HtmlToPlainText()
//        assertEquals(content, extractor.convert(content))
//    }

//    @Test
//    void testConvertHtmlMessage() {
//
//        String html = """<html><head/>
//  <body>
//    <h1>Some Heading</h1>
//    <p>This is some paragraph text</p>
//    <div id='results' class='simple'>
//      <table border='1'>
//        <tr>
//          <th>Heading 1</th>
//          <th>Heading 2</th>
//        </tr>
//        <tr>
//          <td>
//            <p>paragraph in cell 1</p>
//          </td>
//          <td>some text in cell 2</td>
//        </tr>
//      </table>
//    </div>
//  </body>
//</html>"""
//        HtmlToPlainText extractor = new HtmlToPlainText()
//
//        TestUtil.assertEqualsIgnoreEndings("""
//    Some Heading
//    This is some paragraph text
//    """ + """
//      \tHeading 1\tHeading 2
//\t
//            paragraph in cell 1
//          	some text in cell 2
//    """+"""
//  """, extractor.convert(html))
//    }
}
