package simplyconnect.convertor

import org.apache.commons.mail.DefaultAuthenticator
import org.apache.commons.mail.Email
import org.apache.commons.mail.EmailConstants
import org.apache.commons.mail.HtmlEmail
import org.apache.commons.mail.SimpleEmail
import org.apache.commons.mail.util.MimeMessageParser
import org.apache.commons.mail.util.MimeMessageUtils
import org.junit.Test
import simplyconnect.mail.MessageWrapper
import simplyconnect.mail.TestUtil

import javax.activation.DataSource
import javax.mail.Message
import javax.mail.Session
import javax.mail.internet.MimeMessage

import static org.junit.Assert.*

/**
 * User: david
 * Date: 18/02/13
 * Time: 10:13 PM
 */
class MessageExtractorTest {
    @Test
    void testConvertPlainTextMessage() {
        Message message = new MimeMessage((Session) null)
        String content = "This is a plain text test"
        message.setContent(content, "text/plain; charset=utf-8")
        message.saveChanges()
        MessageWrapper messageWrapper = new MessageWrapper(message)
        MessageExtractor extractor = new MessageExtractor(messageWrapper)
        assertEquals(content, extractor.convert(null))
    }

    @Test
    void testConvertHtmlMessage() {
        MessageWrapper wrapper = TestUtil.createHtmlMessageWrapper(null)
        MessageExtractor extractor = new MessageExtractor(wrapper)

        assertEquals("""<html>
  <body>
    <h1>Some Heading</h1>
    <img src='cid:859569AA-EABE-4480-8163-10DC62653606' alt='image some_sig_footer.jpg' width='100' height='50' id='cb926640-5d18-447d-9be7-8cf7a9258d6b' />
    <p>This is some paragraph text</p>
    <div id='results' class='simple'>
      <table border='1'>
        <tr>
          <th>Heading 1</th>
          <th>Heading 2</th>
        </tr>
        <tr>
          <td>
            <p>paragraph in cell 1</p>
          </td>
          <td>some text in cell 2</td>
        </tr>
      </table>
    </div>
  </body>
</html>""", extractor.convert(null))
    }

    @Test
    public void testParseHtmlEmailWithAttachments() throws Exception {
        DataSource dataSource
        MimeMessage message = MimeMessageUtils.createMimeMessage(null, new File("src/test/resources/emails/Test Html Email.eml"))
        MessageWrapper messageWrapper = new MessageWrapper(message)
        MessageExtractor extractor = new MessageExtractor(messageWrapper)
        String content = extractor.convert(null)
        MimeMessageParser mimeMessageParser = new MimeMessageParser(message)

        mimeMessageParser.parse()

        assertEquals("Test email", mimeMessageParser.getSubject())
        assertNotNull(mimeMessageParser.getMimeMessage())
        assertTrue(mimeMessageParser.isMultipart())
        assertTrue(mimeMessageParser.hasHtmlContent())
        assertTrue(mimeMessageParser.hasPlainContent())
        assertNotNull(mimeMessageParser.getPlainContent())
        def expectedPlainText = TestUtil.replaceEndings("""
An image:


Some large text

some bold text

""")
        String actualPlainText = TestUtil.replaceEndings(mimeMessageParser.getPlainContent())
        assertEquals(expectedPlainText, actualPlainText)
        assertNotNull(mimeMessageParser.getHtmlContent())
        assertEquals("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html charset=us-ascii\"></head><body style=\"word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space; \"><div><br></div>An image:&nbsp;<div><img height=\"50\" width=\"100\" apple-width=\"yes\" apple-height=\"yes\" id=\"cb926640-5d18-447d-9be7-8cf7a9258d6b\" src=\"cid:859569AA-EABE-4480-8163-10DC62653606\"></div><div><br></div><div><font size=\"5\">Some large text</font></div><div><font size=\"5\"><br></font></div><div><font size=\"5\"><b>some bold text</b></font></div><div><font size=\"5\"><br></font></div></body></html>",
                content)
        assertTrue(mimeMessageParser.getTo().size() == 1)
        assertTrue(mimeMessageParser.getCc().size() == 0)
        assertTrue(mimeMessageParser.getBcc().size() == 0)
        assertEquals("smith7075@gmail.com", mimeMessageParser.getFrom())
        assertEquals("smith7075@gmail.com", mimeMessageParser.getReplyTo())
        assertTrue(mimeMessageParser.hasAttachments())
        List<?> attachmentList = mimeMessageParser.getAttachmentList()
        assertEquals(1, attachmentList.size())

        dataSource = mimeMessageParser.findAttachmentByName("squigle.png")
        assertNotNull(dataSource)
        assertEquals("image/png", dataSource.getContentType())

    }
}
