package simplyconnect.mail

import com.icegreen.greenmail.util.ServerSetupTest
import groovy.xml.MarkupBuilder
import org.apache.commons.io.IOUtils
import org.joda.time.DateTime

import javax.activation.DataSource
import javax.mail.Flags
import javax.mail.Folder
import javax.mail.Message
import javax.mail.Session
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart

import static junit.framework.Assert.assertEquals


/**
 * User: david
 * Date: 02/02/13
 */
class TestUtil {
    static String subject = "Some subject"
    static String body = "Some test body text."
    static String fromEmailAddress = "from@somehost.com"
    static String ccAddress = "cc@somehost.com"
    static String ccAddress2 = "cc2@somehost.com"
    static String bccAddress = "bcc@somehost.com"
    static String mailboxUsersEmailAddress = "mailuser@somehost.com"
    static DateTime sentDate = new DateTime(2013, 2, 2, 20, 15)

    static String user = "mailuser"
    static String password = "mailpassword"
    static int smtpPort = ServerSetupTest.SMTP.port
    static int imapPort = ServerSetupTest.IMAP.port
    static String smtpProtocol = "smtp"
    static String imapProtocol = "imap"
    static String localHost = "127.0.0.1"
    static String html = createHtml()

    static void assertEqualsIgnoreEndings(String message, String expected, String actual) {
        assertEquals(message, replaceEndings(expected), replaceEndings(actual))
    }

    static void assertEqualsIgnoreEndings(String expected, String actual) {
        String convertedExpected = replaceEndings(expected)
        String convertedActual = replaceEndings(actual)
        assertEquals(convertedExpected, convertedActual)
    }

    static String createHtml() {
        Writer writer = new StringWriter()
        MarkupBuilder builder = new MarkupBuilder(writer)
        builder.html {
            body {
                h1("Some Heading")
                img(src: "cid:859569AA-EABE-4480-8163-10DC62653606", alt: "image some_sig_footer.jpg", width: "100", height: "50", id: "cb926640-5d18-447d-9be7-8cf7a9258d6b")
                p("This is some paragraph text")
                div(id: "results", class: "simple") {
                    table(border: 1) {
                        tr {
                            th("Heading 1")
                            th("Heading 2")
                        }
                        tr {
                            td {
                                p("paragraph in cell 1")
                            }
                            td("some text in cell 2")
                        }
                    }
                }
            }
        }
        return writer.toString()
    }

    static MessageWrapper createHtmlMessageWrapper(SendMail sender, Map fields = [keywords: ["don't care category"]]) {
        MimeMessage message = createHtmlMessageWithInlineImage(sender, fields)
        return new MessageWrapper(message)
    }

    static void addMessageToInbox(Map fields = [keywords: ["don't care category"]]) {
        SendMail sender = new SendMail()
        Properties props = new Properties()
        props.put("mail.smtp.starttls.enable","true")
        sender.connect(user, password, localHost, smtpPort, smtpProtocol, new Properties())
        Message msg = createHtmlMessageWithInlineImage(sender, fields)
        sender.sendMessage(msg)
    }

    private static MimeMessage createHtmlMessageWithInlineImage(SendMail sender, Map fields = [keywords: ["don't care category"]]) {
        MimeMessage msg = new MimeMessage(sender?.session)
        msg.setFrom(new InternetAddress(fromEmailAddress))
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(mailboxUsersEmailAddress))
        msg.addRecipient(Message.RecipientType.CC, new InternetAddress(ccAddress, "Mr CC"))
        msg.addRecipient(Message.RecipientType.CC, new InternetAddress(ccAddress2, "Mr CC2"))
        msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(bccAddress, "Mr BCC"))
        if(fields.subject) {
            msg.setSubject(fields.subject)
        } else {
            msg.setSubject(subject)
        }
        msg.setSentDate(new Date())
        if (fields.keywords) {
            msg.addHeader("Keywords", fields.keywords.join(", "))
        }
        msg.addHeader("Keywords", "Keyword99")
        MimeMultipart multipart = new MimeMultipart()

        MimeBodyPart bodyPart1 = new MimeBodyPart()
        bodyPart1.setContent(body, "text/plain")
        multipart.addBodyPart(bodyPart1)

        MimeBodyPart bodyPart2 = new MimeBodyPart()
        bodyPart2.setContent(html, "text/html")
        multipart.addBodyPart(bodyPart2)

        MimeBodyPart bodyPart3 = new MimeBodyPart()
        File image = new File("src/test/resources/images/squigle.png")
        bodyPart3.attachFile(image)
        bodyPart3.setContentID("<859569AA-EABE-4480-8163-10DC62653606>")
        bodyPart3.setDescription("this is the squigle image")
        bodyPart3.setDisposition(MimeBodyPart.INLINE)
//        bodyPart3.setContent(html,"text/html")
        multipart.addBodyPart(bodyPart3)

        msg.setContent(multipart)
        msg.saveChanges()
        return msg
    }

    static Mailbox connectToMailbox() {
        Mailbox mailbox = new Mailbox()
        mailbox.debug = true
        mailbox.connect(user, password, localHost, imapPort, imapProtocol)
        return mailbox
    }

    static void checkMessageCount(Mailbox mailbox, String path, int expectedCount) {
        Folder folder = mailbox.get(path)
        assertEquals(getSubjects(folder), expectedCount, folder.messageCount)
    }

    static String getSubjects(Folder folder) {
        if (!folder.open) {
            folder.open(Folder.READ_WRITE)
        }
        List<String> subjects = folder.getMessages().collect { MimeMessage message ->
            String deleted = message.isSet(Flags.Flag.DELETED) ? "(Deleted)" : "(Not Deleted)"
            message.subject + deleted
        }
        String separator = "," + System.getProperty("line.separator")
        return "subjects for ${folder.fullName} was$separator${subjects.join(separator)}"
    }

    static String replaceEndings(String text) {
        return text.replaceAll("\r\n", "\n")
    }

    static void checkImage(DataSource imageSource, File expectedFilename) {
        String attachment = IOUtils.toString(imageSource.inputStream)
        assertEquals(expectedFilename.text, attachment)
    }

    static String complexHtml() {
        return """
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=us-ascii">
<meta name="Generator" content="Microsoft Word 14 (filtered medium)">
<!--[if !mso]><style>v\\:* {behavior:url(#default#VML);}
o\\:* {behavior:url(#default#VML);}
w\\:* {behavior:url(#default#VML);}
.shape {behavior:url(#default#VML);}
</style><![endif]--><style><!--
/* Font Definitions */
@font-face
\t{font-family:Calibri;
\tpanose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
\t{font-family:Tahoma;
\tpanose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
\t{font-family:Georgia;
\tpanose-1:2 4 5 2 5 4 5 2 3 3;}
@font-face
\t{font-family:Verdana;
\tpanose-1:2 11 6 4 3 5 4 4 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
\t{margin:0cm;
\tmargin-bottom:.0001pt;
\tfont-size:11.0pt;
\tfont-family:"Calibri","sans-serif";}
a:link, span.MsoHyperlink
\t{mso-style-priority:99;
\tcolor:blue;
\ttext-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
\t{mso-style-priority:99;
\tcolor:purple;
\ttext-decoration:underline;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
\t{mso-style-priority:99;
\tmso-style-link:"Balloon Text Char";
\tmargin:0cm;
\tmargin-bottom:.0001pt;
\tfont-size:8.0pt;
\tfont-family:"Tahoma","sans-serif";}
span.BalloonTextChar
\t{mso-style-name:"Balloon Text Char";
\tmso-style-priority:99;
\tmso-style-link:"Balloon Text";
\tfont-family:"Tahoma","sans-serif";}
span.EmailStyle19
\t{mso-style-type:personal;
\tfont-family:"Verdana","sans-serif";
\tcolor:#17365D;}
span.EmailStyle20
\t{mso-style-type:personal-reply;
\tfont-family:"Calibri","sans-serif";
\tcolor:#1F497D;}
.MsoChpDefault
\t{mso-style-type:export-only;
\tfont-size:10.0pt;}
@page WordSection1
\t{size:612.0pt 792.0pt;
\tmargin:72.0pt 72.0pt 72.0pt 72.0pt;}
div.WordSection1
\t{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif]-->
</head>
<body lang="EN-AU" link="blue" vlink="purple">
<div class="WordSection1">
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal" style="text-autospace:none"><span style="color:#0F243E">Thank you for your email.&nbsp;
<o:p></o:p></span></p>
<p class="MsoNormal" style="text-autospace:none"><span style="color:#0F243E"><o:p>&nbsp;</o:p></span></p>
<p class="MsoNormal" style="text-autospace:none"><span style="color:#0F243E">We will action your request as soon as possible, or contact you if we require any additional information.<o:p></o:p></span></p>
<p class="MsoNormal"><span style="color:#17365D"><o:p>&nbsp;</o:p></span></p>
<p class="MsoNormal"><span style="color:#17365D">Regards,<o:p></o:p></span></p>
<p class="MsoNormal"><span style="color:#17365D"><o:p>&nbsp;</o:p></span></p>
<p class="MsoNormal"><b><span style="font-size:9.0pt;font-family:&quot;Georgia&quot;,&quot;serif&quot;;color:#006F66">Some Team</span></b><b><span style="font-size:8.0pt;font-family:&quot;Georgia&quot;,&quot;serif&quot;;color:#006F66"><o:p></o:p></span></b></p>
<p class="MsoNormal"><b><span style="font-size:8.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#857A76">Some Company
<o:p></o:p></span></b></p>
<p class="MsoNormal"><span style="font-size:8.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#857A76"><o:p>&nbsp;</o:p></span></p>
<p class="MsoNormal"><span style="font-size:8.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#857A76">Email:
<u><a href="mailto:someuser@somehost.com">someuser@somehost.com</a></u>
<i><o:p></o:p></i></span></p>
<p class="MsoNormal"><span style="font-size:8.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#857A76">23 The St, North Sydney, NSW, 2000<o:p></o:p></span></p>
<p class="MsoNormal"><span style="font-size:8.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#857A76">GPO Box 9999, Sydney, NSW, 2000<o:p></o:p></span></p>
<p class="MsoNormal"><span style="color:#17365D"><o:p>&nbsp;</o:p></span></p>
<p class="MsoNormal"><span style="color:#17365D"><img border="0" width="332" height="68" id="Picture_x0020_1" src="cid:image001.jpg@01CD4B07.C5013F60" alt="Email_Sig_Footer300dpi.jpg"><o:p></o:p></span></p>
<p class="MsoNormal"><span style="color:#17365D"><o:p>&nbsp;</o:p></span></p>
<p class="MsoNormal"><span style="font-size:7.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:gray">This e-mail is sent by the some user<o:p></o:p></span></p>
<p class="MsoNormal"><span style="font-size:7.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:gray">blah blah
</span> <img border="0" width="332" height="68" id="Picture_x0020_2" src="cid:image002.jpg@01CD4B07.C5013F60" alt="Email_Sig_Footer300dpi.jpg">
<p class="MsoNormal"><span style="font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#17365D"><o:p>&nbsp;</o:p></span></p>
<p class="MsoNormal"><span style="font-family:&quot;Verdana&quot;,&quot;sans-serif&quot;;color:#17365D"><o:p>&nbsp;</o:p></span></p>
</div>
</body>
</html>
"""
    }
}
