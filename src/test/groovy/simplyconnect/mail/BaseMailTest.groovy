package simplyconnect.mail

import com.icegreen.greenmail.user.GreenMailUser
import com.icegreen.greenmail.util.GreenMail
import com.icegreen.greenmail.util.ServerSetup
import com.icegreen.greenmail.util.ServerSetupTest
import org.apache.commons.mail.Email
import org.apache.commons.mail.EmailUtils
import org.junit.After
import org.junit.Before
import org.junit.Test

import javax.mail.Folder
import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.Session
import javax.mail.internet.MimeMessage

/**
 * User: david
 * Date: 02/02/13
 */
public class BaseMailTest {
    GreenMail mailServer
    GreenMailUser greenMailUser
    public static String inboxPath = "Inbox"
    public static String processedPath = "Inbox/Processed"
    public static String createdPath = "Inbox/Created"

    Mailbox mailbox

    @Before
    public void before() {
        ServerSetup[] configs = [ServerSetupTest.IMAP, ServerSetupTest.SMTP].toArray(new ServerSetup[0])
        mailServer = new GreenMail(configs)
        mailServer.start()

        // create a user on mail server
        greenMailUser = mailServer.setUser(TestUtil.mailboxUsersEmailAddress, TestUtil.user, TestUtil.password)
        mailbox = TestUtil.connectToMailbox()
    }

    @After
    public void tearDown() {
//        sleep(1000000)
        mailServer.stop();
    }

//    public MessageWrapper createMessage() {
//
//        final Session session = Session.getInstance(new Properties())
//        Email message = TestUtil.createHtmlMessage()
////        message.setMailSession(session)
//        greenMailUser.deliver((MimeMessage)message.mimeMessage)
////        message.send()
////        TestUtil.addMessageToInbox()
//        Mailbox mailbox = TestUtil.connectToMailbox()
//        Folder open = mailbox.open("Inbox")
//        message.buildMimeMessage()
//
//        open.appendMessages([message.mimeMessage] as Message[])
////        mailbox.disconnect(true)
////        mailbox = TestUtil.connectToMailbox()
////        mailbox.open("Inbox")
//        MessageWrapper retrievedMesage = mailbox.retrieveMessages(inboxPath).first()
//        return retrievedMesage
//    }
    public MessageWrapper createMessage() {
        TestUtil.addMessageToInbox()
        Mailbox mailbox = TestUtil.connectToMailbox()
        MessageWrapper retrievedMesage = mailbox.retrieveMessages(inboxPath).first()
        return retrievedMesage
    }
}
