package simplyconnect.mail

import org.joda.time.DateTime

import javax.mail.Message
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.MimeMessage

/**
 * User: david
 */
class SendMail {
    Transport transport
    Session session
    void sendMessage(MimeMessage message) {
        // TODO do we need to set the sent date?
        message.setSentDate(DateTime.now().toDate())
        message.saveChanges()
        transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO))
        transport.close()
    }

    void connect(String user, String password, String host, String protocol, Properties properties = new Properties()) {
        initialiseTransport(properties, protocol)
        transport.connect(host, user, password)
    }
    void connect(String user, String password, String host, int port, String protocol, Properties properties = new Properties()) {
        initialiseTransport(properties, protocol)
        transport.connect(host, port, user, password)
    }

    private void initialiseTransport(Properties properties, String protocol) {
        session = Session.getInstance(properties)
        transport = session.getTransport(protocol)
    }
}