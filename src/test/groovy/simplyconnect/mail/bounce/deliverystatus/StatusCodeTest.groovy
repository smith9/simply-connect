package simplyconnect.mail.bounce.deliverystatus

import org.junit.Test

import static org.junit.Assert.assertEquals

/**
 * User: david
 * Date: 29/03/13
 * Time: 8:03 PM
 */
class StatusCodeTest {
    @Test
    void testGetStatusPartsMultipleDigits() {
        StatusCode status = new StatusCode("4.111.999")
        assertEquals("4", status.getStatusClass())
        assertEquals("111", status.getStatusSubject())
        assertEquals("999", status.getStatusDetail())
        assertEquals("111.999", status.getStatusSubjectAndDetail())
    }
    @Test
    void testGetStatusPartsSingleDigits() {
        StatusCode status = new StatusCode("4.1.9")
        assertEquals("4", status.getStatusClass())
        assertEquals("1", status.getStatusSubject())
        assertEquals("9", status.getStatusDetail())
        assertEquals("1.9", status.getStatusSubjectAndDetail())
    }

}
