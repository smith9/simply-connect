package simplyconnect.mail.bounce.deliverystatus

import org.junit.Test
import simplyconnect.mail.fields.NameValueField

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNull

/**
 * User: david
 * Date: 29/03/13
 * Time: 5:57 PM
 */
class DeliveryStatusNotificationTest {
    @Test
    void testGetReceivedFromMta() {
        DeliveryStatusNotification dsn = createDSN(DeliveryStatusNotification.RECEIVED_FROM_MTA_FIELD + ":dns;st11p02mm-asmtp001.mac.com (tcp-daemon)")
        NameValueField field = dsn.receivedFromMtaField
        assertEquals(DeliveryStatusNotification.RECEIVED_FROM_MTA_FIELD, field.name)
        assertEquals("dns;st11p02mm-asmtp001.mac.com (tcp-daemon)", field.getValue(false))
        assertEquals("dns", dsn.getReceivedFromMtaNameType())
        assertEquals("st11p02mm-asmtp001.mac.com", dsn.getReceivedFromMtaName())
        assertEquals("st11p02mm-asmtp001.mac.com (tcp-daemon)", dsn.getReceivedFromMtaName(false))
    }
    @Test
    void testGetReportingMta() {
        DeliveryStatusNotification dsn = createDSN(DeliveryStatusNotification.REPORTING_MTA_FIELD + ":dns;st11p02mm-asmtp001.mac.com (tcp-daemon)")
        NameValueField field = dsn.reportingMtaField
        assertEquals(DeliveryStatusNotification.REPORTING_MTA_FIELD, field.name)
        assertEquals("dns;st11p02mm-asmtp001.mac.com (tcp-daemon)", field.getValue(false))
        assertEquals("dns", dsn.getReportingMtaNameType())
        assertEquals("st11p02mm-asmtp001.mac.com", dsn.getReportingMtaName())
        assertEquals("st11p02mm-asmtp001.mac.com (tcp-daemon)", dsn.getReportingMtaName(false))
    }
    @Test
    void testGetDsnGateway() {
        DeliveryStatusNotification dsn = createDSN(DeliveryStatusNotification.DSN_GATEWAY_FIELD + ":dns;st11p02mm-asmtp001.mac.com (tcp-daemon)")
        NameValueField field = dsn.dsnGatewayField
        assertEquals(DeliveryStatusNotification.DSN_GATEWAY_FIELD, field.name)
        assertEquals("dns;st11p02mm-asmtp001.mac.com (tcp-daemon)", field.getValue(false))
        assertEquals("dns", dsn.getDsnGatewayNameType())
        assertEquals("st11p02mm-asmtp001.mac.com", dsn.getDsnGatewayName())
        assertEquals("st11p02mm-asmtp001.mac.com (tcp-daemon)", dsn.getDsnGatewayName(false))
    }
    @Test
    void testGetOriginalEnvelope() {
        DeliveryStatusNotification dsn = createDSN(DeliveryStatusNotification.ORIGINAL_ENVELOPE_ID_FIELD + ":0MK8003W0MS2RC00@st11p02mm-asmtp001.mac.com")
        NameValueField field = dsn.originalEnvelopeIdField
        assertEquals(DeliveryStatusNotification.ORIGINAL_ENVELOPE_ID_FIELD, field.name)
        assertEquals("0MK8003W0MS2RC00@st11p02mm-asmtp001.mac.com", field.getValue(false))
        assertEquals("0MK8003W0MS2RC00@st11p02mm-asmtp001.mac.com", dsn.getOriginalEnvelopeId())
    }
    @Test
    void testGetArrivalDate() {
        DeliveryStatusNotification dsn = createDSN(DeliveryStatusNotification.ARRIVAL_DATE_FIELD + ":: Thu, 7 Jul 1994 17:15:49 (GMT) -0400")
        NameValueField field = dsn.arrivalDateField
        assertEquals(DeliveryStatusNotification.ARRIVAL_DATE_FIELD, field.name)
        assertEquals(": Thu, 7 Jul 1994 17:15:49 (GMT) -0400", field.getValue(false))
        assertEquals(": Thu, 7 Jul 1994 17:15:49 (GMT) -0400", dsn.getArrivalDate())
    }
    @Test
    void testOptionalOriginalEnvelopeId() {
        DeliveryStatusNotification dsn = createDSN()
        assertNull(dsn.getOriginalEnvelopeIdField())
        assertNull(dsn.getOriginalEnvelopeId())
    }
    @Test
    void testOptionalDsnGateway() {
        DeliveryStatusNotification dsn = createDSN()
        assertNull(dsn.getDsnGatewayField())
        assertNull(dsn.getDsnGatewayNameType())
        assertNull(dsn.getDsnGatewayName())
    }
    @Test
    void testOptionalReportingMta() {
        DeliveryStatusNotification dsn = createDSN()
        assertNull(dsn.getReportingMtaField())
        assertNull(dsn.getReportingMtaName())
        assertNull(dsn.getReportingMtaNameType())
    }
    @Test
    void testOptionalArrivalDate() {
        DeliveryStatusNotification dsn = createDSN()
        assertNull(dsn.getArrivalDateField())
        assertNull(dsn.getArrivalDate())
    }

    DeliveryStatusNotification createDSN(String... lines) {
        DeliveryStatusNotification fields = new DeliveryStatusNotification()
        if (lines) {
            NameValueField field = new NameValueField()
            lines.each { String line ->
                field.append(line)
            }
            fields.addField(field)
        }
        return fields
    }

}
