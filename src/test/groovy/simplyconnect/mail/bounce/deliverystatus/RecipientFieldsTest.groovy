package simplyconnect.mail.bounce.deliverystatus

import org.junit.Test
import simplyconnect.mail.fields.NameValueField

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNull

/**
 * User: david
 * Date: 28/03/13
 * Time: 11:50 PM
 */
class RecipientFieldsTest {
    @Test
    void testGetStatusCodeMultiDigit() {
        RecipientFields recipients = createRecipients(RecipientFields.STATUS_FIELD + ":4.111.999 (some comment)")
        NameValueField field = recipients.statusField
        assertEquals(RecipientFields.STATUS_FIELD, field.name)
        assertEquals("4.111.999 (some comment)", field.getValue(false))
        assertEquals("4.111.999", recipients.getStatusCode().status)
    }
    @Test
    void testGetStatusCodeWithComment() {
        RecipientFields recipients = createRecipients(RecipientFields.STATUS_FIELD + ":5.1.1 (some comment)")
        NameValueField field = recipients.statusField
        assertEquals(RecipientFields.STATUS_FIELD, field.name)
        assertEquals("5.1.1 (some comment)", field.getValue(false))
        assertEquals("5.1.1", recipients.getStatusCode().status)
    }
    @Test
    void testGetStatusCodeWithStangeComment() {
        RecipientFields recipients = createRecipients(RecipientFields.STATUS_FIELD + ":5.1.1 some comment about 5.1.1")
        NameValueField field = recipients.statusField
        assertEquals(RecipientFields.STATUS_FIELD, field.name)
        assertEquals("5.1.1 some comment about 5.1.1", field.getValue(false))
        assertEquals("5.1.1", recipients.getStatusCode().status)
    }
    @Test
    void testGetStatusCodeWithoutComment() {
        RecipientFields recipients = createRecipients(RecipientFields.STATUS_FIELD + ":5.1.2")
        NameValueField field = recipients.statusField
        assertEquals(RecipientFields.STATUS_FIELD, field.name)
        assertEquals("5.1.2", field.getValue(false))
        assertEquals("5.1.2", recipients.getStatusCode().status)
    }
    @Test
    void testGetAction() {
        RecipientFields recipients = createRecipients(RecipientFields.ACTION_FIELD + ":delayed")
        NameValueField field = recipients.actionField
        assertEquals(RecipientFields.ACTION_FIELD, field.name)
        assertEquals("delayed", field.getValue(false))
        assertEquals("delayed", recipients.getAction())
        assertEquals("delayed", recipients.getAction(false))
    }
    @Test
    void testGetLastAttemptDate() {
        RecipientFields recipients = createRecipients(RecipientFields.LAST_ATTEMPT_DATE_FIELD + ": Thu, 7 Jul 1994 17:15:49 (GMT) -0400")
        NameValueField field = recipients.lastAttemptDateField
        assertEquals(RecipientFields.LAST_ATTEMPT_DATE_FIELD, field.name)
        assertEquals("Thu, 7 Jul 1994 17:15:49 (GMT) -0400", field.getValue(false))
        assertEquals("Thu, 7 Jul 1994 17:15:49 (GMT) -0400", recipients.getLastAttemptDate())
    }
    @Test
    void testOptionalLastAttemptDate() {
        RecipientFields recipients = createRecipients()
        assertNull(recipients.lastAttemptDateField)
        assertNull(recipients.getLastAttemptDate())
    }
    @Test
    void testOptionalWillRetryUntil() {
        RecipientFields recipients = createRecipients()
        assertNull(recipients.willRetryUntilField)
        assertNull(recipients.getWillRetryUntilField())
    }
    @Test
    void testOptionalFinalLogId() {
        RecipientFields recipients = createRecipients()
        assertNull(recipients.finalLogIdField)
        assertNull(recipients.getFinalLogIdField())
    }
    @Test
    void testOptionalDiagnosticCode() {
        RecipientFields recipients = createRecipients()
        assertNull(recipients.diagnosticCodeField)
        assertNull(recipients.diagnosticCodeTypeText)
        assertNull(recipients.diagnosticCodeType)
    }
    @Test
    void testOptionalOriginalRecipient() {
        RecipientFields recipients = createRecipients()
        assertNull(recipients.originalRecipientField)
        assertNull(recipients.originalRecipientAddressType)
        assertNull(recipients.originalRecipientAddress)
    }
    @Test
    void testOptionalRemoteMta() {
        RecipientFields recipients = createRecipients()
        assertNull(recipients.remoteMtaField)
        assertNull(recipients.remoteMtaName)
        assertNull(recipients.remoteMtaType)
    }
    @Test
    void testGetWillRetryUntil() {
        RecipientFields recipients = createRecipients(RecipientFields.WILL_RETRY_UNTIL_FIELD + ": Thu, 7 Jul 1994 17:15:49 (GMT) -0400")
        NameValueField field = recipients.willRetryUntilField
        assertEquals(RecipientFields.WILL_RETRY_UNTIL_FIELD, field.name)
        assertEquals("Thu, 7 Jul 1994 17:15:49 (GMT) -0400", field.getValue(false))
        assertEquals("Thu, 7 Jul 1994 17:15:49 (GMT) -0400", recipients.getWillRetryUntil())
    }
    @Test
    void testGetFinalLogIdField() {
        RecipientFields recipients = createRecipients(RecipientFields.FINAL_LOG_ID_FIELD + ":some text(comment)")
        NameValueField field = recipients.finalLogIdField
        assertEquals(RecipientFields.FINAL_LOG_ID_FIELD, field.name)
        assertEquals("some text(comment)", field.getValue(false))
        assertEquals("some text", recipients.getFinalLogId())
        assertEquals("some text(comment)", recipients.getFinalLogId(false))
    }
    @Test
    void testGetRecipientAddressWithOriginal() {
        RecipientFields recipients = createRecipients(RecipientFields.ORIGINAL_RECIPIENT_FIELD + ":rfc822;someuser@someddress.com")
        String address = recipients.getEmailAddress()
        assertEquals("someuser@someddress.com", address)
    }
    @Test
    void testGetRecipientAddressWithFinal() {
        RecipientFields recipients = createRecipients(RecipientFields.FINAL_RECIPIENT_FIELD + ":rfc822;someuser@someddress.com")
        String address = recipients.getEmailAddress()
        assertEquals("someuser@someddress.com", address)
    }
    @Test
    void testGetOriginalRecipientAddress() {
        RecipientFields recipients = createRecipients(RecipientFields.ORIGINAL_RECIPIENT_FIELD + ":rfc822;someuser@someddress.com")
        NameValueField field = recipients.originalRecipientField
        assertEquals(RecipientFields.ORIGINAL_RECIPIENT_FIELD, field.name)
        assertEquals("rfc822;someuser@someddress.com", field.getValue(false))
        assertEquals("rfc822", recipients.getOriginalRecipientAddressType())
        assertEquals("someuser@someddress.com", recipients.getOriginalRecipientAddress())
    }
    @Test
    void testGetFinalRecipientAddress() {
        RecipientFields recipients = createRecipients(RecipientFields.FINAL_RECIPIENT_FIELD + ":rfc822;someuser@someddress.com")
        NameValueField field = recipients.finalRecipientField
        assertEquals(RecipientFields.FINAL_RECIPIENT_FIELD, field.name)
        assertEquals("rfc822;someuser@someddress.com", field.getValue(false))
        assertEquals("rfc822", recipients.getFinalRecipientAddressType())
        assertEquals("someuser@someddress.com", recipients.getFinalRecipientAddress())
    }
    @Test
    void testGetRemoteMta() {
        RecipientFields recipients = createRecipients(RecipientFields.REMOTE_MTA_FIELD + ":dns;gmail-smtp-in.l.google.com",
         " (TCP|17.172.220.236|38163|173.194.79.27|25)",
         " (mx.google.com ESMTP kb5si627274pbc.17 - gsmtp)")
        NameValueField field = recipients.remoteMtaField
        assertEquals(RecipientFields.REMOTE_MTA_FIELD, field.name)
        assertEquals("dns;gmail-smtp-in.l.google.com" +
                " (TCP|17.172.220.236|38163|173.194.79.27|25)" +
                " (mx.google.com ESMTP kb5si627274pbc.17 - gsmtp)", field.getValue(false))
        assertEquals("dns", recipients.getRemoteMtaType())
        assertEquals("gmail-smtp-in.l.google.com", recipients.getRemoteMtaName())
    }
    @Test
    void testGetDiagnosticCode() {
        RecipientFields recipients = createRecipients(RecipientFields.DIAGNOSITC_CODE_FIELD + ":smtp;550-5.1.1 The email account that you tried to reach does" +
                " not exist. Please try the recipient's email address for typos or spaces. Learn" +
                " more at http://support.google.com/mail/bin/answer.py?answer=6596 kb5si627274")
        NameValueField field = recipients.diagnosticCodeField
        assertEquals(RecipientFields.DIAGNOSITC_CODE_FIELD, field.name)
        assertEquals("smtp;550-5.1.1 The email account that you tried to reach does" +
                        " not exist. Please try the recipient's email address for typos or spaces. Learn" +
                        " more at http://support.google.com/mail/bin/answer.py?answer=6596 kb5si627274", field.getValue(false))
        assertEquals("smtp", recipients.getDiagnosticCodeType())
        assertEquals("550-5.1.1 The email account that you tried to reach does" +
                " not exist. Please try the recipient's email address for typos or spaces. Learn" +
                " more at http://support.google.com/mail/bin/answer.py?answer=6596 kb5si627274", recipients.getDiagnosticCodeTypeText())
    }

    private RecipientFields createRecipients(String... lines) {
        RecipientFields fields = new RecipientFields()
        if (lines) {
            NameValueField field = new NameValueField()
            lines.each { String line ->
                field.append(line)
            }
            fields.addField(field)
        }
        return fields
    }
}
