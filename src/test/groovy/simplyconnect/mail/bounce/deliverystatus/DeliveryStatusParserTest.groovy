package simplyconnect.mail.bounce.deliverystatus

import org.junit.Test
import simplyconnect.mail.fields.NameValueField

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertNull

/**
 * User: david
 * Date: 27/03/13
 * Time: 11:23 PM
 */
class DeliveryStatusParserTest {
    @Test
    void testParseSingleRecipient() {
    String content = """Original-envelope-id: 0MK8003W0MS2RC00@st11p02mm-asmtp001.mac.com
Reporting-MTA: dns;st11p02mm-asmtp001.mac.com (tcp-daemon)
DSN-Gateway: dns; mail-f.bcc.ac.uk
Received-From-MTA: dns; omta13.westchester.pa.mail.comcast.net [76.96.62.52]
Arrival-date: Mon, 25 Mar 2013 22:36:05 +0000 (GMT)

Original-recipient: rfc822;xsome-email-that-does-not-exist@gmail.com
Final-recipient: rfc822;some-email-that-does-not-exist@gmail.com
Action: failed
Status: 5.1.1 (Remote SMTP server has rejected address)
Remote-MTA: dns;gmail-smtp-in.l.google.com
 (TCP|17.172.220.236|38163|173.194.79.27|25)
 (mx.google.com ESMTP kb5si627274pbc.17 - gsmtp)
Diagnostic-code: smtp;550-5.1.1 The email account that you tried to reach does
 not exist. Please try the recipient's email address for typos or spaces. Learn
 more at http://support.google.com/mail/bin/answer.py?answer=6596 kb5si627274
Last-Attempt-Date: Thu, 6 Jul 1994 17:15:49 -0400
Final-Log-ID: some log id
Will-Retry-Until: Thu, 7 Jul 1994 17:15:49 -0400"""
        DeliveryStatusNotification dsn = new DeliveryStatusParser().parse(content)
        checkField(DeliveryStatusNotification.REPORTING_MTA_FIELD, "dns;st11p02mm-asmtp001.mac.com (tcp-daemon)", dsn.getReportingMtaField())
        checkField(DeliveryStatusNotification.ARRIVAL_DATE_FIELD, "Mon, 25 Mar 2013 22:36:05 +0000 (GMT)", dsn.getArrivalDateField())
        checkField(DeliveryStatusNotification.DSN_GATEWAY_FIELD, "dns; mail-f.bcc.ac.uk", dsn.getDsnGatewayField())
        checkField(DeliveryStatusNotification.ORIGINAL_ENVELOPE_ID_FIELD, "0MK8003W0MS2RC00@st11p02mm-asmtp001.mac.com", dsn.getOriginalEnvelopeIdField())
        checkField(DeliveryStatusNotification.RECEIVED_FROM_MTA_FIELD, "dns; omta13.westchester.pa.mail.comcast.net [76.96.62.52]", dsn.getReceivedFromMtaField())
        List<RecipientFields> recipients = dsn.getRecipients()
        assertEquals(1, recipients.size())
        RecipientFields recipient = recipients.first()
        checkField(RecipientFields.FINAL_RECIPIENT_FIELD,"rfc822;some-email-that-does-not-exist@gmail.com", recipient.getFinalRecipientField())
        checkField(RecipientFields.ORIGINAL_RECIPIENT_FIELD,"rfc822;xsome-email-that-does-not-exist@gmail.com", recipient.getOriginalRecipientField())
        checkField(RecipientFields.ACTION_FIELD,"failed", recipient.getActionField())
        checkField(RecipientFields.STATUS_FIELD,"5.1.1 (Remote SMTP server has rejected address)", recipient.getStatusField())
        checkField(RecipientFields.REMOTE_MTA_FIELD,"dns;gmail-smtp-in.l.google.com" +
                " (TCP|17.172.220.236|38163|173.194.79.27|25)" +
                " (mx.google.com ESMTP kb5si627274pbc.17 - gsmtp)", recipient.getRemoteMtaField())
        checkField(RecipientFields.DIAGNOSITC_CODE_FIELD,"smtp;550-5.1.1 The email account that you tried to reach does" +
                " not exist. Please try the recipient's email address for typos or spaces. Learn" +
                " more at http://support.google.com/mail/bin/answer.py?answer=6596 kb5si627274", recipient.getDiagnosticCodeField())
        checkField(RecipientFields.WILL_RETRY_UNTIL_FIELD,"Thu, 7 Jul 1994 17:15:49 -0400", recipient.getWillRetryUntilField())
        checkField(RecipientFields.LAST_ATTEMPT_DATE_FIELD,"Thu, 6 Jul 1994 17:15:49 -0400", recipient.getLastAttemptDateField())
        checkField(RecipientFields.FINAL_LOG_ID_FIELD,"some log id", recipient.getFinalLogIdField())
    }
    @Test
    void testParseSingleRecipientOrderDifferent() {
    String content = """Original-envelope-id: 0MK8003W0MS2RC00@st11p02mm-asmtp001.mac.com
Reporting-MTA: dns;st11p02mm-asmtp001.mac.com (tcp-daemon)
DSN-Gateway: dns; mail-f.bcc.ac.uk
Received-From-MTA: dns; omta13.westchester.pa.mail.comcast.net [76.96.62.52]
Arrival-date: Mon, 25 Mar 2013 22:36:05 +0000 (GMT)

Action: failed
Status: 5.1.1 (Remote SMTP server has rejected address)
Original-recipient: rfc822;xsome-email-that-does-not-exist@gmail.com
Final-recipient: rfc822;some-email-that-does-not-exist@gmail.com
Remote-MTA: dns;gmail-smtp-in.l.google.com
 (TCP|17.172.220.236|38163|173.194.79.27|25)
 (mx.google.com ESMTP kb5si627274pbc.17 - gsmtp)
Diagnostic-code: smtp;550-5.1.1 The email account that you tried to reach does
 not exist. Please try the recipient's email address for typos or spaces. Learn
 more at http://support.google.com/mail/bin/answer.py?answer=6596 kb5si627274
Last-Attempt-Date: Thu, 6 Jul 1994 17:15:49 -0400
Final-Log-ID: some log id
Will-Retry-Until: Thu, 7 Jul 1994 17:15:49 -0400"""
        DeliveryStatusNotification dsn = new DeliveryStatusParser().parse(content)
        checkField(DeliveryStatusNotification.REPORTING_MTA_FIELD, "dns;st11p02mm-asmtp001.mac.com (tcp-daemon)", dsn.getReportingMtaField())
        checkField(DeliveryStatusNotification.ARRIVAL_DATE_FIELD, "Mon, 25 Mar 2013 22:36:05 +0000 (GMT)", dsn.getArrivalDateField())
        checkField(DeliveryStatusNotification.DSN_GATEWAY_FIELD, "dns; mail-f.bcc.ac.uk", dsn.getDsnGatewayField())
        checkField(DeliveryStatusNotification.ORIGINAL_ENVELOPE_ID_FIELD, "0MK8003W0MS2RC00@st11p02mm-asmtp001.mac.com", dsn.getOriginalEnvelopeIdField())
        checkField(DeliveryStatusNotification.RECEIVED_FROM_MTA_FIELD, "dns; omta13.westchester.pa.mail.comcast.net [76.96.62.52]", dsn.getReceivedFromMtaField())
        List<RecipientFields> recipients = dsn.getRecipients()
        assertEquals(1, recipients.size())
        RecipientFields recipient = recipients.first()
        checkField(RecipientFields.FINAL_RECIPIENT_FIELD,"rfc822;some-email-that-does-not-exist@gmail.com", recipient.getFinalRecipientField())
        checkField(RecipientFields.ORIGINAL_RECIPIENT_FIELD,"rfc822;xsome-email-that-does-not-exist@gmail.com", recipient.getOriginalRecipientField())
        assertEquals("failed", recipient.getAction())
        checkField(RecipientFields.ACTION_FIELD,"failed", recipient.getActionField())
        checkField(RecipientFields.STATUS_FIELD,"5.1.1 (Remote SMTP server has rejected address)", recipient.getStatusField())
        checkField(RecipientFields.REMOTE_MTA_FIELD,"dns;gmail-smtp-in.l.google.com" +
                " (TCP|17.172.220.236|38163|173.194.79.27|25)" +
                " (mx.google.com ESMTP kb5si627274pbc.17 - gsmtp)", recipient.getRemoteMtaField())
        checkField(RecipientFields.DIAGNOSITC_CODE_FIELD,"smtp;550-5.1.1 The email account that you tried to reach does" +
                " not exist. Please try the recipient's email address for typos or spaces. Learn" +
                " more at http://support.google.com/mail/bin/answer.py?answer=6596 kb5si627274", recipient.getDiagnosticCodeField())
        checkField(RecipientFields.WILL_RETRY_UNTIL_FIELD,"Thu, 7 Jul 1994 17:15:49 -0400", recipient.getWillRetryUntilField())
        checkField(RecipientFields.LAST_ATTEMPT_DATE_FIELD,"Thu, 6 Jul 1994 17:15:49 -0400", recipient.getLastAttemptDateField())
        checkField(RecipientFields.FINAL_LOG_ID_FIELD,"some log id", recipient.getFinalLogIdField())
    }
    @Test
    void testParseMultipleRecipients() {
    String content = """Reporting-MTA: dns;st11p02mm-asmtp001.mac.com (tcp-daemon)

Final-recipient: rfc822;some-email-that-does-not-exist@gmail.com
Action: failed
Status: 5.1.1 (Remote SMTP server has rejected address)
Final-Log-ID: some log id

Final-recipient: rfc822;some-email@gmail.com
Action: delayed
Status: 4.0.0
Final-Log-ID: some id
"""
        DeliveryStatusNotification dsn = new DeliveryStatusParser().parse(content)
        checkField(DeliveryStatusNotification.REPORTING_MTA_FIELD, "dns;st11p02mm-asmtp001.mac.com (tcp-daemon)", dsn.getReportingMtaField())
        assertNull(dsn.getArrivalDateField())
        assertNull(dsn.getDsnGatewayField())
        assertNull(dsn.getOriginalEnvelopeIdField())
        assertNull(dsn.getReceivedFromMtaField())

        List<RecipientFields> recipients = dsn.getRecipients()
        assertEquals(2, recipients.size())
        RecipientFields recipient = recipients.first()
        checkField(RecipientFields.FINAL_RECIPIENT_FIELD,"rfc822;some-email-that-does-not-exist@gmail.com", recipient.getFinalRecipientField())
        assertNull(recipient.getOriginalRecipientField())
        assertNull(recipient.getRemoteMtaField())
        assertNull(recipient.getDiagnosticCodeField())
        assertNull(recipient.getLastAttemptDateField())
        assertNull(recipient.getWillRetryUntilField())
        checkField(RecipientFields.ACTION_FIELD,"failed", recipient.getActionField())
        checkField(RecipientFields.STATUS_FIELD,"5.1.1 (Remote SMTP server has rejected address)", recipient.getStatusField())
        checkField(RecipientFields.FINAL_LOG_ID_FIELD,"some log id", recipient.getFinalLogIdField())

        recipient = recipients.get(1)
        checkField(RecipientFields.FINAL_RECIPIENT_FIELD,"rfc822;some-email@gmail.com", recipient.getFinalRecipientField())
        assertNull(recipient.getOriginalRecipientField())
        assertNull(recipient.getRemoteMtaField())
        assertNull(recipient.getDiagnosticCodeField())
        assertNull(recipient.getLastAttemptDateField())
        assertNull(recipient.getWillRetryUntilField())
        checkField(RecipientFields.ACTION_FIELD,"delayed", recipient.getActionField())
        checkField(RecipientFields.STATUS_FIELD,"4.0.0", recipient.getStatusField())
        checkField(RecipientFields.FINAL_LOG_ID_FIELD,"some id", recipient.getFinalLogIdField())
    }
    @Test
    void testParseMultipleRecipientsFieldOrderChangedWithCustomFieldAsFirstField() {
    String content = """Reporting-MTA: dns;st11p02mm-asmtp001.mac.com (tcp-daemon)

X-Custom-Field: some value
Final-recipient: rfc822;some-email-that-does-not-exist@gmail.com
Action: failed
Status: 5.1.1 (Remote SMTP server has rejected address)
Final-Log-ID: some log id

Action: delayed
Status: 4.0.0
Final-Log-ID: some id
Final-recipient: rfc822;some-email@gmail.com
"""
        DeliveryStatusNotification dsn = new DeliveryStatusParser().parse(content)
        checkField(DeliveryStatusNotification.REPORTING_MTA_FIELD, "dns;st11p02mm-asmtp001.mac.com (tcp-daemon)", dsn.getReportingMtaField())
        assertNull(dsn.getArrivalDateField())
        assertNull(dsn.getDsnGatewayField())
        assertNull(dsn.getOriginalEnvelopeIdField())
        assertNull(dsn.getReceivedFromMtaField())

        List<RecipientFields> recipients = dsn.getRecipients()
        assertEquals(2, recipients.size())
        RecipientFields recipient = recipients.first()
        checkField(RecipientFields.FINAL_RECIPIENT_FIELD,"rfc822;some-email-that-does-not-exist@gmail.com", recipient.getFinalRecipientField())
        assertNull(recipient.getOriginalRecipientField())
        assertNull(recipient.getRemoteMtaField())
        assertNull(recipient.getDiagnosticCodeField())
        assertNull(recipient.getLastAttemptDateField())
        assertNull(recipient.getWillRetryUntilField())
        checkField("X-Custom-Field","some value", recipient.get("X-Custom-Field"))
        checkField(RecipientFields.ACTION_FIELD,"failed", recipient.getActionField())
        checkField(RecipientFields.STATUS_FIELD,"5.1.1 (Remote SMTP server has rejected address)", recipient.getStatusField())
        checkField(RecipientFields.FINAL_LOG_ID_FIELD,"some log id", recipient.getFinalLogIdField())

        recipient = recipients.get(1)
        checkField(RecipientFields.FINAL_RECIPIENT_FIELD,"rfc822;some-email@gmail.com", recipient.getFinalRecipientField())
        assertNull(recipient.getOriginalRecipientField())
        assertNull(recipient.getRemoteMtaField())
        assertNull(recipient.getDiagnosticCodeField())
        assertNull(recipient.getLastAttemptDateField())
        assertNull(recipient.getWillRetryUntilField())
        checkField(RecipientFields.ACTION_FIELD,"delayed", recipient.getActionField())
        checkField(RecipientFields.STATUS_FIELD,"4.0.0", recipient.getStatusField())
        checkField(RecipientFields.FINAL_LOG_ID_FIELD,"some id", recipient.getFinalLogIdField())
    }

    void checkField(String fieldName, String fieldValue, NameValueField field) {
        assertNotNull("field $fieldName was not set", field)
        assertEquals(fieldName.toLowerCase(), field.name.toLowerCase())
        assertEquals(fieldValue, field.getValue(false))

    }
}
