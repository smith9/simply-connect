package simplyconnect.mail.bounce.config

import org.junit.Before
import org.junit.Test
import simplyconnect.mail.bounce.deliverystatus.StatusCode

import static junit.framework.TestCase.assertEquals

/**
 * User: david
 * Date: 18/04/13
 * Time: 9:43 PM
 */
class StatusCodeConfigTest {
    StatusCodeConfig config

    @Before
    public void setUp() throws Exception {
        config = new StatusCodeConfig(new File("src/main/resources/dsn.properties"))
    }

    @Test
    void testGetDescription() {
        checkDescriptions("4")
        checkDescriptions("5")
        
    }

    private void checkDescriptions(String prefix) {
        assertEquals("Other address status", config.getDescription(new StatusCode(prefix + ".1.0")))
        assertEquals("Bad destination mailbox address", config.getDescription(new StatusCode(prefix + ".1.1")))
        assertEquals("Bad destination system address", config.getDescription(new StatusCode(prefix + ".1.2")))
        assertEquals("Bad destination mailbox address syntax", config.getDescription(new StatusCode(prefix + ".1.3")))
        assertEquals("Destination mailbox address ambiguous", config.getDescription(new StatusCode(prefix + ".1.4")))
        assertEquals("Destination mailbox address valid", config.getDescription(new StatusCode(prefix + ".1.5")))
        assertEquals("Mailbox has moved", config.getDescription(new StatusCode(prefix + ".1.6")))
        assertEquals("Bad sender's mailbox address syntax", config.getDescription(new StatusCode(prefix + ".1.7")))
        assertEquals("Bad sender's system address", config.getDescription(new StatusCode(prefix + ".1.8")))
        assertEquals("Other or undefined mailbox status", config.getDescription(new StatusCode(prefix + ".2.0")))
        assertEquals("Mailbox disabled, not accepting messages", config.getDescription(new StatusCode(prefix + ".2.1")))
        assertEquals("Mailbox full", config.getDescription(new StatusCode(prefix + ".2.2")))
        assertEquals("Message length exceeds administrative limit.", config.getDescription(new StatusCode(prefix + ".2.3")))
        assertEquals("Mailing list expansion problem", config.getDescription(new StatusCode(prefix + ".2.4")))
        assertEquals("Other or undefined mail system status", config.getDescription(new StatusCode(prefix + ".3.0")))
        assertEquals("Mail system full", config.getDescription(new StatusCode(prefix + ".3.1")))
        assertEquals("System not accepting network messages", config.getDescription(new StatusCode(prefix + ".3.2")))
        assertEquals("System not capable of selected features", config.getDescription(new StatusCode(prefix + ".3.3")))
        assertEquals("Message too big for system", config.getDescription(new StatusCode(prefix + ".3.4")))
        assertEquals("Other or undefined network or routing status", config.getDescription(new StatusCode(prefix + ".4.0")))
        assertEquals("No answer from host", config.getDescription(new StatusCode(prefix + ".4.1")))
        assertEquals("Bad connection", config.getDescription(new StatusCode(prefix + ".4.2")))
        assertEquals("Routing server failure", config.getDescription(new StatusCode(prefix + ".4.3")))
        assertEquals("Unable to route", config.getDescription(new StatusCode(prefix + ".4.4")))
        assertEquals("Network congestion", config.getDescription(new StatusCode(prefix + ".4.5")))
        assertEquals("Routing loop detected", config.getDescription(new StatusCode(prefix + ".4.6")))
        assertEquals("Delivery time expired", config.getDescription(new StatusCode(prefix + ".4.7")))
        assertEquals("Other or undefined protocol status", config.getDescription(new StatusCode(prefix + ".5.0")))
        assertEquals("Invalid command", config.getDescription(new StatusCode(prefix + ".5.1")))
        assertEquals("Syntax error", config.getDescription(new StatusCode(prefix + ".5.2")))
        assertEquals("Too many recipients", config.getDescription(new StatusCode(prefix + ".5.3")))
        assertEquals("Invalid command arguments", config.getDescription(new StatusCode(prefix + ".5.4")))
        assertEquals("Wrong protocol version", config.getDescription(new StatusCode(prefix + ".5.5")))
        assertEquals("Other or undefined media error", config.getDescription(new StatusCode(prefix + ".6.0")))
        assertEquals("Media not supported", config.getDescription(new StatusCode(prefix + ".6.1")))
        assertEquals("Conversion required and prohibited", config.getDescription(new StatusCode(prefix + ".6.2")))
        assertEquals("Conversion required but not supported", config.getDescription(new StatusCode(prefix + ".6.3")))
        assertEquals("Conversion with loss performed", config.getDescription(new StatusCode(prefix + ".6.4")))
        assertEquals("Conversion failed", config.getDescription(new StatusCode(prefix + ".6.5")))
        assertEquals("Other or undefined security status", config.getDescription(new StatusCode(prefix + ".7.0")))
        assertEquals("Delivery not authorized, message refused", config.getDescription(new StatusCode(prefix + ".7.1")))
        assertEquals("Mailing list expansion prohibited", config.getDescription(new StatusCode(prefix + ".7.2")))
        assertEquals("Security conversion required but not possible", config.getDescription(new StatusCode(prefix + ".7.3")))
        assertEquals("Security features not supported", config.getDescription(new StatusCode(prefix + ".7.4")))
        assertEquals("Cryptographic failure", config.getDescription(new StatusCode(prefix + ".7.5")))
        assertEquals("Cryptographic algorithm not supported", config.getDescription(new StatusCode(prefix + ".7.6")))
        assertEquals("Message integrity failure", config.getDescription(new StatusCode(prefix + ".7.7")))
    }
}
