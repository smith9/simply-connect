package simplyconnect.mail.bounce.categoryfile

import org.apache.commons.io.FileUtils
import org.junit.Before
import org.junit.Test
import simplyconnect.mail.MailFileUtils
import simplyconnect.mail.MessageWrapper
import simplyconnect.mail.TestUtil
import simplyconnect.mail.bounce.categoriser.Categoriser
import simplyconnect.mail.bounce.categoriser.MessageResults

/**
 * User: david
 * Date: 17/04/13
 * Time: 11:39 PM
 */
class CategoriserUtilTest {
    private File outputDir
    @Before
    void before() {
        outputDir = new File("target/CategoriserUtilTest")
        outputDir.mkdirs()
        FileUtils.cleanDirectory(outputDir)
    }

    @Test
    void testToTableFile() {
        Categoriser categoriser = new Categoriser(new File("src/main/resources/bounce.xml"), new File("src/main/resources/dsn.properties"))
        List<File> inputs = new File("src/test/resources/sampleemails").listFiles({dir, file-> file ==~ /.*?\.eml/ } as FilenameFilter)
        List<MessageWrapper> messages = MailFileUtils.load(inputs)

        List<MessageResults> categorised = CategoriserUtil.categorise(categoriser, messages)
        List<MessageResult> result = CategoriserUtil.toCategorisedResult(categorised)
        File tableFile = CategoriserUtil.toTableFile(result, outputDir)
        File expectedTable = new File("src/test/resources/sampleemails/expected.txt")
        TestUtil.assertEqualsIgnoreEndings(expectedTable.text, tableFile.text)
    }
}
