package simplyconnect.mail.bounce

import org.apache.commons.mail.util.MimeMessageUtils
import org.junit.BeforeClass
import org.junit.Test
import simplyconnect.mail.MessageWrapper
import simplyconnect.mail.bounce.categoriser.Categoriser
import simplyconnect.mail.bounce.categoriser.CategoryResult
import simplyconnect.mail.bounce.categoriser.MessageResults
import simplyconnect.mail.bounce.config.ConfigurationException

import javax.mail.internet.MimeMessage

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue

/**
 * User: david
 * Date: 25/03/13
 * Time: 8:35 AM
 */
class CategoriserTest {
    static Categoriser categoriser
    @BeforeClass
    public static void beforeClass() {
        categoriser = new Categoriser(new File("src/main/resources/bounce.xml"), new File("src/main/resources/dsn.properties"))
    }
    @Test(expected=ConfigurationException)
    void testConfigError() {
        new Categoriser(new File("src/test/resources/bounce/bounce-error.xml"), new File("src/main/resources/dsn.properties"))
    }

    @Test
    void testOutOfOfficeSubject() {
        checkCategoriseOutOfOffice("Out of office, blah", "some content")
    }
    @Test
    void testOutOfOfficeContent() {
        checkCategoriseOutOfOffice("some subject", "Out of office, blah")
    }
    @Test
    void testOutOfMyOffice() {
        checkCategoriseOutOfOffice("some subject", "I am currently out of my office and cannot check my email regularly.")
    }
    @Test
    void testOnVacation() {
        checkCategoriseOutOfOffice("some subject", "I am currently on vacation and cannot check my email regularly.")
    }
    void checkCategoriseOutOfOffice(String subject, String content) {
        checkCategory(loadMessage("template-autoreply.eml", subject, content),
                CategoryResult.TYPE_INFO,
                CategoryResult.CATEGORY_AUTOREPLY,
                "out-of-office",
                "delivered",
                null)
    }
    @Test
    void testMailboxFull() {
        checkCategory(loadMessage("template-fail-with-original-msg.eml", "some subject", "This message was created automatically by mail delivery software.\n" +
                "\n" +
                "A message that you sent could not be delivered to one or more of its\n" +
                "recipients. This is a permanent error. The following address(es) failed:\n" +
                "\n" +
                "  someone-not-reached@somehost.com.au\n" +
                "    mailbox is full: retry timeout exceeded"),
                CategoryResult.TYPE_NOT_DELIVERED,
                CategoryResult.CATEGORY_SOFT,
                "Mailbox full",
                "failed",
                "4.2.2")
    }
    @Test
    void testOtherAddressStatus() {
        checkCategory(loadMessage("template-fail-with-original-msg.eml", "some subject", "This is an automatically generated Delivery Status Notification\n" +
                "\n" +
                "Delivery to the following recipient failed permanently:\n" +
                "\n" +
                "     credit@somehost.net.au\n" +
                "\n" +
                "Technical details of permanent failure: \n" +
                "Google tried to deliver your message, but it was rejected by the recipient domain. We recommend contacting the other email provider for further information about the cause of this error. The error that the other server returned was: 550 550 #5.1.0 Address rejected credit@somehost.net.au (state 14)."),
                CategoryResult.TYPE_NOT_DELIVERED,
                CategoryResult.CATEGORY_HARD,
                "Other address status",
                "failed",
                "5.1.0")
    }
    @Test
    void testMailboxUnavailable() {
        checkCategory(loadMessage("template-fail-with-original-msg.eml", "some subject", "Delivery to the following recipient failed permanently:\n" +
                "\n" +
                "     someinvalidemailaddress@me.com\n" +
                "\n" +
                "Technical details of permanent failure:\n" +
                "Google tried to deliver your message, but it was rejected by the server for the recipient domain me.com by mx3.me.com.akadns.net. [17.158.8.50].\n" +
                "\n" +
                "The error that the other server returned was:\n" +
                "550 5.1.1 unknown or illegal alias: someinvalidemailaddress@me.com"),
                CategoryResult.TYPE_NOT_DELIVERED,
                CategoryResult.CATEGORY_HARD,
                "Bad destination mailbox address",
                "failed",
                "5.1.1")
    }
    @Test
    void testMailboxUnavailableNoAccount() {
        checkCategory(loadMessage("template-fail-with-original-msg.eml", "some subject", "Delivery to the following recipient failed permanently:\n" +
                "\n" +
                "     some-test-email-unknown@yahoo.com\n" +
                "\n" +
                "Technical details of permanent failure: \n" +
                "Google tried to deliver your message, but it was rejected by the server for the recipient domain yahoo.com by mta7.am0.yahoodns.net. [66.196.118.35].\n" +
                "\n" +
                "The error that the other server returned was:\n" +
                "554 delivery error: dd This user doesn't have a yahoo.com account (some-test-email-unknown@yahoo.com) [-15] - mta1325.mail.bf1.yahoo.com"),
                CategoryResult.TYPE_NOT_DELIVERED,
                CategoryResult.CATEGORY_HARD,
                "Bad destination mailbox address",
                "failed",
                "5.1.1")
    }
    @Test
    void testMailboxUnavailableNoCode() {
        checkCategory(loadMessage("template-fail-with-original-msg.eml", "some subject", "Delivery to the following recipient failed permanently:\n" +
                "\n" +
                "     some-test-email-unknown@hotmail.com\n" +
                "\n" +
                "Technical details of permanent failure: \n" +
                "Google tried to deliver your message, but it was rejected by the server for the recipient domain hotmail.com by mx2.hotmail.com. [65.55.92.184].\n" +
                "\n" +
                "The error that the other server returned was:\n" +
                "550 Requested action not taken: mailbox unavailable"),
                CategoryResult.TYPE_NOT_DELIVERED,
                CategoryResult.CATEGORY_HARD,
                "Bad destination mailbox address",
                "failed",
                "5.1.1")
    }
    @Test
    void testMailboxUnavailableDoesNotExist() {
        checkCategory(loadMessage("template-fail-with-original-msg.eml", "some subject", "Delivery to the following recipient failed permanently:\n" +
                "\n" +
                "     some-test-email-unknown@gmail.com\n" +
                "\n" +
                "Technical details of permanent failure: \n" +
                "The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces. Learn more at http://support.google.com/mail/bin/answer.py?answer=6596"),
                CategoryResult.TYPE_NOT_DELIVERED,
                CategoryResult.CATEGORY_HARD,
                "Bad destination mailbox address",
                "failed",
                "5.1.1")
    }
    @Test
    void testMailboxUnavailableWithHash() {
        checkCategory(loadMessage("template-fail-with-original-msg.eml", "some subject", "Delivery to the following recipient failed permanently:\n" +
                "\n" +
                "     david.smith6-uuid=1234@somehost.com.au\n" +
                "\n" +
                "Technical details of permanent failure: \n" +
                "Google tried to deliver your message, but it was rejected by the server for the recipient domain somehost.com.au by cluster9.us.somelabs.com. [111.11.111.111].\n" +
                "\n" +
                "The error that the other server returned was:\n" +
                "550-Invalid recipient <david.smith6-uuid=1234@somehost.com.au\n" +
                "550 > (#5.1.1)"),
                CategoryResult.TYPE_NOT_DELIVERED,
                CategoryResult.CATEGORY_HARD,
                "Bad destination mailbox address",
                "failed",
                "5.1.1")
    }
    @Test
    void testDomainIssueInRecipientAddress() {
        checkCategory(loadMessage("template-fail-with-original-msg.eml", "some subject", "Delivery to the following recipient failed permanently:\n" +
                "\n" +
                "     info@somedomain.com.au\n" +
                "\n" +
                "Technical details of permanent failure:=20\n" +
                "Google tried to deliver your message, but it was rejected by the recipient =\n" +
                "domain. We recommend contacting the other email provider for further inform=\n" +
                "ation about the cause of this error. The error that the other server return=\n" +
                "ed was: 550 550 No such domain at this location (info@somedomain.com.au) (st=\n" +
                "ate 14)."),
                CategoryResult.TYPE_NOT_DELIVERED,
                CategoryResult.CATEGORY_HARD,
                "Bad destination system address",
                "failed",
                "5.1.2")
    }
    @Test
    void testDomainDNSErrorRecipientAddress() {
        checkCategory(loadMessage("template-fail-with-original-msg.eml", "some subject", "Delivery to the following recipient failed permanently:\n" +
                "\n" +
                "     peter@somehost.plus.com\n" +
                "\n" +
                "Technical details of permanent failure: \n" +
                "DNS Error: Domain name not found"),
                CategoryResult.TYPE_NOT_DELIVERED,
                CategoryResult.CATEGORY_HARD,
                "Bad destination system address",
                "failed",
                "5.1.2")
    }
    @Test
    void testDnsServerNotContactable() {
        checkCategory(loadMessage("template-fail-with-original-msg.eml", "some subject", "Delivery to the following recipient failed permanently:\n" +
                "\n" +
                "     someone@somehost.com\n" +
                "\n" +
                "Technical details of permanent failure: \n" +
                "DNS Error: Could not contact DNS servers"),
                CategoryResult.TYPE_NOT_DELIVERED,
                CategoryResult.CATEGORY_SOFT,
                "Directory server failure",
                "failed",
                "4.4.3")
    }
    @Test
    void testDSN_5_0_0() {
        checkCategory(loadMessage("template-dsn.eml", "", "Original-envelope-id: 0MJY008KPOG6V350@st11p00mm-asmtp002.mac.com\n" +
                "Reporting-MTA: dns;st11p00mm-asmtp002.mac.com (tcp-daemon)\n" +
                "Arrival-date: Wed, 20 Mar 2013 13:36:09 +0000 (GMT)\n" +
                "\n" +
                "Original-recipient: rfc822;mailuser@somehost.com\n" +
                "Final-recipient: rfc822;mailuser@somehost.com\n" +
                "Action: failed\n" +
                "Status: 5.0.0 (Remote SMTP server has rejected address)\n" +
                "Remote-MTA: dns;smtp.secureserver.net (TCP|17.172.81.1|35415|72.167.238.201|25)\n" +
                " (p3pismtp01-016.prod.phx3.secureserver.net ESMTP)\n" +
                "Diagnostic-code: smtp;550 #5.1.0 Address rejected."),
                CategoryResult.TYPE_NOT_DELIVERED,
                CategoryResult.CATEGORY_HARD,
                "Other undefined Status",
                "failed",
                "5.0.0",
                "mailuser@somehost.com")
    }
    @Test
    void testDSN_5_1_1() {
        checkCategory(loadMessage("template-dsn.eml", "", "Original-envelope-id: 0MK8003W0MS2RC00@st11p02mm-asmtp001.mac.com\n" +
                "Reporting-MTA: dns;st11p02mm-asmtp001.mac.com (tcp-daemon)\n" +
                "Arrival-date: Mon, 25 Mar 2013 22:36:05 +0000 (GMT)\n" +
                "\n" +
                "Original-recipient: rfc822;someone-not-reached@somehost.com.au\n" +
                "Final-recipient: rfc822;someone-not-reached@somehost.com.au\n" +
                "Action: failed\n" +
                "Status: 5.1.1 (Remote SMTP server has rejected address)\n" +
                "Remote-MTA: dns;gmail-smtp-in.l.google.com\n" +
                " (TCP|17.172.220.236|38163|173.194.79.27|25)\n" +
                " (mx.google.com ESMTP kb5si627274pbc.17 - gsmtp)\n" +
                "Diagnostic-code: smtp;550-5.1.1 The email account that you tried to reach does\n" +
                " not exist. Please try the recipient's email address for typos or spaces. Learn\n" +
                " more at http://support.google.com/mail/bin/answer.py?answer=6596 kb5si627274"),
                CategoryResult.TYPE_NOT_DELIVERED,
                CategoryResult.CATEGORY_HARD,
                "Bad destination mailbox address",
                "failed",
                "5.1.1",
                "someone-not-reached@somehost.com.au")
    }
    @Test
    void testDSN_5_7_1() {
        checkCategory(loadMessage("template-dsn.eml", "", "Reporting-MTA: dns;int.abc.abc\n" +
                "Received-From-MTA: dns;SOMESERVER.workgroup.com\n" +
                "Arrival-Date: Thu, 27 Jan 2011 22:59:05 +0000\n" +
                "\n" +
                "Final-Recipient: rfc822;DL-SOMEDL@somehost.com.au\n" +
                "Action: failed\n" +
                "Status: 5.7.1\n" +
                "Diagnostic-Code: smtp;550 5.7.1 RESOLVER.RST.AuthRequired; authentication required\n" +
                "X-Display-Name: DL-SOMEDL"),
                CategoryResult.TYPE_NOT_DELIVERED,
                CategoryResult.CATEGORY_HARD,
                "Delivery not authorized, message refused",
                "failed",
                "5.7.1",
                "DL-SOMEDL@somehost.com.au")
    }

    private MessageWrapper loadMessage(String filename) {
        String fileContent = new File("src/test/resources/emails/$filename").text
        return createMessage(fileContent)
    }

    private MessageWrapper loadMessage(String filename, String subject, String content) {
        String fileContent = new File("src/test/resources/emails/$filename").text
        fileContent = fileContent.replaceAll("%CONTENT%", content)
        fileContent = fileContent.replaceAll("%SUBJECT%", subject)
        return createMessage(fileContent)
    }

    private MessageWrapper createMessage(String fileContent) {
        MimeMessage message = MimeMessageUtils.createMimeMessage(null, fileContent)
        MessageWrapper messageWrapper = new MessageWrapper(message)
        return messageWrapper
    }

    private void checkCategory(MessageWrapper message, String expectedType, String expectedCategory, String expectedSubCategory, String expectedAction, String expectedStatusCode, String expectedEmail = null) {
        MessageResults categories = categoriser.categorise(message)
        CategoryResult category = categories.results.first()
        assertEquals(expectedCategory, category.category)
        assertEquals(expectedType, category.type)
        assertEquals(expectedSubCategory, category.subCategory)
        assertEquals(expectedAction, category.action)
        assertEquals(expectedStatusCode, category.statusCode?.status)

        if (expectedEmail) {
            assertEquals(expectedEmail, category.emailAddress)
        }
    }
}
