package simplyconnect.mail

import org.apache.commons.mail.HtmlEmail
import org.apache.commons.mail.util.MimeMessageParser
import org.apache.commons.mail.util.MimeMessageUtils
import org.junit.Test
import simplyconnect.convertor.MessageExtractor

import javax.activation.DataSource
import javax.mail.internet.MimeMessage

import static org.junit.Assert.*

/**
 * User: david
 * Date: 19/02/13
 * Time: 8:25 AM
 */
class MessageWrapperTest extends BaseMailTest {
    @Test
    public void testHasKeyword() throws Exception {
        MessageWrapper message = messageWithKeywords()
        assertTrue(message.hasKeyword("Keyword1"))
    }
    @Test
    public void testGetKeywords() throws Exception {
        MessageWrapper message = messageWithKeywords()
        assertEquals(["Keyword1", "Keyword2", "Keyword3", "Keyword99"], message.getKeywords() as List<String>)
    }

    @Test
    void testHtmlMessage() {
        MessageWrapper messageWrapper = TestUtil.createHtmlMessageWrapper(null)
        assertTrue(messageWrapper.hasHtmlContent())
        assertTrue(messageWrapper.hasPlainContent())
        assertEquals(["cc@somehost.com", "cc2@somehost.com"], messageWrapper.getCcAsString())
        assertEquals(["bcc@somehost.com"], messageWrapper.getBccAsString())
        assertEquals("Some test body text.", messageWrapper.getPlainContent())
        assertEquals("""<html>
  <body>
    <h1>Some Heading</h1>
    <img src='cid:859569AA-EABE-4480-8163-10DC62653606' alt='image some_sig_footer.jpg' width='100' height='50' id='cb926640-5d18-447d-9be7-8cf7a9258d6b' />
    <p>This is some paragraph text</p>
    <div id='results' class='simple'>
      <table border='1'>
        <tr>
          <th>Heading 1</th>
          <th>Heading 2</th>
        </tr>
        <tr>
          <td>
            <p>paragraph in cell 1</p>
          </td>
          <td>some text in cell 2</td>
        </tr>
      </table>
    </div>
  </body>
</html>""", messageWrapper.getHtmlContent())
    }

    @Test
    public void testEmailWithAttachments() throws Exception {
        DataSource dataSource
        MimeMessage message = MimeMessageUtils.createMimeMessage(null, new File("src/test/resources/emails/Test Html Email.eml"))
        MessageWrapper messageWrapper = new MessageWrapper(message)

        assertEquals("Test email", messageWrapper.getSubject())
        assertNotNull(messageWrapper.getMimeMessage())
        assertTrue(messageWrapper.isMultipart())
        assertTrue(messageWrapper.getCc().isEmpty())
        assertEquals([], messageWrapper.getCcAsString())
        assertTrue(messageWrapper.getBcc().isEmpty())
        assertEquals("smith7075@gmail.com", messageWrapper.getFromAsString())
        assertEquals(["smith7075@gmail.com"], messageWrapper.getToAsString())
        assertEquals("smith7075@gmail.com", messageWrapper.getReplyToAsString())
        assertTrue(messageWrapper.hasAttachments())
        List<?> attachmentList = messageWrapper.getAttachmentList()
        assertEquals(1, attachmentList.size())
        assertEquals("<4CC0CA14-3D70-4678-9E0F-955616E98B74@gmail.com>", messageWrapper.getMessageId())
        dataSource = messageWrapper.findAttachmentByName("squigle.png")
        assertNotNull(dataSource)
        assertEquals("image/png", dataSource.getContentType())
        TestUtil.checkImage(dataSource, new File("src/test/resources/images/squigle.png"))
        assertEquals("squigle.png", messageWrapper.getAttachmentName("859569AA-EABE-4480-8163-10DC62653606"))
        assertTrue(messageWrapper.hasHtmlContent())
        assertNotNull(messageWrapper.getHtmlContent())
        assertEquals("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html charset=us-ascii\"></head><body style=\"word-wrap: break-word; -webkit-nbsp-mode: space; -webkit-line-break: after-white-space; \"><div><br></div>An image:&nbsp;<div><img height=\"50\" width=\"100\" apple-width=\"yes\" apple-height=\"yes\" id=\"cb926640-5d18-447d-9be7-8cf7a9258d6b\" src=\"cid:859569AA-EABE-4480-8163-10DC62653606\"></div><div><br></div><div><font size=\"5\">Some large text</font></div><div><font size=\"5\"><br></font></div><div><font size=\"5\"><b>some bold text</b></font></div><div><font size=\"5\"><br></font></div></body></html>",
                messageWrapper.getHtmlContent())
        assertTrue(messageWrapper.hasPlainContent())
        def expectedPlainText = TestUtil.replaceEndings("""
An image:


Some large text

some bold text

""")
        String actualPlainText = TestUtil.replaceEndings(messageWrapper.getPlainContent())
        assertEquals(expectedPlainText, actualPlainText)
    }

    static private MessageWrapper messageWithKeywords() {
        return new MessageWrapper(TestUtil.createHtmlMessageWithInlineImage(null, [keywords: ["Keyword1", "Keyword2", "Keyword3"]]))
    }
}
