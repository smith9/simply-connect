package simplyconnect.mail.fields

import org.junit.Test

import static junit.framework.Assert.assertTrue
import static org.junit.Assert.*

/**
 * User: david
 * Date: 27/03/13
 * Time: 10:15 PM
 */
class FieldParserTest {
    @Test
    void testParseSingleLine() {
        List<Field> fields = new FieldParser().parse("Field:value")
        assertEquals(1, fields.size())
        Field field = fields.first()
        assertEquals("Field", field.name)
        assertEquals("value", field.value)
    }
    @Test
    void testParseMultipleColons() {
        List<Field> fields = new FieldParser().parse("Field:value:at")
        assertEquals(1, fields.size())
        Field field = fields.first()
        assertEquals("Field", field.name)
        assertEquals("value:at", field.value)
    }
    @Test
    void testParseWithBlanksAsideColon() {
        List<Field> fields = new FieldParser().parse("""Field : value""")
        assertEquals(1, fields.size())
        Field field = fields.first()
        assertEquals("Field", field.name)
        assertEquals("value", field.value)
    }
    @Test
    void testParseMultipleLines() {
        List<Field> fields = new FieldParser().parse("""Field:value
Field2:value2""")
        assertEquals(2, fields.size())
        Field field = fields.get(0)
        assertEquals("Field", field.name)
        assertEquals("value", field.value)
        field = fields.get(1)
        assertEquals("Field2", field.name)
        assertEquals("value2", field.value)
    }
    @Test
    void testParseLineContinuation() {
        List<Field> fields = new FieldParser().parse("""Field: value
 is continued
 over multiple lines
Field2:value2""")
        assertEquals(2, fields.size())
        Field field = fields.get(0)
        assertEquals("Field", field.name)
        assertEquals("value is continued over multiple lines", field.value)
        field = fields.get(1)
        assertEquals("Field2", field.name)
        assertEquals("value2", field.value)
    }
    @Test
    void testParseWithBlankLineBetweenFields() {
        List<Field> fields = new FieldParser().parse("""Field:value

Field2:value2""")
        assertEquals(3, fields.size())
        Field field = fields.get(0)
        assertEquals("Field", field.name)
        assertEquals("value", field.value)
        field = fields.get(1)
        assertTrue(field instanceof SeparatorField)
        field = fields.get(2)
        assertEquals("Field2", field.name)
        assertEquals("value2", field.value)
    }
}
