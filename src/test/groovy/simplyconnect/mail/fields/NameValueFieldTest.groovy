package simplyconnect.mail.fields

import org.junit.Test

import static org.junit.Assert.assertEquals

/**
 * User: david
 * Date: 29/03/13
 * Time: 5:06 PM
 */
class NameValueFieldTest {
    @Test
    void testAddingText() {
        NameValueField field = new NameValueField()
        field.append("some key:some value")
        assertEquals("some key", field.name)
        assertEquals("some value", field.value)
    }
    @Test
    void testAddingTextMultipleLines() {
        NameValueField field = new NameValueField()
        field.append("some key:some value")
        field.append(" some more text")
        assertEquals("some key", field.name)
        assertEquals("some value some more text", field.value)
    }
    @Test
    void testGettingValueWithComments() {
        NameValueField field = new NameValueField()
        field.append("some key:some value (with comment)")
        assertEquals("some key", field.name)
        assertEquals("some value (with comment)", field.getValue(false))
    }
    @Test
    void testStrippingComments() {
        NameValueField field = new NameValueField()
        field.append("some key:some(a comment with space to the right) value")
        field.append(" some more (a comment with space to the left)text")
        field.append(" and some more (a comment with space to both sides) text")
        field.append(" and even more  (a comment with spaces to both sides)  text")
        field.append(" and more(a comment with no spaces)text")
        assertEquals("some key", field.name)
        assertEquals("some value some more text and some more text and even more text and more text", field.value)
    }
    @Test
    void testGetValueAroundSemiColon() {
        NameValueField field = new NameValueField()
        field.append("some key:some;value")
        assertEquals("some key", field.name)
        assertEquals("some", field.getValueBeforeSemiColon())
        assertEquals("value", field.getValueAfterSemiColon())
    }
    @Test
    void testGetValueAroundSemiColonTrim() {
        NameValueField field = new NameValueField()
        field.append("some key:some ; value")
        assertEquals("some key", field.name)
        assertEquals("some", field.getValueBeforeSemiColon())
        assertEquals("value", field.getValueAfterSemiColon())
    }
}
