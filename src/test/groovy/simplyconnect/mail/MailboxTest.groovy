package simplyconnect.mail

import com.icegreen.greenmail.util.ServerSetup
import org.joda.time.DateTime
import org.junit.After
import org.junit.Before
import org.junit.Test
import com.icegreen.greenmail.util.GreenMail
import com.icegreen.greenmail.user.GreenMailUser
import com.icegreen.greenmail.util.ServerSetupTest

import javax.mail.MessagingException

import static org.junit.Assert.assertTrue

/**
 * User: david
 * Date: 02/02/13
 */
public class MailboxTest extends BaseMailTest {
    @Before
    public void before() {
        super.before()
        createFolders()
    }

    @Test
    public void testRetrievingMessages() throws IOException, MessagingException {
        int messageCount = 2
        messageCount.times {
            TestUtil.addMessageToInbox()
            sleep(1000)
        }
        Mailbox mailbox = TestUtil.connectToMailbox()
        List<MessageWrapper> messages = mailbox.retrieveMessages(inboxPath)
        TestUtil.checkMessageCount(mailbox, inboxPath, 2)
        TestUtil.checkMessageCount(mailbox, processedPath, 0)
        DateTime firstDate = messages.get(0).receivedDate
        DateTime secondDate = messages.get(1).receivedDate
        assertTrue("$firstDate should be before $secondDate", firstDate.isBefore(secondDate))
    }

    @Test
    void testMovingMessage() {
        int messageCount = 2
        messageCount.times { TestUtil.addMessageToInbox() }
        Mailbox mailbox = TestUtil.connectToMailbox()
        MessageWrapper firstMessage = mailbox.retrieveMessages(inboxPath).first()
        mailbox.moveMessage(firstMessage.message, processedPath)
        TestUtil.checkMessageCount(mailbox, inboxPath, 1)
        TestUtil.checkMessageCount(mailbox, processedPath, 1)
    }
    @Test
    void testMovingMessageWorksToFolderThatDoesntExist() {
        String newFolder = "Inbox/newFolder"
        int messageCount = 2
        messageCount.times { TestUtil.addMessageToInbox() }
        Mailbox mailbox = TestUtil.connectToMailbox()
        MessageWrapper firstMessage = mailbox.retrieveMessages(inboxPath).first()
        mailbox.moveMessage(firstMessage.message, newFolder, true)
        TestUtil.checkMessageCount(mailbox, inboxPath, 1)
        TestUtil.checkMessageCount(mailbox, newFolder, 1)
    }
    @Test(expected = MailException)
    void testMovingMessageErrorsToFolderThatDoesntExist() {
        String newFolder = "Inbox/newFolder"
        int messageCount = 2
        messageCount.times { TestUtil.addMessageToInbox() }
        Mailbox mailbox = TestUtil.connectToMailbox()
        MessageWrapper firstMessage = mailbox.retrieveMessages(inboxPath).first()
        mailbox.moveMessage(firstMessage.message, newFolder, false)
    }

    void createFolders() {
        mailbox.create(inboxPath)
        mailbox.create(createdPath)
        mailbox.create(processedPath)
    }
}
