package simplyconnect.mail

import com.icegreen.greenmail.user.GreenMailUser
import com.icegreen.greenmail.util.GreenMail
import com.icegreen.greenmail.util.ServerSetup
import com.icegreen.greenmail.util.ServerSetupTest
import org.junit.After
import org.junit.Before
import org.junit.Test

import javax.mail.Flags.Flag
import javax.mail.Folder
import javax.mail.Message
import javax.mail.MessagingException

import static junit.framework.Assert.assertEquals
import static junit.framework.Assert.assertTrue
import static org.junit.Assert.assertFalse

/**
 * User: david
 * Date: 02/02/13
 */
public class FolderCacheTest extends BaseMailTest {

    @Test
    public void testGettingFoldersWhenNoneCreated() throws IOException, MessagingException {
        Folder inbox = mailbox.get(inboxPath)
        assertFalse(inbox.open)
        assertTrue(inbox.exists()) // special case, inbox always exists

        Folder processed = mailbox.get(processedPath)
        assertFalse(processed.open)
        assertFalse(processed.exists())
    }
    @Test
    public void testOpenFolderWhenItExists() throws IOException, MessagingException {
        assertTrue(mailbox.open(inboxPath).open)
    }
    @Test(expected = MailException)
    public void testOpenWhenFolderDoesNotExist() throws IOException, MessagingException {
        assertFalse(mailbox.open(processedPath).open)
    }
    @Test
    public void testCanCreateAfterAGet() throws IOException, MessagingException {
        assertFalse(mailbox.get(processedPath).open)
        assertTrue(mailbox.create(processedPath).exists())
    }
    @Test
    public void testClose() throws IOException, MessagingException {
        assertTrue(mailbox.create(processedPath).exists())
        mailbox.close(processedPath)
        assertFalse(mailbox.isOpen(processedPath))
    }
    @Test
    public void testCloseAll() throws IOException, MessagingException {
        assertTrue(mailbox.create(processedPath).exists())
        mailbox.closeAll()
        assertFalse(mailbox.isOpen(inboxPath))
        assertFalse(mailbox.isOpen(processedPath))
    }
    @Test
    public void testExpunge() throws IOException, MessagingException {
        int initialMessageCount = 2
        initialMessageCount.times {
            TestUtil.addMessageToInbox()
        }
        TestUtil.checkMessageCount(mailbox, inboxPath, initialMessageCount)

        int deleteCount = 1
        markDeleted(deleteCount)
        Message[] deletedMessages = mailbox.expunge(inboxPath)
        assertEquals(deleteCount, deletedMessages.size())
        TestUtil.checkMessageCount(mailbox, inboxPath, initialMessageCount - deleteCount)
    }

    void markDeleted(int numberToMarkAsDeleted) {
        List<MessageWrapper> messages = mailbox.retrieveMessages(inboxPath)
        numberToMarkAsDeleted.times { int index ->
            messages.get(index).message.setFlag(Flag.DELETED, true)
        }

    }
}
