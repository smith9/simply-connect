package simplyconnect.mail

import org.junit.Test

import static org.junit.Assert.assertEquals

/**
 * User: david
 * Date: 4/03/13
 * Time: 8:55 PM
 */
class InlineImageHandlerTest extends BaseMailTest {
    @Test
    void testHtmlParsing() {
        Set<ImageDetails> imageDetails = InlineImageHandler.extractInlineImages(TestUtil.complexHtml())
        ImageDetails detail = imageDetails.find { it.cid == "image002.jpg@01CD4B07.C5013F60" }
        assertEquals("cid:image002.jpg@01CD4B07.C5013F60", detail.src)
        assertEquals("Email_Sig_Footer300dpi.jpg", detail.alt)
        detail = imageDetails.find { it.cid == "image001.jpg@01CD4B07.C5013F60" }
        assertEquals("cid:image001.jpg@01CD4B07.C5013F60", detail.src)
        assertEquals("Email_Sig_Footer300dpi.jpg", detail.alt)
    }
    @Test
    void testHtmlExtract() {
        MessageWrapper message = createMessage()
        InlineImageHandler handler = new InlineImageHandler(message)
        ImageDetails detail = handler.getImageByCid("859569AA-EABE-4480-8163-10DC62653606")
        assertEquals("859569AA-EABE-4480-8163-10DC62653606", detail.cid)
        assertEquals("cid:859569AA-EABE-4480-8163-10DC62653606", detail.src)
        assertEquals("image some_sig_footer.jpg", detail.alt)
    }
}
