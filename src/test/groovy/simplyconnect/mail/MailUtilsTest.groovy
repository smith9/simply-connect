package simplyconnect.mail

import org.junit.Test

import static org.junit.Assert.*

/**
 * User: david
 * Date: 22/02/13
 * Time: 9:58 AM
 */
class MailUtilsTest extends BaseMailTest {
    @Test
    void testFilterMessages() {
        TestUtil.addMessageToInbox(keywords:["David", "Mark"])
        TestUtil.addMessageToInbox(keywords:["Lori", "Maya"])
        Mailbox mailbox = TestUtil.connectToMailbox()
        List<MessageWrapper> messages = mailbox.retrieveMessages(inboxPath)
        checkFiltering(messages, ["David"], 1)
        checkFiltering(messages, ["David", "Mark"], 1)
        checkFiltering(messages, ["David", "Maya"], 2)
    }

    void checkFiltering(List<MessageWrapper> messages, List<String> filteredByAnyKeyword, int expectedMessageCount) {
        List<MessageWrapper> filteredMessages = MailUtils.filterMessages(messages, filteredByAnyKeyword as Set<String>)
        assertEquals(expectedMessageCount, filteredMessages.size())
        filteredMessages.each { MessageWrapper wrapper ->
            assertFalse("Filtered by any of these keywords: ${filteredByAnyKeyword} should intersect Message Keywords: ${wrapper.keywords}",
                    wrapper.keywords.intersect(filteredByAnyKeyword as Set<String>).isEmpty())
        }
    }
}
