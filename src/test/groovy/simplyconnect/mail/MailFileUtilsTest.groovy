package simplyconnect.mail

import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import org.junit.Before
import org.junit.Test

import javax.mail.Message
import javax.mail.internet.MimeMessage

import static org.junit.Assert.*

/**
 * User: david
 * Date: 17/04/13
 * Time: 1:25 PM
 */
class MailFileUtilsTest {
    private File outputDir
    @Before
    void before() {
        outputDir = new File("target/MailFileUtilsTest")
        outputDir.mkdirs()
        FileUtils.cleanDirectory(outputDir)

    }

    @Test
    void testWrapper() {
        MimeMessage message1 = TestUtil.createHtmlMessageWithInlineImage(null)
        MimeMessage message2 = TestUtil.createHtmlMessageWithInlineImage(null)
        List<MessageWrapper> messages = MailFileUtils.toWrapper([message1, message2] as List<Message>)
        assertEquals(message1, messages.get(0).message)
        assertEquals(message2, messages.get(1).message)
    }
    @Test
    void testLoadAndSaveMultipleFiles() {
        File inputEmail = new File("src/test/resources/emails/Test Html Email.eml")
        File inputEmail2 = new File("src/test/resources/emails/Test Html Email2.eml")
        List<MessageWrapper> messages = MailFileUtils.load([inputEmail, inputEmail2])
        List<File> savedFiles = MailFileUtils.save(messages, outputDir)
        File firstFile = savedFiles.get(0)
        TestUtil.assertEqualsIgnoreEndings(inputEmail.text, firstFile.text)
        assertEquals("id-4CC0CA143D7046789E0F955616E98B74gmailcom-Testemail-from-smith7075gmailcom-to-smith7075gmailcom.eml", firstFile.name)
        File secondFile = savedFiles.get(1)
        TestUtil.assertEqualsIgnoreEndings(inputEmail2.text, secondFile.text)
        assertEquals("id-4CC0CA143D7046789E0F955616E98B75gmailcom-Testemail-from-smith7075gmailcom-to-smith7075gmailcom.eml", secondFile.name)
    }
}
