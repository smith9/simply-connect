package simplyconnect.io;


import org.junit.Test
import static org.junit.Assert.assertEquals

/**
 * User: u321869
 * Date: 8/03/13
 */
public class ContentTypeToFilenameTest {
    @Test
    public void testGenerate() throws Exception {
        assertEquals("messagerfc822.eml", new ContentTypeToFilename().generate("message/rfc822"))
        assertEquals("unknown.unknown", new ContentTypeToFilename().generate("someunknownmimetype"))
    }
}
