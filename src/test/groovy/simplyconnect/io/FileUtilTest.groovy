package simplyconnect.io

import org.junit.Test
import simplyconnect.mail.MailFileUtils

import static org.junit.Assert.assertEquals

/**
 * User: david
 * Date: 17/04/13
 * Time: 9:29 PM
 */
class FileUtilTest {
    @Test
    void testFileFriendly() {
        assertEquals("abc", FileUtil.fileFriendly("a/b:c?=!~"))
    }
}
