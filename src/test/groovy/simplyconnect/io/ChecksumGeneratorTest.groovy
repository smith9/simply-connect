package simplyconnect.io

import org.apache.commons.io.FileUtils
import org.junit.Before
import org.junit.Test

import javax.activation.DataSource
import javax.activation.FileDataSource

import static org.junit.Assert.*

/**
 * User: david
 * Date: 24/02/13
 * Time: 11:31 PM
 */
class ChecksumGeneratorTest {
    private File outputDir

    @Before
    void before() {
        outputDir = new File("target/images")
        if (outputDir.exists()) {
            FileUtils.cleanDirectory(outputDir)
        }
    }
    @Test
    void testGenerateMultiple() {
        List<FileDataSource> dataSources = ["squigle.png", "squigle2.png"].collect { String filename ->
            new FileDataSource(new File("src/test/resources/images/$filename"))
        }
        ChecksumGenerator generator = new ChecksumGenerator(outputDir: outputDir)
        Map<String, File> checksumsForFiles = generator.generate(dataSources as DataSource[])
        assertEquals(2, checksumsForFiles.size())
        Map.Entry<String, File>[] checksums = checksumsForFiles.entrySet().toArray(new Map.Entry<String, File>[0])
        // on OSX run md5 squigle.png to generate checksum to compare here
        assertEquals("8262be9855c4d8046921352bbc8bb987", checksums[0].key)
        assertEquals(new File("src/test/resources/images/squigle.png").text, checksums[0].value.text)
        assertEquals("squigle.png", checksums[0].value.name)
        assertEquals("b8a9cc7f47f76644cff97b7d63c5c63e", checksums[1].key)
        assertEquals(new File("src/test/resources/images/squigle2.png").text, checksums[1].value.text)
        assertEquals("squigle2.png", checksums[1].value.name)
    }
    @Test
    void testGenerateMultipleSameNameDifferentContent() {
        List<FileDataSource> dataSources = ["src/test/resources/images", "src/test/resources/images2"].collect { String filename ->
            new FileDataSource(new File("$filename/squigle.png"))
        }
        ChecksumGenerator generator = new ChecksumGenerator(outputDir: outputDir)
        Map<String, File> checksumsForFiles = generator.generate(dataSources as DataSource[])
        assertEquals(2, checksumsForFiles.size())
        Map.Entry<String, File>[] checksums = checksumsForFiles.entrySet().toArray(new Map.Entry<String, File>[0])
        // on OSX run md5 squigle.png to generate checksum to compare here
        assertEquals("8262be9855c4d8046921352bbc8bb987", checksums[0].key)
        assertEquals(new File("src/test/resources/images/squigle.png").text, checksums[0].value.text)
        assertEquals("squigle.png", checksums[0].value.name)
        assertEquals("b8a9cc7f47f76644cff97b7d63c5c63e", checksums[1].key)
        assertEquals(new File("src/test/resources/images2/squigle.png").text, checksums[1].value.text)
        String name = checksums[1].value.name
        assertTrue(name.startsWith("squigle") && name.endsWith(".png"))
        assertTrue(name.length() > "squigle.png".size())
    }
}
