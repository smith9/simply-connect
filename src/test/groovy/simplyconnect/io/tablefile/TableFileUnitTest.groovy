package simplyconnect.io.tablefile

import static org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created with IntelliJ IDEA.
 * User: Rob Nielsen
 * Date: 21/01/13
 * Time: 7:00 AM
 */

class TableFileUnitTest {
    private static final ArrayList<TableFileSampleRow> ROWS = [new TableFileSampleRow("A", "B", "C"), new TableFileSampleRow("D", "E", "F")]

    @Test
    public void testParseTabDelimited() {
        checkParse("A\tB\tC\nD\tE\tF")
    }

    @Test
    public void testParseTabDelimitedWithTitleRow() {
        checkParse("One\tTwo\tThree\nA\tB\tC\nD\tE\tF")
    }

    @Test
    public void testParsePipeDelimited() {
        checkParse("| A | B | C |\n| D | E | F |")
    }

    @Test
    public void testParsePipeDelimitedWithLotsOfCommas() {
        checkParse("| A,1,2,3,4 | B | C |\n| D | E | F |", [new TableFileSampleRow("A,1,2,3,4", "B", "C"), new TableFileSampleRow("D", "E", "F")]        )
    }

    @Test
    public void testParsePipeDelimitedWithEmbeddedPipes() {
        checkParse("| A||B | B&&C | D |\n| D | E | F |", [new TableFileSampleRow("A||B", "B&&C", "D"), new TableFileSampleRow("D", "E", "F")]        )
    }

    @Test
    public void testParseSingle() {
        String content = """
--------------------
|   Assign Setup   |
|------------------|
| PART_A           |
--------------------"""
        TableFile<TableFileSampleRow> tableFile = new TableFile<TableFileSampleRow>(TableFileSampleRow.class, ["Assign Setup"], content)
        assertEquals(tableFile.results, [new TableFileSampleRow("PART_A", null, null)])
    }

    @Test
    public void testParsePipeDelimitedWithTitleRow() {
        checkParse("|One|Two|Three|\n|A|B|C|\n| D | E | F |")
    }

    @Test
    public void testParseOutputFormat() {
        TableFile<TableFileSampleRow> tableFile = new TableFile<TableFileSampleRow>(TableFileSampleRow.class, ["One", "Two", "Three"])
        ROWS.each { tableFile.addResult(it) }
        String content = tableFile.toString()
        checkParse(content)
    }

    private void checkParse(String content, List<TableFileSampleRow> expectedRows = ROWS) {
        TableFile<TableFileSampleRow> tableFile = new TableFile<TableFileSampleRow>(TableFileSampleRow.class, ["One", "Two", "Three"], content)
        assertEquals(expectedRows, tableFile.results)
    }
}