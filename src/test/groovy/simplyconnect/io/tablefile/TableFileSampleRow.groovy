package simplyconnect.io.tablefile

/**
 * Created with IntelliJ IDEA.
 * User: Rob Nielsen
 * Date: 29/01/13
 * Time: 9:22 AM
 */
class TableFileSampleRow implements PipeStorable {
    String one
    String two
    String three

    public TableFileSampleRow(List<String> row) {
        this(row[0], row[1], row[2])
    }

    public TableFileSampleRow(String one, String two, String three) {
        this.one = one
        this.two = two
        this.three = three
    }

    Object getKey() {
        one
    }

    List<String> toRow() {
        return [one, two, three]
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        TableFileSampleRow that = (TableFileSampleRow) o

        if (one != that.one) return false
        if (three != that.three) return false
        if (two != that.two) return false

        return true
    }

    int hashCode() {
        int result
        result = (one != null ? one.hashCode() : 0)
        result = 31 * result + (two != null ? two.hashCode() : 0)
        result = 31 * result + (three != null ? three.hashCode() : 0)
        return result
    }

    @Override
    public String toString() {
        return "TableFileSampleRow{" +
                "one='" + one + '\'' +
                ", two='" + two + '\'' +
                ", three='" + three + '\'' +
                '}';
    }
}
