package simplyconnect.jira

import org.apache.commons.io.FileUtils
import org.junit.Before
import org.junit.Test

import static org.junit.Assert.assertEquals

/**
 * User: david
 * Date: 24/02/13
 * Time: 11:05 AM
 */
class FileChecksumHandlerTest {
    File checksumDir
    FileChecksumHandler handler
    private static final String JIRA_1 = "EST-0001"
    private static final String JIRA_2 = "EST-0002"
    private static final String HASH_1A = "hash1A"
    private static final String HASH_1B = "hash1B"
    private static final String HASH_2A = "hash2A"
    private static final String HASH_2B = "hash2B"

    @Before
    void before() {
        checksumDir = new File("target/checksums")
        checksumDir.mkdirs()
        FileUtils.cleanDirectory(checksumDir)
        handler = new FileChecksumHandler(checksumDir)
    }
    @Test
    void testChecksumsReturnsEmptyListForNewJira() {
        assertEquals([] as Set<String>, handler.getChecksums(JIRA_1))
    }
    @Test
    void testCreatingChecksumsWhenNoneExistAlready() {
        handler.setChecksums(JIRA_1, [HASH_1A] as Set<String>)
        assertEquals([HASH_1A] as Set<String>, handler.getChecksums(JIRA_1))
    }
    @Test
    void testUpdatingExistingChecksums() {
        handler.setChecksums(JIRA_1, [HASH_1A] as Set<String>)
        assertEquals([HASH_1A] as Set<String>, handler.getChecksums(JIRA_1))
        handler.setChecksums(JIRA_1, [HASH_1A, HASH_1B] as Set<String>)
        assertEquals([HASH_1A, HASH_1B] as Set<String>, handler.getChecksums(JIRA_1))
    }
    @Test
    void testNewHandlerUpdatingExistingChecksums() {
        handler.setChecksums(JIRA_1, [HASH_1A] as Set<String>)
        assertEquals([HASH_1A] as Set<String>, handler.getChecksums(JIRA_1))
        handler = new FileChecksumHandler(checksumDir)
        handler.setChecksums(JIRA_1, [HASH_1A, HASH_1B] as Set<String>)
        assertEquals([HASH_1A, HASH_1B] as Set<String>, handler.getChecksums(JIRA_1))
    }
    @Test
    void testCreatingMultipleChecksums() {
        checkMultiple(JIRA_1, [HASH_1A, HASH_1B] as Set<String>)
        checkMultiple(JIRA_2, [HASH_2A, HASH_2B] as Set<String>)
    }

    private void checkMultiple(String jiraNumber, Set<String> checksums) {
        handler.setChecksums(jiraNumber, checksums)
        assertEquals(checksums, handler.getChecksums(jiraNumber))
    }
}
